/**
 * 
 */
package networking.prosurvey;

import survery.database.Participant;
import survery.database.Questions;
import survery.database.Response;
import survery.database.Responsechoice;
import survery.database.Settings;
import survery.database.Survey;
import survery.database.User;
import tz.co.bongolive.apps.prosurvey.AppPreference;
import tz.co.bongolive.apps.prosurvey.R;
import tz.co.bongolive.apps.prosurvey.StartScreen;
import tz.co.bongolive.apps.prosurvey.Surveys;

import android.accounts.Account;
import android.annotation.TargetApi;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SyncResult;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import org.json.JSONException;

/**
 * @author nasznjoka
 * 
 * Oct 8, 2014
 * @param
 *
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class SyncAdapterMaster extends AbstractThreadedSyncAdapter
{
	
	private static final String TAG = "SyncAdapter";
	AppPreference appPreference;
	ContentResolver mContentResolver ;    
	/**
	 * @param context
	 * @param autoInitialize
	 */
	public SyncAdapterMaster(Context context, boolean autoInitialize) {
		super(context, autoInitialize);
		mContentResolver = context.getContentResolver() ; 
	}
	
	//android 3.0 and above compatibility
	public SyncAdapterMaster(Context context, boolean autoInitialize, boolean allowParallelSync)
	{
		super(context, autoInitialize, allowParallelSync);
		mContentResolver = context.getContentResolver() ;
	}

	/* (non-Javadoc)
	 * @see android.content.AbstractThreadedSyncAdapter#onPerformSync(android.accounts.Account, android.os.Bundle, java.lang.String, android.content.ContentProviderClient, android.content.SyncResult)
	 */
	@Override
	public void onPerformSync(Account account, Bundle extras, String authority,
			ContentProviderClient provider, SyncResult syncResult) {
		appPreference = new AppPreference(getContext());
		/*
		 * This is where all the methods for uploading and download are done
		 */ 
		Log.d(TAG, "*********** start of *********   onPerformSync for account[" + account.name + "]");
		try {
			if(appPreference.getSyS() > 0)
		     Survey.processSurvey(getContext());

			 Log.d(TAG, "SURVEY processing");
		} catch (Exception e) {
//            e.printStackTrace();
        }  
		
		 
		try {
			if(appPreference.getSyQ() > 1)
			Questions.processQuestion(getContext());
			 Log.d(TAG, "QUESTIONS processing");
			// Customers 
		} catch (Exception e) {
//            e.printStackTrace();
        }  
		try {
			if(appPreference.getSyO() > 2)
			Responsechoice.processQuestion(getContext());
			  Log.d(TAG, "RESPONSE CHOICE TEXT processing");
		} catch (Exception e) {
//           e.printStackTrace();
       } 
		
		try {

			if(appPreference.getSyU() > 3)
			 User.processCredentials(getContext());
			 Log.v(TAG, "USERS processing");
		} catch (Exception e) {
//            e.printStackTrace();
        } 

		try {
			Log.d(TAG, "PARTICIPANTS processing");
			Participant.validatePartiSync(getContext());
		} catch (Exception e) {
//            e.printStackTrace();
        }


		try {
			Log.d(TAG, "RESPONSE processing");
			Response.validateResponseSync(getContext());
		} catch (Exception e) {
//            e.printStackTrace();
		}


		try {
			Log.d(TAG, "SETTINGS processing");
			if(appPreference.getSySt() > 4)
				Settings.processSettings(getContext());
		} catch (Exception e) {
//            e.printStackTrace();
		}

		Log.d(TAG, "*********** end of *********   onPerformSync for account[" + account.name + "]");
	}
 
}
