/*
 * Copyright (C) 2010 The Android Open Source Project
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package networking.prosurvey;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import tz.co.bongolive.apps.prosurvey.AppPreference;
import tz.co.bongolive.apps.prosurvey.Dashboard;

public class Constants {

    /**
     * Account type string.
     */
    public static final String ACCOUNT_TYPE = "tz.co.prosurvey";
	public static final String ACCOUNT= "Prosurvey Mobile" ; 
//	public static final String SERVER = "http://www.apps.co.tz/prosurvey/main.php";
//	public static final String SERVER = "http://pro.co.tz/egpaf/index.php/api/service";
	public static final String SERVER = "http://apps.co.tz/prosurvey/index.php/api/service";
//	public static final String SERVER = "http://egpaf.pro.co.tz/index.php/api/service";
	public static final String KEY_DB = "key_db_name";
	public static final String KEY_DBPATH = "key_db_path";
	public static final String SENDER = "bongodev02@gmail.com" ;
	public static final String RECE = "dev02logs@gmail.com" ;
	public static final String MAILPASS = "Mjeremani@2013";
	//    public static final String SERVER   = "http://egpaf.pro.co.tz/main.php";
	public static String TAGUSERDATA = "user_data";
	public static String TAGSURVEY = "survey_data"; 
	public static String TAGQUESTION = "questions_data";
	public static String TAGRESPONSE = "response_data";
	public static String RESPONSEARRAY = "response_data_array";
	public static String PARTICIPANTARRAY = "participant_data_array";
	public static String TAGRESPONSECHOICE = "response_choice_data"; 
	public static String UPDATEDARRAY = "update_values";
	public static String TAGPARTICIPANT = "participant_data"; 
	public static String TAGSETTINGS = "settings_data";
	public static String TAGUPDATE = "update_tag";
	public static String SUCCESS = "success" ; 
	public static String FAILURE = "failure";
	public static String ACKVALUE = "1" ;
	public static String OTHER = "other" ;
	public static String SEPARATOR = "||" ;
	static String ACK = "ack" ;
	public static String TAGACK = "acktag" ;
	public static String PREF_LANG_KEY = "pref_language_key";
	public static String PREF_SYNC_KEY = "pref_sync_key";
	public static String PREF_ADMIN_KEY = "pref_admin_key";
	public static String PREF_EXPORT_KEY = "pref_export_key";
	public static String PREF_RESETSYNC_KEY = "pref_resetsync_key";
    public static String PREF_IMPORT_KEY =  "pref_import_extenal_key";
    public static String PREF_IMEI_KEY =  "pref_imei_key";
	public static String IMEI = "imei" ;
	public static String ACKREFERENCE = "ref";
	public static String ACKKEY = "key" ;
	public static final String USERDATA = "authdata";
	public static final String ACKTARGETKEY = "target";
	public static final String ACKTARGETNORMAL = "0";//normal
	public static final String ACKTARGETUPDATE = "1";//update
	public static final String USERROLE = "userrole";
	public static final String USERID = "userid";
	public static final String SURVEYSYNC = "sync_survey";
	public static final String SURVEYSYNCVALUE = "1";
	public static final String QUESTIONSYNC = "sync_qn";
	public static final String QUESTIONSYNCVALUE = "1";
	public static final String RESPONSESYNC = "sync_responses";
	public static final String RESPONSESYNCVALUE = "1";
	public static final String USERSSYNC = "sync_user";
	public static final String USERSSYNCVALUE = "1";
	public static final String SETTINGSSYNC = "sync_settings";
	public static final String SETTINGSSYNCVALUE = "1";
	public static final String USERLANGUAGE = "userlanguage";
	// participant range 29900 22999
    /**
     * Authtoken type string.
     */
    public static final String AUTHTOKEN_TYPE =  "com.example.android.samplesync"; 
	private static String TAG = "prostock_mobile_account" ;
	public static String PREF_URL = "key_show_url";


	public static String getIMEINO(Context ctx) {
        AppPreference appPreference = new AppPreference(ctx.getApplicationContext());
        String customimei = appPreference.getDbnm();
        if(!customimei.isEmpty()){
            Log.v("customimei","custom imei "+customimei);
            return customimei;
        } else {
            TelephonyManager tManager = (TelephonyManager) ctx
                    .getSystemService(Context.TELEPHONY_SERVICE);
            String imeiid = tManager.getDeviceId();
            Log.v("legitimei","legit imei "+imeiid);
            return imeiid;
        }
	}

    public static String getDate() {
		Date update = null;
		String returnstring = null ;
		try {
		  update = new Date(); 

		  String[] formats = new String[] { 
				   "yyyy-MM-dd HH:mm" 
				 };
		  
		  for(String df: formats)
		  {
			  SimpleDateFormat sdf = new SimpleDateFormat(df, Locale.getDefault()); 
			  
			  return sdf.format(update);
		  }
		} finally {
		
		}
		return returnstring;
	}
    public static String getDateOnly() {
    	Date update = null;
		String returnstring = null ;
		try {
		  update = new Date(); 

		  String[] formats = new String[] { 
				   "yyyy-MM-dd" 
				 };
		  
		  for(String df: formats)
		  {
			  SimpleDateFormat sdf = new SimpleDateFormat(df, Locale.getDefault()); 
			  
			  return sdf.format(update);
		  }
		} finally {
		
		}
		return returnstring;
  	}
    
    public static String getTimeOnly() {
    	Date update = null;
		String returnstring = null ;
		try {
		  update = new Date(); 

		  String[] formats = new String[] { 
				   "HH:mm" 
				 };
		  
		  for(String df: formats)
		  {
			  SimpleDateFormat sdf = new SimpleDateFormat(df, Locale.getDefault()); 
			  
			  return sdf.format(update);
		  }
		} finally {
		
		}
		return returnstring;
  	}
	public static String getTimeGmt() {
		Date update = null;
		String returnstring = null ;
		try {
			update = new Date();

			String[] formats = new String[] {
					"yyyy-MM-dd HH:mm"
			};

			for(String df: formats)
			{
				SimpleDateFormat sdf = new SimpleDateFormat(df, Locale.getDefault());
//				sdf.setTimeZone(TimeZone.getTimeZone("GMT"));

				return sdf.format(update);
			}
		} finally {

		}
		return returnstring;
	}

	public static void LogException(Exception ex) {
		Log.d(TAG +" Exception",
				TAG+"Exception -- > " + ex.getMessage() + "\n");
		ex.printStackTrace();
	}
  

   
   public static String makeSHA1Hash(String input) throws NoSuchAlgorithmException {
	   MessageDigest md = MessageDigest.getInstance("SHA1");
	   md.reset();
	   byte[] buffer = input.getBytes();
	   md.update(buffer);
	   byte[] digest = md.digest();
	    
	   String hexStr = "";
	   for (int i = 0; i < digest.length; i++) {
	   hexStr +=  Integer.toString( ( digest[i] & 0xff ) + 0x100, 16).substring( 1 );
	   }
	   return hexStr;
	   }
   public static boolean hasInternetAccess(Context context) {
	    if (isNetworkAvailable(context)) {
	        try {
	            HttpURLConnection urlc = (HttpURLConnection) 
	                (new URL("http://clients3.google.com/generate_204")
	                .openConnection());
	            urlc.setRequestProperty("User-Agent", "Android");
	            urlc.setRequestProperty("Connection", "close");
	            urlc.setConnectTimeout(1500); 
	            urlc.connect();
	            return (urlc.getResponseCode() == 204 &&
	                        urlc.getContentLength() == 0);
	        } catch (IOException e) {
	            Log.e(TAG, "Error checking internet connection", e);
	        }
	    } else {
	        Log.d(TAG, "No network available!");
	    }
	    return false;
	}


	public static boolean isNetworkAvailable(Context context) {
		ConnectivityManager connectivityMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connectivityMgr.getActiveNetworkInfo();
		/// if no network is available networkInfo will be null
		if (networkInfo != null && networkInfo.isConnected()) {
			return true;
		}
		return false;
	}

public static void storeUserId(Context context, int user, String constant) {
    final SharedPreferences prefs = getSurveyPreferences(context); 
    SharedPreferences.Editor editor = prefs.edit(); 
    editor.putInt(constant, user);
    editor.commit();
}

public static SharedPreferences getSurveyPreferences(Context context) { 
    return context.getSharedPreferences(Dashboard.class.getSimpleName(),
            Context.MODE_PRIVATE);
}
 
public static int getUserId(Context context,String constant) {
    final SharedPreferences prefs = getSurveyPreferences(context);
    int registrationId = prefs.getInt(constant, 0);
    if (registrationId != 1 || registrationId != 0) {
        Log.i(TAG, "Registration not found.");
        return 0;
    }  
    return registrationId;
}

 public static String getVersionNumber(Context context){
     try {
         String versionName = context.getPackageManager()
                 .getPackageInfo(context.getPackageName(), 0).versionName;
         return versionName;
     } catch (PackageManager.NameNotFoundException ne){
         ne.printStackTrace();
     }
     return "";
    }



}