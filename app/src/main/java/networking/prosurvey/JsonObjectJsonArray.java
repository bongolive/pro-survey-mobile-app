/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package networking.prosurvey;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.BasicHttpEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import tz.co.bongolive.apps.prosurvey.R;

import android.os.StrictMode;
import android.util.Log;

/**
 *
 * @author nasznjoka
 */
public class JsonObjectJsonArray {
 static String response = null;
    public final static int GET = 1;
    public final static int POST = 2;
    static JSONObject jObj = null;
    static InputStream is ; 
    static JSONArray jArry = null ;
    static String json = "";
    public JsonObjectJsonArray() {
 
    }
      public String getJsonArray(String url, int method,
                                 JSONObject params){
//            List<NameValuePair> params) {
        try {
            // http client
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpEntity httpEntity = null;
            HttpResponse httpResponse = null; 
            // Checking http request method type
            if (method == POST) {
	            json = params.toString();
			httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(url);

	            StringEntity se = new StringEntity(json);
	            se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
//	            httpPost.setHeader("Content-type", "application/json");
//	            httpPost.setEntity(new StringEntity(params.toString(), HTTP.UTF_8));
//		    	httpPost.setEntity(new UrlEncodedFormEntity(params));
//	            httpPost.setEntity(new StringEntity(params.toString(), "UTF8"));
//	            httpPost.setHeader("Content-type", "application/json");
	            httpPost.setEntity(se);
			httpResponse = httpClient.execute(httpPost);
			httpEntity = httpResponse.getEntity();
                        is = httpEntity.getContent();
 
            } else if (method == GET) {
                /*// appending params to url
                if (params != null) {
                    params = new ArrayList<NameValuePair>() ;
                    String paramString = URLEncodedUtils
                            .format(params, "utf-8");
                    url += "?" + paramString;
                }
                HttpGet httpGet = new HttpGet(url);
 
                httpResponse = httpClient.execute(httpGet);
                httpEntity = httpResponse.getEntity();
                response = EntityUtils.toString(httpEntity) ; */
            } 
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        //return response;
        try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			response = sb.toString();
			Log.e("JSON", response);
		} catch (Exception e) {
			Log.e("Buffer Error", "Error converting result " + e.toString());
		}

		return response ;
 
    }

	public String getJsonArrayYenyew(String url, int method,
	                           JSONArray params){
//            List<NameValuePair> params) {
		try {
			// http client
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpEntity httpEntity = null;
			HttpResponse httpResponse = null;
			// Checking http request method type
			if (method == POST) {
				json = params.toString();
				httpClient = new DefaultHttpClient();
				HttpPost httpPost = new HttpPost(url);

				StringEntity se = new StringEntity(json);
				se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
				httpPost.setEntity(se);
				httpResponse = httpClient.execute(httpPost);
				httpEntity = httpResponse.getEntity();
				is = httpEntity.getContent();

			} else if (method == GET) {
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		//return response;
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			response = sb.toString();
			Log.e("JSON", response);
		} catch (Exception e) {
			Log.e("Buffer Error", "Error converting result " + e.toString());
		}

		return response ;

	}
      
      public JSONObject getJsonObject(String url,  JSONObject params) {

		// Making HTTP request
		try {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
			StrictMode.setThreadPolicy(policy);
			
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(url);
			StringEntity st = new StringEntity(params.toString());
			st.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			httpPost.setEntity(st);

			HttpResponse httpResponse = httpClient.execute(httpPost);
			HttpEntity httpEntity = httpResponse.getEntity();
			is = httpEntity.getContent();

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			json = sb.toString();
			Log.e("JSON", json);
		} catch (Exception e) {
			Log.e("Buffer Error", "Error converting result " + e.toString());
		}

		// try parse the string to a JSON object
		try {
			jObj = new JSONObject(json);			
		} catch (JSONException e) {
			Log.e("JSON Parser", "Error parsing data " + e.toString());
		}

		// return JSON String`
		return jObj;

	} 
    
}
