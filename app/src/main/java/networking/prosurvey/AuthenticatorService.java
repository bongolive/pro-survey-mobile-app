package networking.prosurvey;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import tz.co.bongolive.apps.prosurvey.R;

/**
 * @author nasznjoka
 * 
 * Oct 8, 2014
 *
 */
public class AuthenticatorService extends Service{
	private Authenticator mAuthenticator ;
	private static String TAG ="AuthenticatorService" ;
	/* (non-Javadoc)
	 * @see android.app.Service#onBind(android.content.Intent)
	 */
	@Override
	public void onCreate(){
		if (Log.isLoggable(TAG, Log.VERBOSE)) {
            Log.v(TAG, "Survey Authentication Service started.");
        }
		mAuthenticator = new Authenticator(this);
	}
	 @Override
	    public void onDestroy() {
	        if (Log.isLoggable(TAG, Log.VERBOSE)) {
	            Log.v(TAG, "Survey Authentication Service stopped.");
	        }
	    }
	 
	 @Override
	    public IBinder onBind(Intent intent) {
	        if (Log.isLoggable(TAG, Log.VERBOSE)) {
	            Log.v(TAG,
	                "getBinder()...  returning the AccountAuthenticator binder for intent "
	                    + intent);
	        }
	        return mAuthenticator.getIBinder();
	    }

}
