/**
 * 
 */
package survery.database;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import java.io.IOException;

import tz.co.bongolive.apps.prosurvey.AppPreference;
import tz.co.bongolive.apps.prosurvey.DbExportImport;

/**
 *    @author nasznjoka
 *
 *    
 *    Nov 15, 2014
 */
public class ContentProviderSurvey extends ContentProvider{
	private static UriMatcher uriMatcher ;
	public static String AUTHORITY = "survery.database.ContentProviderSurvey" ;
	Context context ; 
	
	private static final int GENERALUSER = 1;
	private static final int SPECIFICUSER = 2 ;
	private static final int GENERALSURVEY = 3;
	private static final int SPECIFICSURVEY = 4;
	private static final int GENERALPARTICIPANT = 5;
	private static final int SPECIFICPARTICIPANT = 6 ;
	private static final int GENERALRESPONSECHOICE = 7;
	private static final int SPECIFICRESPONSECHOICE = 8 ;
	private static final int GENERALRESPONSE = 9 ;
	private static final int SPECIFICRESPONSE = 10;
	private static final int GENERALQUESTIONS = 11 ;
	private static final int SPECIFICQUESTIONS = 12 ;
	private static final int GENERALSETTINGS = 13 ;
	private static final int SPECIFICSETTINGS = 14 ;
	private static final int GENERALLOGS = 15 ;
	private static final int SPECIFICLOGS = 16 ;

	static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH) ; 
        uriMatcher.addURI(AUTHORITY, User.TABLENAME, GENERALUSER) ;
		uriMatcher.addURI(AUTHORITY, User.TABLENAME+ "/#", SPECIFICUSER) ;
        uriMatcher.addURI(AUTHORITY, Survey.TABLENAME, GENERALSURVEY) ;
		uriMatcher.addURI(AUTHORITY, Survey.TABLENAME+ "/#", SPECIFICSURVEY) ;
        uriMatcher.addURI(AUTHORITY, Participant.TABLENAME, GENERALPARTICIPANT) ;
		uriMatcher.addURI(AUTHORITY, Participant.TABLENAME+ "/#", SPECIFICPARTICIPANT) ;
        uriMatcher.addURI(AUTHORITY, Responsechoice.TABLENAME, GENERALRESPONSECHOICE) ;
		uriMatcher.addURI(AUTHORITY, Responsechoice.TABLENAME+ "/#", SPECIFICRESPONSECHOICE) ;
        uriMatcher.addURI(AUTHORITY, Response.TABLENAME, GENERALRESPONSE) ;
		uriMatcher.addURI(AUTHORITY, Response.TABLENAME+ "/#", SPECIFICRESPONSE) ;
        uriMatcher.addURI(AUTHORITY, Questions.TABLENAME, GENERALQUESTIONS) ;
		uriMatcher.addURI(AUTHORITY, Questions.TABLENAME+ "/#", SPECIFICQUESTIONS) ;
		uriMatcher.addURI(AUTHORITY, Settings.TABLENAME, GENERALSETTINGS) ;
		uriMatcher.addURI(AUTHORITY, Settings.TABLENAME+ "/#", SPECIFICSETTINGS) ;
		uriMatcher.addURI(AUTHORITY, LogsTable.TABLENAME, GENERALLOGS) ;
		uriMatcher.addURI(AUTHORITY, LogsTable.TABLENAME+ "/#", SPECIFICLOGS) ;
	}

	/* (non-Javadoc)
	 * @see android.content.ContentProvider#delete(android.net.Uri, java.lang.String, java.lang.String[])
	 */
    @Override
	public int delete(Uri uri, String selection, String[] selectionArgs) { 
		SQLiteDatabase db = DatabaseHandler.getInstance(getContext()).getWritableDatabase();
		int count = 0 ;
		switch(uriMatcher.match(uri)){
		case GENERALUSER:
			count = db.delete(User.TABLENAME, selection, selectionArgs);
			break;
		case SPECIFICUSER:
			count = db.delete(User.TABLENAME, User.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERALSURVEY:
			count = db.delete(Survey.TABLENAME, selection,selectionArgs);
			break ;
		case SPECIFICSURVEY:
			count = db.delete(Survey.TABLENAME, Survey.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERALPARTICIPANT:
			count = db.delete(Participant.TABLENAME, selection,selectionArgs);
			break ;
		case SPECIFICPARTICIPANT:
			count = db.delete(Participant.TABLENAME, Participant.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERALRESPONSECHOICE:
			count = db.delete(Responsechoice.TABLENAME, selection,selectionArgs);
			break ;
		case SPECIFICRESPONSECHOICE:
			count = db.delete(Responsechoice.TABLENAME,Responsechoice.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;  
		case GENERALRESPONSE:
			count = db.delete(Response.TABLENAME, selection,selectionArgs);
			break ;
		case SPECIFICRESPONSE:
			count = db.delete(Response.TABLENAME,  Response.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERALQUESTIONS:
			count = db.delete(Questions.TABLENAME, selection,selectionArgs);
			break ;
		case SPECIFICQUESTIONS:
			count = db.delete(Questions.TABLENAME,  Questions.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
			case GENERALSETTINGS:
				count = db.delete(Settings.TABLENAME, selection,selectionArgs);
				break ;
			case SPECIFICSETTINGS:
				count = db.delete(Settings.TABLENAME,  Settings.ID + " = ?", new String[]{uri.getLastPathSegment()});
				break;
			case GENERALLOGS:
				count = db.delete(LogsTable.TABLENAME, selection,selectionArgs);
				break ;
			case SPECIFICLOGS:
				count = db.delete(LogsTable.TABLENAME,  LogsTable.ID + " = ?", new String[]{uri.getLastPathSegment()});
				break;
			default:
				throw new IllegalArgumentException(" invalid uri "+ uri);
		}
			getContext().getContentResolver().notifyChange(uri, null);
			return count ;
	}


	/* (non-Javadoc)
	 * @see android.content.ContentProvider#getType(android.net.Uri)
	 */

	@Override
	public String getType(Uri uri) { 
		switch(uriMatcher.match(uri)){
		case GENERALUSER:
			return User.GENERAL_CONTENT_TYPE ;
		case SPECIFICUSER:
			return User.SPECIFIC_CONTENT_TYPE ;
		case GENERALSURVEY:
			return Survey.GENERAL_CONTENT_TYPE ;
		case SPECIFICSURVEY:
			return Survey.SPECIFIC_CONTENT_TYPE ;
		case GENERALPARTICIPANT:
			return Participant.GENERAL_CONTENT_TYPE;
		case SPECIFICPARTICIPANT:
			return Participant.SPECIFIC_CONTENT_TYPE ;
		case GENERALRESPONSECHOICE:
			return Responsechoice.GENERAL_CONTENT_TYPE;
		case SPECIFICRESPONSECHOICE:
			return Responsechoice.SPECIFIC_CONTENT_TYPE; 
		case GENERALRESPONSE:
			return Response.GENERAL_CONTENT_TYPE;
		case SPECIFICRESPONSE:
			return Response.SPECIFIC_CONTENT_TYPE ;
		case GENERALQUESTIONS:
			return Questions.GENERAL_CONTENT_TYPE;
		case SPECIFICQUESTIONS:
			return Questions.SPECIFIC_CONTENT_TYPE ;
			case GENERALSETTINGS:
				return Settings.GENERAL_CONTENT_TYPE;
			case SPECIFICSETTINGS:
				return Settings.SPECIFIC_CONTENT_TYPE ;
			case GENERALLOGS:
				return LogsTable.GENERAL_CONTENT_TYPE;
			case SPECIFICLOGS:
				return LogsTable.SPECIFIC_CONTENT_TYPE ;
			default:
			throw new IllegalArgumentException(" Unkown TYPE " + uri) ;
		}  
	}


	/* (non-Javadoc)
	 * @see android.content.ContentProvider#insert(android.net.Uri, android.content.ContentValues)
	 */
	@Override
	public Uri insert(Uri uri, ContentValues values) { 
		SQLiteDatabase db = DatabaseHandler.getInstance(getContext()).getWritableDatabase() ;
		long id ;
		switch(uriMatcher.match(uri))
		{
		case GENERALUSER:
			id = db.insertOrThrow(User.TABLENAME, null, values);
			break;
		case GENERALSURVEY:
			id = db.insertOrThrow(Survey.TABLENAME, null, values);
			break;
		case GENERALPARTICIPANT:
			id = db.insertOrThrow(Participant.TABLENAME, null, values);
			break;
		case GENERALRESPONSECHOICE:
			id = db.insertOrThrow(Responsechoice.TABLENAME, null, values);
			break; 
		case GENERALRESPONSE:
			id = db.insertOrThrow(Response.TABLENAME, null, values) ;
			break;
		case GENERALQUESTIONS:
			id = db.insertOrThrow(Questions.TABLENAME, null, values) ;
			break;
			case GENERALSETTINGS:
				id = db.insertOrThrow(Settings.TABLENAME, null, values) ;
				break;
			case GENERALLOGS:
				id = db.insertOrThrow(LogsTable.TABLENAME, null, values) ;
				break;
			default:
			throw new IllegalArgumentException(" error invalid uri " + uri);
		}
		Uri inserturi = ContentUris.withAppendedId(uri, id);
		getContext().getContentResolver().notifyChange(inserturi, null);
		return inserturi ;
	}

	/* (non-Javadoc)
	 * @see android.content.ContentProvider#onCreate()
	 */
	@Override
	public boolean onCreate() { 
		try{
			AppPreference appPreference = new AppPreference(getContext().getApplicationContext());
			if (!appPreference.getDbpath().isEmpty())
			{
				DbExportImport localDbExportImport = new DbExportImport(getContext());
				localDbExportImport.createDataBase();
				appPreference.reset_path();
			}
			else
			{
				DatabaseHandler.getInstance(getContext());
			}
		} catch (SQLiteException ex)
		{
			throw new Error(ex.toString());
		} catch (IOException e) {
            e.printStackTrace();
        }
        return true ;
	}


	/* (non-Javadoc)
	 * @see android.content.ContentProvider#query(android.net.Uri, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String)
	 */
	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortParticipant) { 
		SQLiteDatabase db = DatabaseHandler.getInstance(getContext()).getReadableDatabase() ;
		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
		switch(uriMatcher.match(uri)){
		case GENERALUSER:
			qb.setTables(User.TABLENAME) ;
			break ;
		case SPECIFICUSER:
			qb.setTables(User.TABLENAME);
			qb.appendWhere(User.ID + " =  " + uri.getLastPathSegment()) ;
			break ;
		case GENERALSURVEY:
			qb.setTables(Survey.TABLENAME) ;
			break ;
		case SPECIFICSURVEY:
			qb.setTables(Survey.TABLENAME);
			qb.appendWhere(Survey.ID + " =  " + uri.getLastPathSegment()) ;
			break ;
		case GENERALPARTICIPANT:
			qb.setTables(Participant.TABLENAME);
			break ;
		case SPECIFICPARTICIPANT:
			qb.setTables(Participant.TABLENAME);
			qb.appendWhere(Participant.ID + " =  " + uri.getLastPathSegment()) ;
			break ;
		case GENERALRESPONSECHOICE:
			qb.setTables(Responsechoice.TABLENAME);
			break ;
		case SPECIFICRESPONSECHOICE:
			qb.setTables(Responsechoice.TABLENAME);
			qb.appendWhere(Responsechoice.ID + " =  " + uri.getLastPathSegment()) ;
			break ; 
		case GENERALRESPONSE:
			qb.setTables(Response.TABLENAME);
			break ;
		case SPECIFICRESPONSE:
			qb.setTables(Response.TABLENAME);
			qb.appendWhere(Response.ID + " =  " + uri.getLastPathSegment()) ;
			break;
		case GENERALQUESTIONS:
			qb.setTables(Questions.TABLENAME);
			break ;
		case SPECIFICQUESTIONS:
			qb.setTables(Questions.TABLENAME);
			qb.appendWhere(Questions.ID + " =  " + uri.getLastPathSegment()) ;
			break ;
			case GENERALSETTINGS:
				qb.setTables(Settings.TABLENAME);
				break ;
			case SPECIFICSETTINGS:
				qb.setTables(Settings.TABLENAME);
				qb.appendWhere(Settings.ID + " =  " + uri.getLastPathSegment()) ;
				break ;
			case GENERALLOGS:
				qb.setTables(LogsTable.TABLENAME);
				break ;
			case SPECIFICLOGS:
				qb.setTables(LogsTable.TABLENAME);
				qb.appendWhere(LogsTable.ID + " =  " + uri.getLastPathSegment()) ;
				break ;
			default:
			throw new IllegalArgumentException(" UNKOWN URI :" + uri);
		}  
		Cursor c = qb.query(db, projection, selection, selectionArgs, null, null, sortParticipant) ;
		c.setNotificationUri(getContext().getContentResolver(), uri);
		return c ;
	}


	/* (non-Javadoc)
	 * @see android.content.ContentProvider#update(android.net.Uri, android.content.ContentValues, java.lang.String, java.lang.String[])
	 */
	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) { 
		SQLiteDatabase db = DatabaseHandler.getInstance(getContext()).getWritableDatabase() ;
		int count ;
		switch(uriMatcher.match(uri)){
		case GENERALUSER:
			count = db.update(User.TABLENAME, values, selection, selectionArgs);
			break;
		case SPECIFICUSER:
			count = db.update(User.TABLENAME, values, User.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERALSURVEY:
			count = db.update(Survey.TABLENAME, values, selection, selectionArgs);
			break ;
		case SPECIFICSURVEY:
			count = db.update(Survey.TABLENAME, values, Survey.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERALPARTICIPANT:
			count = db.update(Participant.TABLENAME, values, selection, selectionArgs);
			break ;
		case SPECIFICPARTICIPANT:
			count = db.update(Participant.TABLENAME, values, Participant.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERALRESPONSECHOICE:
			count = db.update(Responsechoice.TABLENAME, values, selection, selectionArgs);
			break ;
		case SPECIFICRESPONSECHOICE:
			count = db.update(Responsechoice.TABLENAME, values, Responsechoice.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break; 
		case GENERALRESPONSE:
			count = db.update(Response.TABLENAME, values, selection, selectionArgs);
			break ;
		case SPECIFICRESPONSE:
			count = db.update(Response.TABLENAME, values, Response.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERALQUESTIONS:
			count = db.update(Questions.TABLENAME, values, selection, selectionArgs);
			break ;
		case SPECIFICQUESTIONS:
			count = db.update(Questions.TABLENAME, values, Questions.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
			case GENERALSETTINGS:
				count = db.update(Settings.TABLENAME, values, selection, selectionArgs);
				break ;
			case SPECIFICSETTINGS:
				count = db.update(Settings.TABLENAME, values, Settings.ID + " = ?", new String[]{uri.getLastPathSegment()});
				break;
			case GENERALLOGS:
				count = db.update(LogsTable.TABLENAME, values, selection, selectionArgs);
				break ;
			case SPECIFICLOGS:
				count = db.update(LogsTable.TABLENAME, values, LogsTable.ID + " = ?", new String[]{uri.getLastPathSegment()});
				break;
			default:
				throw new IllegalArgumentException(" invalid uri "+ uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return count ;
	}


}
