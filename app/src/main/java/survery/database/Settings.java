package survery.database;

import java.util.ArrayList;

import networking.prosurvey.Constants;
import networking.prosurvey.JsonObjectJsonArray;
import networking.prosurvey.Validating;
import tz.co.bongolive.apps.prosurvey.AppPreference;
import tz.co.bongolive.apps.prosurvey.Mail;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

public class Settings {
	public static final String TABLENAME = "settings" ;
	public static final String ID = "id" ;
	public static final String SYSTEMID = "settings_id" ;
	public static final String ACCOUNT = "account_no" ;
	public static final String LANGUAGE = "default_language" ;
	public static final String SKIPVALUE = "skip_value" ;
	public static final String ACTIVESURVEY = "active_survey" ;
	public static final String SMSREPORT = "sms_report" ;
	public static final String SERVER = "server_url" ;
	public static final Uri BASEURI = Uri.parse("content://" + ContentProviderSurvey.AUTHORITY + "/" + TABLENAME) ;
	public static final String GENERAL_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.com.prosurvey" +
			".settings" ;
	public static final String SPECIFIC_CONTENT_TYPE = "vnd.android.cursor.item/vnd.com.prosurvey.settings" ;
    public static final String ARRAYNAME = "settings_data_array";
	public static final String UPDATEARRAYNAME = "update_settings_data_array";
//    public static final String SETTINGSOBJECT = "settings_data_array";
	private static final String TAG = "SETTINGSPAYMENTMODE";
	public static final String SYNCDATE = "syc_date";
    private static  JSONArray settingsarray;

	public static int getSettingsCount(Context context){
			ContentResolver cr = context.getContentResolver() ;
			Cursor c = cr.query(BASEURI, null, null, null, null);
			int count = 0 ;
			try {
				count = c.getCount() ;
			} finally {
				if(c != null) {
					c.close();
				}
			}
			return count;
	}
	
	public static int insertSettings(Context context,String[] settings)
	{
		int settingsCount = getSettingsCount(context) ;

		ContentResolver cr = context.getContentResolver() ;
		ContentValues cv = new ContentValues() ;
		cv.put(SYSTEMID, settings[0]) ;
		cv.put(ACCOUNT, settings[1]);
		cv.put(ACTIVESURVEY, settings[2]);
		cv.put(SKIPVALUE, settings[3]);
		cv.put(LANGUAGE, settings[4]);
		cv.put(SMSREPORT, settings[5]);
		cv.put(SERVER, settings[6]);
		cr.insert(BASEURI, cv);
		if(getSettingsCount(context) == settingsCount +1)
		{
			return 1 ;
		} else {
			return 0 ;
		}
	}

	public static int isSettingsExist(Context context, String vals)
	{
		String where = SYSTEMID + " = " + vals;
		Cursor c = context.getContentResolver().query(BASEURI, null, where, null, null) ;
		try {
			if(c.getCount() > 0)
			{
				return 1 ;
			}
		} finally {
			if(c != null) {
				c.close();
			}
		}
		return 0 ;
	}


	public static void processSettings(Context context) throws JSONException{
		AppPreference appPreference = new AppPreference(context);
		JSONObject json = getSettingsJson(context);
		Mail mail = new Mail();
		try {
			if(json.has(Constants.SUCCESS)) {
				String res = json.getString(Constants.SUCCESS);
				if (Integer.parseInt(res) == 1) {
					if (json.has(ARRAYNAME)) {
						JSONObject js = json.getJSONObject(ARRAYNAME);
						String sv = js.getString(SYSTEMID);
						String ti = js.getString(ACCOUNT);
						String tf = js.getString(ACTIVESURVEY);
						String tt = js.getString(SKIPVALUE);
						String cp = js.getString(LANGUAGE);
						String qn = js.getString(SMSREPORT);
						String sr = js.getString(SERVER);
						String[] vals = {sv, ti, tf, tt, cp, qn, sr};
						if (Validating.areSet(vals)) {
							if (isSettingsExist(context, vals[0]) < 1) {
								if (insertSettings(context, vals) == 1) {
									ack(context, 0);
									appPreference.saveSyncStatus(Constants.SETTINGSSYNC);
								}
							} else {
								ack(context, 0);
								appPreference.saveSyncStatus(Constants.SETTINGSSYNC);
							}
						} else {
							//send mail here
							String subject = Constants.getIMEINO(context) + " null settings";
							String body = " Settings no " + sv + "  has null value " +
									" today at " + Constants.getDate();
							String[] msg = {subject, body};
							try {
								mail.sendEmail(msg);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					} else if (json.has(UPDATEARRAYNAME)) {
						JSONObject js = json.getJSONObject(UPDATEARRAYNAME);
						if (isSettingsExist(context, js.getString(SYSTEMID)) == 1) {
							String sv = js.getString(SYSTEMID);
							String tf = js.getString(ACTIVESURVEY);
							String cp = js.getString(LANGUAGE);
							String tt = js.getString(SKIPVALUE);
							String qn = js.getString(SMSREPORT);
							String sr = js.getString(SERVER);
							String[] vals = {sv, tf, cp, tt, qn, sr};
							if (Validating.areSet(vals)) {
								if (updateSettings(context, vals) == 1) {
									ack(context, 1);
								}
							} else {
								//send mail here
								String subject = Constants.getIMEINO(context) + " null settings";
								String body = " Settings no " + sv + "  has null value " +
										" today at " + Constants.getDate();
								String[] msg = {subject, body};
								try {
									mail.sendEmail(msg);
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						} else {
							JSONObject jss = json.getJSONObject(UPDATEARRAYNAME);
							String sv = jss.getString(SYSTEMID);
							String ti = jss.getString(ACCOUNT);
							String tf = jss.getString(ACTIVESURVEY);
							String tt = jss.getString(SKIPVALUE);
							String cp = jss.getString(LANGUAGE);
							String qn = jss.getString(SMSREPORT);
							String sr = jss.getString(SERVER);
							String[] vals = {sv, ti, tf, tt, cp, qn, sr};
							if (Validating.areSet(vals)) {
								if (insertSettings(context, vals) == 1) {
									ack(context, 1);
								} else {
									ack(context, 1);
									appPreference.saveSyncStatus(Constants.SETTINGSSYNC);
								}
							} else {
								//send mail here
								String subject = Constants.getIMEINO(context) + " null settings";
								String body = " Settings no " + sv + "  has null value " +
										" today at " + Constants.getDate();
								String[] msg = {subject, body};
								try {
									mail.sendEmail(msg);
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						}
					}
				}
			}
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
	}


	public static String getServerName(Context context){
		ContentResolver cr = context.getContentResolver() ;
		Cursor c = cr.query(BASEURI, null, null, null, null);
		try {
			if(c.moveToFirst()) {
				return c.getString(c.getColumnIndexOrThrow(SERVER));
			}
		} finally {
			if(c != null) {
				c.close();
			}
		}
		return Constants.SERVER ;
	}

	public static int updateSettings(Context context, String[] settings)
	{
		ContentResolver cr = context.getContentResolver();
		ContentValues cv = new ContentValues() ;
		cv.put(ACTIVESURVEY, settings[1]);
		cv.put(SKIPVALUE, settings[2]);
		cv.put(LANGUAGE, settings[3]);
		cv.put(SMSREPORT, settings[4]);
		cv.put(SERVER, settings[5]);
		String where =  SYSTEMID + " = " + settings[0] ;
		if( cr.update(BASEURI, cv, where, null) > 0) {
			return 1 ;
		}
		return 0;
	}

	public static JSONObject ack(Context context, int val) throws JSONException {
		JSONObject jobs = new JSONObject();
		jobs.put("tag", Constants.TAGACK);
		jobs.put("item", "settings");
		jobs.put(Constants.ACKTARGETKEY,val);
		jobs.put(Constants.IMEI, Constants.getIMEINO(context));
		JsonObjectJsonArray js = new JsonObjectJsonArray();
		String job =  js.getJsonArray(getServerName(context), JsonObjectJsonArray.POST, jobs);
		JSONObject json;
		try {
			json = new JSONObject(job);
			if(json.length() != 0)
			{
				return json ;
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null ;
	}

	public static JSONObject getSettingsJson(Context context) throws JSONException {
		JSONObject params = new JSONObject();
		params.put("tag", Constants.TAGSETTINGS) ;
		params.put(Constants.IMEI, Constants.getIMEINO(context)) ;
		JsonObjectJsonArray js = new JsonObjectJsonArray();
		JSONObject job = null ;
		job = js.getJsonObject(getServerName(context), params);
//		Log.e("JSON", job.toString());
		return job ;
	}
	public static String[] getSettings(Context context) { 
        ContentResolver cr = context.getContentResolver();
		Cursor cursor = cr.query(BASEURI, null, null, null, null) ;
        try {
        if(cursor.getCount() >0)
        {
            String[] str = new String[cursor.getCount()]; 
 
            while (cursor.moveToNext())
            { 
                 String print =  cursor.getString(cursor.getColumnIndex(SYSTEMID));
                 String edit =  cursor.getString(cursor.getColumnIndex(ACCOUNT));
                 String taxinc =  cursor.getString(cursor.getColumnIndex(ACTIVESURVEY));
                 String vat = cursor.getString(cursor.getColumnIndex(SKIPVALUE));
                 String discount =  cursor.getString(cursor.getColumnIndex(LANGUAGE));
                 String fixed =  cursor.getString(cursor.getColumnIndex(SMSREPORT));
	             String serve = cursor.getString(cursor.getColumnIndex(SERVER));
                 str = new String[]{print,edit,taxinc,vat,discount,fixed,serve};
             }
            cursor.close();
            return str;
        }
        else
        {
        	cursor.close();
            return new String[] {};
        }
        }finally {
			 if(cursor != null) {
		            cursor.close();
		        }
		 }  
}

}
