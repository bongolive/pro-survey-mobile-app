/**
 * 
 */
package survery.database;

import java.util.ArrayList;

import networking.prosurvey.Constants;
import networking.prosurvey.JsonObjectJsonArray;
import networking.prosurvey.Validating;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import tz.co.bongolive.apps.prosurvey.AppPreference;
import tz.co.bongolive.apps.prosurvey.Mail;
import tz.co.bongolive.apps.prosurvey.R;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

/**
 *    @author nasznjoka
 *
 *    
 *    Nov 15, 2014
 */
public class User {
 public static final String TABLENAME = "user" ;
    
 public static final Uri BASEURI = Uri.parse("content://"+ContentProviderSurvey.AUTHORITY+"/"+TABLENAME) ;
 public static final String SPECIFIC_CONTENT_TYPE = "vnd.android.cursor.item/vnd.co.tz.prosurvey.user" ;
 public static final String GENERAL_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.co.tz.prosurvey.user" ;

 public static final String ID = "uid";
 public static final String SYSTEMUSERID = "user_id";
 public static final String USERNAME = "username";
 public static final String PASSWORD = "password";
 public static final String ROLE = "role";
 public static final String STATUS = "status";  
 public static final String LOCATION = "location";
 public static final String DATACOLLECTORNO = "data_collector_no"; 
 public static final String ACCOUNT = "account_no";
	public static final String ADMINNORMALSTATUS = "admin_normal";
 public static final String ARRAYNAME = "user_data_array";
	public static final String UPDATEARRAYNAME = "update_user_data_array";

private static final String TAG = User.class.getName();
	String name,sysid,status,loc,datacoll,account,rol;

	public String getName() {
		return name;
	}

	public String getSysid() {
		return sysid;
	}

	public String getStatus() {
		return status;
	}

	public String getLoc() {
		return loc;
	}

	public String getDatacoll() {
		return datacoll;
	}

	public String getAccount() {
		return account;
	}

	public String getRol() {
		return rol;
	}

	public User(String n, String sid, String loc,String account,String datacoll){
		this.name = n;
		this.sysid = sid;
		this.loc = loc;
		this.account = account;
		this.datacoll = datacoll;
	}
 
 public static JsonObjectJsonArray js ;
 
 public static int login(Context context,String[] credentials)
 { 
	 ContentResolver cr = context.getContentResolver();
	 Cursor c = cr.query(BASEURI, null, null, null, null);
	 try {
	 if(c.moveToFirst())
     {  
     	do{
     		String uid = c.getString(c.getColumnIndexOrThrow(USERNAME)) ;
     		String pass = c.getString(c.getColumnIndexOrThrow(PASSWORD));
     		if(credentials[0].equals(uid) && credentials[1].equals(pass)) {
     			return c.getInt(c.getColumnIndexOrThrow(SYSTEMUSERID)) ;
     		}
     	} while (c.moveToNext()) ;
     	} 
      }finally {
	    if(c != null) {
            c.close();
         }
      }  
	 return -1 ;
 }
 
 public static int getUserCount(Context context){ 
	    ContentResolver cr = context.getContentResolver() ;
		Cursor c = cr.query(BASEURI, null, null, null, null);
		int count = 0 ;
		try {
		count = c.getCount() ;
		} finally {
 		if(c != null) {
	            c.close();
	        }
 	}  
		return count; 
	}
	public static int changePass(Context context, String[] args){
		ContentResolver cr = context.getContentResolver();
		ContentValues contentValues = new ContentValues(1);
		contentValues.put(PASSWORD, args[1]);
		String where = SYSTEMUSERID + " = " + args[0];
		int psw = cr.update(BASEURI,contentValues,where,null);
		if(psw == 1)
			return 1;
		return 0;
	}
 public static int getUserlRole(Context context, int id){ 
	    ContentResolver cr = context.getContentResolver() ;
	    String where = SYSTEMUSERID + " = " + id ;
		Cursor c = cr.query(BASEURI, null, where, null, null); 
		try {
			if(c.moveToFirst()) { 
		 return c.getInt(c.getColumnIndexOrThrow(ROLE));
			}
		} finally {
		if(c != null) {
	            c.close();
	        }
	}
		return 9;   
	}

	public static int getAdminUser(Context context, int id){
		ContentResolver cr = context.getContentResolver() ;
		String where = SYSTEMUSERID + " = " + id ;
		Cursor c = cr.query(BASEURI, null, where, null, null);
		try {
			if(c.moveToFirst()) {
				return c.getInt(c.getColumnIndexOrThrow(ADMINNORMALSTATUS));
			}
		} finally {
			if(c != null) {
				c.close();
			}
		}
		return 0;
	}

	public static int inAppChangeAdminMode(Context context, int args, int value){
		ContentResolver cr = context.getContentResolver();
		ContentValues contentValues = new ContentValues(1);/*
		int status = getAdminUser(context, args);
		if(status == 1){
			contentValues.put(ADMINNORMALSTATUS, "0");
		} else if (status == 0){*/
			contentValues.put(ADMINNORMALSTATUS, value);
//		}
		String where = SYSTEMUSERID + " = " + args;
		int psw = cr.update(BASEURI, contentValues, where, null);
		if(psw == 1)
			return 1;
		return 0;
	}

	public static int setAdminMode(Context context, int args){
		ContentResolver cr = context.getContentResolver();
		ContentValues contentValues = new ContentValues(1);
		contentValues.put(ADMINNORMALSTATUS, "0");
		String where = SYSTEMUSERID + " = " + args;
		int psw = cr.update(BASEURI,contentValues,where,null);
		if(psw == 1)
			return 1;
		return 0;
	}

	public static int getDataCollector(Context context, int id){
	    ContentResolver cr = context.getContentResolver() ;
	    String where = SYSTEMUSERID + " = " + id ;
		Cursor c = cr.query(BASEURI, null, where, null, null); 
		try {
			if(c.moveToFirst()) { 
		 return c.getInt(c.getColumnIndexOrThrow(DATACOLLECTORNO)); 
			}
		} finally {
		if(c != null) {
	            c.close();
	        }
	}
		return 0;   
	}
 
 public static int getUserAccount(Context context, int id){ 
	    ContentResolver cr = context.getContentResolver() ;
	    String where = SYSTEMUSERID + " = " + id ;
		Cursor c = cr.query(BASEURI, null, where, null, null); 
		try {
			if(c.moveToFirst()) { 
		 return c.getInt(c.getColumnIndexOrThrow(ACCOUNT)); 
			}
		} finally {
		if(c != null) {
	            c.close();
	        }
	}
		return 9;   
	}
 
 
 public static String getDataCollectorNO(Context context, int userid)
 {
	 ContentResolver cr = context.getContentResolver() ;
	    String where = SYSTEMUSERID + " = " + userid ;
		Cursor c = cr.query(BASEURI, null, where, null, null); 
		try {
			if(c.moveToFirst()) { 
		 return c.getString(c.getColumnIndexOrThrow(DATACOLLECTORNO)); 
			}
		} finally {
		if(c != null) {
	            c.close();
	        }
	}
		return null; 
 }
 
 public static String getFacilityNO(Context context, int userid)
 {
	 ContentResolver cr = context.getContentResolver() ;
	    String where = SYSTEMUSERID + " = " + userid ;
		Cursor c = cr.query(BASEURI, null, where, null, null); 
		try {
			if(c.moveToFirst()) { 
		 return c.getString(c.getColumnIndexOrThrow(LOCATION)); 
			}
		} finally {
		if(c != null) {
	            c.close();
	        }
	}
		return null; 
 }
 
 public static int storeCredentials(Context context, String[] credentials)
 {
	 int value = getUserCount(context);
	 ContentValues cv = new ContentValues();
	 cv.put(SYSTEMUSERID, credentials[0]);
	 cv.put(USERNAME, credentials[1]);
	 cv.put(PASSWORD, credentials[2]);
	 cv.put(ROLE, credentials[3]);
	 cv.put(STATUS, credentials[4]);  
	 cv.put(LOCATION, credentials[5]);
	 cv.put(DATACOLLECTORNO, credentials[6]);
	 cv.put(ACCOUNT, credentials[7]);
	 ContentResolver cr = context.getContentResolver() ;
	 cr.insert(BASEURI, cv);
	 if(getUserCount(context) == value + 1) {
//		 Log.v(TAG, "USER "+ credentials[1] + " IS CREATED") ;
		 return 1 ;
	 }
	 return 0 ;
 }
 
 public static int storeCredentialsLocal(Context context, String[] credentials)
 {
	 int value = getUserCount(context);
	 ContentValues cv = new ContentValues();
	 cv.put(USERNAME, credentials[0]);
	 cv.put(PASSWORD, credentials[1]); 
	 ContentResolver cr = context.getContentResolver() ;
	 cr.insert(BASEURI, cv);
	 if(getUserCount(context) == value + 1) {
		 return 1 ;
	 }
	 return 0 ;
 }
 
 public static int isUserPresent(Context context, String userid)
 {
	 String where = SYSTEMUSERID + " LIKE " + userid ;
 	Cursor c = context.getContentResolver().query(BASEURI, null, where,null,null) ;
 	 try { 
 	if(c.getCount() > 0)
 	{ 
// 				Log.v(TAG, "USER "+ userid+" IS THERE ALREADY SKIP") ;
 				return 1 ; 
 	} 
	 } finally {
		 if(c != null) {
	            c.close();
	        }
	 }
 	return 0 ;
 } 


	public static JSONObject getCredentials(Context context) throws JSONException {
		JSONObject jobs = new JSONObject();
		jobs.put("tag", Constants.TAGUSERDATA);
		jobs.put(Constants.IMEI, Constants.getIMEINO(context));
		js = new JsonObjectJsonArray();
		String job =  js.getJsonArray(Settings.getServerName(context), JsonObjectJsonArray.POST, jobs);
		JSONObject json;
		try {
			json = new JSONObject(job);
			if(json.length() != 0)
			{
				return json ;
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null ;
	}
	public static int updateUser(Context context, String[] data)
	{
		ContentResolver cr = context.getContentResolver();
		ContentValues cv = new ContentValues() ;
		cv.put(ROLE, data[1]);
		cv.put(LOCATION, data[2]);
		cv.put(PASSWORD, data[3]);
		cv.put(USERNAME, data[4]);
		String where =  SYSTEMUSERID + " = " + data[0] ;
		if( cr.update(BASEURI, cv, where, null) > 0) {
			return 1 ;
		}
		return 0;
	}

	public static JSONObject ack(Context context, int val) throws JSONException {
		JSONObject jobs = new JSONObject();
		jobs.put("tag", Constants.TAGACK);
		jobs.put("item","users");
		jobs.put(Constants.ACKTARGETKEY,val);
		jobs.put(Constants.IMEI, Constants.getIMEINO(context));
		js = new JsonObjectJsonArray();
		String job =  js.getJsonArray(Settings.getServerName(context), JsonObjectJsonArray.POST, jobs);
		JSONObject json;
		try {
			json = new JSONObject(job);
			if(json.length() != 0)
			{
				return json ;
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null ;
	}

 public static void processCredentials(Context context) throws JSONException {
	 JSONArray userArray ;
	 JSONObject json = getCredentials(context);
	 Mail mail = new Mail();
	 AppPreference appPreference = new AppPreference(context);
	 try {
		 if(json.has(ARRAYNAME)) {
			 userArray = json.getJSONArray(ARRAYNAME);

			 if (!TextUtils.isEmpty(json.getString(Constants.SUCCESS))) {
				 String res = json.getString(Constants.SUCCESS);
				 if (Integer.parseInt(res) == 1) {
					 int success = 0;
					 for (int i = 0; i < userArray.length(); i++) {
						 JSONObject js = userArray.getJSONObject(i);
						 String sid = js.getString(SYSTEMUSERID);
						 String un = js.getString(USERNAME);
						 String ps = js.getString(PASSWORD);
						 String rl = js.getString(ROLE);
						 String st = js.getString(STATUS);
						 String lc = js.getString(LOCATION);
						 String dc = js.getString(DATACOLLECTORNO);
						 String ac = js.getString(ACCOUNT);
						 String[] vals = new String[]{sid, un, ps, rl, st, lc, dc, ac};
						  if(Validating.areSet(vals)) {
							  if (isUserPresent(context, vals[0]) < 1) {
								  if (storeCredentials(context, vals) == 1) {
									  success += 1;
								  }
							  } else {
								  success += 1;
							  }
						  } else {
							  String subject = Constants.getIMEINO(context)+" null user";
							  String body = " User no "+sid+" username "+un+" account no" +ac+
									  " has null value " +
									  " today at "+Constants.getDate();
							  String[] msg = {subject,body};
							  try {
								  mail.sendEmail(msg);
							  } catch (Exception e) {
								  e.printStackTrace();
							  }
						  }
					 }
					 if (success == userArray.length()) {
						 ack(context,0);
						 appPreference.saveSyncStatus(Constants.USERSSYNC);
					 }
				 }
			 }
		 } else if (json.has(UPDATEARRAYNAME)){
			 JSONArray jsonArray = json.getJSONArray(UPDATEARRAYNAME);
			 for(int ui = 0; ui < jsonArray.length(); ui ++) {
				 JSONObject jsonObject = jsonArray.getJSONObject(ui);
				 String sid = jsonObject.getString(User.SYSTEMUSERID);
				 if(isUserPresent(context, sid) == 1){
				 String rl = jsonObject.getString(User.ROLE);
				 String lc = jsonObject.getString(User.LOCATION);
				 String st = jsonObject.getString(STATUS);
				 String ps = jsonObject.getString(User.PASSWORD);
				 String un = jsonObject.getString(User.USERNAME);
				 String ac = jsonObject.getString(User.ACCOUNT);
				 String[] vals = new String[]{sid, rl, lc, ps, un};
				 if (Validating.areSet(vals)){
						 int uu = updateUser(context, vals);
						 if (uu == 1) {
							 ack(context,1);
							 appPreference.saveSyncStatus(Constants.USERSSYNC);
						 }
				 }  else {
					 String subject = Constants.getIMEINO(context)+" null user";
					 String body = " User no "+sid+" username "+un+" account no" +ac+
							 " has null value " +
							 " today at "+Constants.getDate();
					 String[] msg = {subject,body};
					 try {
						 mail.sendEmail(msg);
					 } catch (Exception e) {
						 e.printStackTrace();
					 }
				 }
			 }  else {
					 String siid = jsonObject.getString(SYSTEMUSERID);
					 String un = jsonObject.getString(USERNAME);
					 String ps = jsonObject.getString(PASSWORD);
					 String rl = jsonObject.getString(ROLE);
					 String st = jsonObject.getString(STATUS);
					 String lc = jsonObject.getString(LOCATION);
					 String dc = jsonObject.getString(DATACOLLECTORNO);
					 String ac = jsonObject.getString(ACCOUNT);
					 String[] vals = new String[]{siid, un, ps, rl, st, lc, dc, ac};
					 if (Validating.areSet(vals)) {
						 if (isUserPresent(context, vals[0]) < 1) {
							 if (storeCredentials(context, vals) == 1) {
								 ack(context,1);
								 appPreference.saveSyncStatus(Constants.USERSSYNC);
							 }
						 } else {
							 ack(context,1);
							 appPreference.saveSyncStatus(Constants.USERSSYNC);
						 }
					 } else {
						 String subject = Constants.getIMEINO(context)+" null user";
						 String body = " User no "+sid+" username "+un+" account no" +ac+
								 " has null value " +
								 " today at "+Constants.getDate();
						 String[] msg = {subject,body};
						 try {
							 mail.sendEmail(msg);
						 } catch (Exception e) {
							 e.printStackTrace();
						 }
					 }
				 }
			 }
		 }
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
 }

}
