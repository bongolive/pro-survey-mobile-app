/**
 *
 */
package survery.database;

import java.util.ArrayList;
import java.util.zip.CheckedOutputStream;

import networking.prosurvey.Constants;
import networking.prosurvey.JsonObjectJsonArray;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import networking.prosurvey.Validating;
import tz.co.bongolive.apps.prosurvey.AppPreference;
import tz.co.bongolive.apps.prosurvey.Mail;
import tz.co.bongolive.apps.prosurvey.R;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;

/**
 *    @author nasznjoka
 *
 *
 *    Nov 15, 2014
 */
public class Responsechoice {

	public static final String TABLENAME = "responsechoice" ;

	public static final Uri BASEURI = Uri.parse("content://"+ContentProviderSurvey.AUTHORITY+"/"+TABLENAME) ;
	public static final String SPECIFIC_CONTENT_TYPE = "vnd.android.cursor.item/vnd.co.tz.prosurvey.responsechoice" ;
	public static final String GENERAL_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.co.tz.prosurvey.responsechoice" ;

	public static final String ID = "id";
	public static final String SYSTEMCHOICEID = "choice_id" ;
	public static final String QUESTIONNO = "question_no";
	public static final String RESPONSETEXT = "response_text";
	public static final String NEXT = "next_question";
	public static final String SURVEYNO = "survey_no";
	public static final String MATCHWITH = "match_with";
	static JsonObjectJsonArray js ;
	public static final String ARRAYNAME = "response_choices_array";
	public static final String UPDATEARRAYNAME = "update_response_choices_array";
	public static final String SYNCDATE = "syc_date";
	public static final String MINVALUE = "min_value";
	public static final String DATATYPE = "data_type";
	public static final String MAXVALUE = "max_value";
	public static final String REQUIRED = "required";
	public static final String CHOICENO = "choice_no";
	// language 
	public static final String TAG = Responsechoice.class.getName();

	public static int getCount(Context context){
		ContentResolver cr = context.getContentResolver() ;
		Cursor c = cr.query(BASEURI, null, null, null, null);
		int count = 0 ;
		try {
			count = c.getCount() ;
		} finally {
			if(c != null) {
				c.close();
			}
		}
		return count;
	}

	public static int storeResponseChoice(Context context, String[] credentials)
	{
		int value = getCount(context);
		ContentValues cv = new ContentValues();
		cv.put(SYSTEMCHOICEID, credentials[0]) ;
		cv.put(QUESTIONNO, credentials[1]);
		cv.put(RESPONSETEXT, credentials[2]);
		cv.put(NEXT, credentials[3]);
		cv.put(MATCHWITH, credentials[4]);
		cv.put(SURVEYNO, credentials[5]);
		cv.put(MINVALUE, credentials[6]);
		cv.put(MAXVALUE, credentials[7]);
		cv.put(DATATYPE, credentials[8]);
		cv.put(REQUIRED, credentials[9]);
		cv.put(CHOICENO, credentials[10]);
		ContentResolver cr = context.getContentResolver() ;
		cr.insert(BASEURI, cv);
		if(getCount(context) == value + 1) {
			return 1 ;
		}
		return 0 ;
	}

	public static String getChoiceNumber(Context context, String val)
	{
		String where = SYSTEMCHOICEID + " = " + val;
		Cursor c = context.getContentResolver().query(BASEURI, new String[]{CHOICENO}, where,null,
				QUESTIONNO + " ASC") ;
		try {
			if(c.getCount() > 0)
			{
				c.moveToFirst() ;
				String chno = c.getString(c.getColumnIndex(CHOICENO));
				Log.v("CHOICENUMBER"," from responcechoice is "+chno + " and count is "+c.getCount());
					return chno;
			}
		} finally {
			if(c != null) {
				c.close();
			}
		}
		Log.v("HAIPOOOO"," return 0");
		return "0" ;
	}

	public static int getDataType(Context context, String val)
	{
		String where = SYSTEMCHOICEID + " = " + val ;
		Cursor c = context.getContentResolver().query(BASEURI, new String[]{DATATYPE}, where,null,
				CHOICENO + " ASC") ;
		try {
			if(c.getCount() > 0)
			{
				c.moveToFirst() ;
				int chno = c.getInt(c.getColumnIndex(DATATYPE));
				Log.v("DATA TYPE"," is "+chno );
				return chno;
			}
		} finally {
			if(c != null) {
				c.close();
			}
		}
		return 0 ;
	}

	public static String getMin(Context context, String val)
	{
		String where = SYSTEMCHOICEID + " = " + val;
		Cursor c = context.getContentResolver().query(BASEURI, new String[]{MINVALUE}, where,null,
				null) ;
		try {
			if(c.getCount() > 0)
			{
				c.moveToFirst() ;
				String chno = c.getString(c.getColumnIndex(MINVALUE));
				Log.v("DATA TYPE"," from responcechoice is "+chno + " and count is "+c.getCount());
				return chno;
			}
		} finally {
			if(c != null) {
				c.close();
			}
		}
		Log.v("HAIPOOOO"," return 0");
		return "" ;
	}
	public static String getMax(Context context, String val)
	{
		String where = SYSTEMCHOICEID + " = " + val;
		Cursor c = context.getContentResolver().query(BASEURI, new String[]{MAXVALUE}, where,null,
				null) ;
		try {
			if(c.getCount() > 0)
			{
				c.moveToFirst() ;
				String chno = c.getString(c.getColumnIndex(MAXVALUE));
				Log.v("DATA TYPE"," from responcechoice is "+chno + " and count is "+c.getCount());
				return chno;
			}
		} finally {
			if(c != null) {
				c.close();
			}
		}
		return "" ;
	}
	public static int isResponsePresent(Context context, String vals)
	{
		String where = SYSTEMCHOICEID + " = " + vals ;
		Cursor c = context.getContentResolver().query(BASEURI, null, where,null,null) ;
		try {
			if(c.getCount() > 0)
			{
				return 1 ;
			}
		} finally {
			if(c != null) {
				c.close();
			}
		}
		return 0 ;
	}


	public static JSONObject getResponseChoice(Context context) throws JSONException {
		JSONObject jobs = new JSONObject();
		jobs.put("tag", Constants.TAGRESPONSECHOICE);
		jobs.put(Constants.IMEI, Constants.getIMEINO(context));
		js = new JsonObjectJsonArray();
		String job =  js.getJsonArray(Settings.getServerName(context), JsonObjectJsonArray.POST, jobs);
		JSONObject json;
		try {
			json = new JSONObject(job);
			if(json.length() != 0)
			{
				return json ;
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null ;
	}


	public static int updateVariousData(Context context, int data)
	{
		//flag a response to 3 ready to deleted it when synced
		ContentResolver cr = context.getContentResolver();
		ContentValues values = new ContentValues() ;
		values.put(REQUIRED , "1") ;
		String where =  QUESTIONNO + " = " + data ;
		if( cr.update(BASEURI, values, where, null) > 0) {
			return 1 ;
		}
		return 0;
	}

	public static void processQuestion(Context context) throws JSONException {
		JSONArray userArray ;
		Mail mail = new Mail();
		AppPreference appPreference = new AppPreference(context);
		try{
		JSONObject json = getResponseChoice(context);
		if(json.has(ARRAYNAME)) {
			userArray = json.getJSONArray(ARRAYNAME);

			if (!TextUtils.isEmpty(json.getString(Constants.SUCCESS))) {
				String res = json.getString(Constants.SUCCESS);
				if (Integer.parseInt(res) == 1) {
					int succss = 0;
					for (int i = 0; i < userArray.length(); i++) {
						JSONObject js = userArray.getJSONObject(i);
						String sid = js.getString(SYSTEMCHOICEID);
						String qn = js.getString(QUESTIONNO);
						String resp = js.getString(RESPONSETEXT);
						String nxt = js.getString(NEXT);
						String mt = js.getString(MATCHWITH);
						String surv = js.getString(SURVEYNO);
						String minv = js.getString(MINVALUE);
						String maxv = js.getString(MAXVALUE);
						String dt = js.getString(DATATYPE);
						String req = js.getString(REQUIRED);
						String chno = js.getString(CHOICENO);
						String[] vals = new String[]{sid, qn, resp, nxt, mt, surv,minv,maxv,dt,
								req,chno};
						if(Validating.areSet(vals)){
						if (isResponsePresent(context, vals[0]) != 1) {

							if(Validating.areSet(vals)) {
							
								int jk = storeResponseChoice(context, vals);
								if (jk == 1) {
									succss += 1;
								}
							}
						} else {
							succss +=1;
						}
						} else {
							String subject = Constants.getIMEINO(context)+" null reponsechoice";
							String body = " Choice no "+sid+" in question no "+qn+" survey no "+surv+" has null value " +
									" today at "+Constants.getDate();
							String[] msg = {subject,body};
							String[] logsdata = {"null value",body};
							LogsTable.insert(context,logsdata);
							try {
								mail.sendEmail(msg);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
					if (succss == userArray.length()) {
						ack(context,0);
						appPreference.saveSyncStatus(Constants.RESPONSESYNC);
					}
				}
			}
		} else if (json.has(UPDATEARRAYNAME)){
			JSONArray jsonArray = json.getJSONArray(UPDATEARRAYNAME);
			int succes = 0;
			int success_ = 0;
			for (int ii = 0; ii < jsonArray.length(); ii++) {
				JSONObject jsonObject = jsonArray.getJSONObject(ii);

				if (Responsechoice.isResponsePresent(context, jsonObject.getString(SYSTEMCHOICEID)) == 1) {
					//the response choice is there so update it
					String scid = jsonObject.getString(Responsechoice.SYSTEMCHOICEID);
					String restxt = jsonObject.getString(Responsechoice.RESPONSETEXT);
					String match = jsonObject.getString(Responsechoice.MATCHWITH);
					String next = jsonObject.getString(Responsechoice.NEXT);
					String minv = jsonObject.getString(MINVALUE);
					String maxv = jsonObject.getString(MAXVALUE);
					String dt = jsonObject.getString(DATATYPE);
					String req = jsonObject.getString(REQUIRED);
					String chno = jsonObject.getString(CHOICENO);
					String s = jsonObject.getString(SURVEYNO);
					String q = jsonObject.getString(QUESTIONNO);
					String[] vals = new String[]{scid, restxt, match, next,minv,maxv,dt,req,chno};
					if(Validating.areSet(vals)) {
					
						int jk = Responsechoice.updateResponseChoice(context, vals);
						if (jk == 1) {
							succes += 1;
						}
					} else {
						String subject = Constants.getIMEINO(context)+" null reponsechoice";
						String body = " Choice no "+scid+" in question no "+s+" survey no "+q+" has null value " +
								" today at "+Constants.getDate();
						String[] msg = {subject,body};
						String[] logsdata = {"null value",body};
						LogsTable.insert(context,logsdata);
						try {
							mail.sendEmail(msg);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				} else {
					//
					Log.v("responsechoicenew","the response choice is new on update flag " +
							""+jsonObject.getString(SYSTEMCHOICEID));
					String sid = jsonObject.getString(SYSTEMCHOICEID);
					String qn = jsonObject.getString(QUESTIONNO);
					String resp = jsonObject.getString(RESPONSETEXT);
					String nxt = jsonObject.getString(NEXT);
					String mt = jsonObject.getString(MATCHWITH);
					String surv = jsonObject.getString(SURVEYNO);
					String minv = jsonObject.getString(MINVALUE);
					String maxv = jsonObject.getString(MAXVALUE);
					String dt = jsonObject.getString(DATATYPE);
					String req = jsonObject.getString(REQUIRED);
					String cho = jsonObject.getString(CHOICENO);
					String[] vals = new String[]{sid, qn, resp, nxt, mt, surv,minv,maxv,dt,req,cho};
					if(Validating.areSet(vals)){
					if (isResponsePresent(context, vals[0]) < 1) {

						if(Validating.areSet(vals)) {
							int jk = storeResponseChoice(context, vals);
							if (jk == 1) {
								success_ += 1;
							}
						}
					} else {
						success_ += 1;
					}
					} else {
						String subject = Constants.getIMEINO(context)+" null reponsechoice";
						String body = " Choice no "+sid+" in question no "+qn+" survey no "+surv+" has null value " +
								" today at "+Constants.getDate();
						String[] msg = {subject,body};
						String[] logsdata = {"null value",body};
						LogsTable.insert(context,logsdata);
						try {
							mail.sendEmail(msg);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
			if(succes > 0 || success_ > 0){
				ack(context,1);
				appPreference.saveSyncStatus(Constants.RESPONSESYNC);
			}
		}
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
	}

	public static ArrayList<String> getQuestionAnswer(Context context, int questionno)
	{
		ContentResolver cr = context.getContentResolver();
		String where = QUESTIONNO + " = " +  questionno ;

		Cursor c = cr.query(BASEURI, null, where,null,CHOICENO + " ASC") ;
		ArrayList<String> answer = new ArrayList<String>();
		try {
			if(c.moveToFirst() )
			{
				do {
					String n = c.getString(c.getColumnIndexOrThrow(RESPONSETEXT));
					answer.add(n);
				}while( c.moveToNext()) ;
				return answer ;
			}
		} finally {
			if(c != null) {
				c.close();
			}
		}
		return null;
	}

	public static int updateResponseChoice(Context context, String[] data)
	{
		ContentResolver cr = context.getContentResolver();
		ContentValues cv = new ContentValues() ;
		cv.put(RESPONSETEXT, data[1]);
		cv.put(MATCHWITH, data[2]);
		cv.put(NEXT, data[3]);
		cv.put(MINVALUE, data[4]);
		cv.put(MAXVALUE, data[5]);
		cv.put(DATATYPE, data[6]);
		cv.put(REQUIRED, data[7]);
		cv.put(CHOICENO, data[8]);
		String where =  SYSTEMCHOICEID + " = " + data[0] ;
		if( cr.update(BASEURI, cv, where, null) > 0) {
			return 1 ;
		}
		return 0;
	}

	public static int isQuestionRequired(Context context, String data)
	{
		String where = SYSTEMCHOICEID + " = "+data;
		Cursor c = context.getContentResolver().query(BASEURI, null, where,null,null) ;
		try {
			if(c.getCount() > 0)
			{
				    c.moveToFirst();
					int qn = c.getInt(c.getColumnIndexOrThrow(REQUIRED));
				    return  qn;
			}
		} finally {
			if(c != null) {
				c.close();
			}
		}
		return 8 ;
	}

	public static JSONObject ack(Context context, int val) throws JSONException {
		JSONObject jobs = new JSONObject();
		jobs.put("tag", Constants.TAGACK);
		jobs.put("item", "responses");
		jobs.put(Constants.ACKTARGETKEY,val);
		jobs.put(Constants.IMEI, Constants.getIMEINO(context));
		js = new JsonObjectJsonArray();
		String job =  js.getJsonArray(Settings.getServerName(context), JsonObjectJsonArray.POST, jobs);
		JSONObject json;
		try {
			json = new JSONObject(job);
			if(json.length() != 0)
			{
				return json ;
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null ;
	}


	public static ArrayList<String> getQuestionAnswerId(Context context, int questionno, int survey)
	{
		ContentResolver cr = context.getContentResolver();
		String where = QUESTIONNO + " = " +  questionno +
				" AND "+SURVEYNO + " = "+ survey ;

		Cursor c = cr.query(BASEURI, null, where,null, CHOICENO + " ASC") ;
		ArrayList<String> answer = new ArrayList<String>();
		try {
			if(c.moveToFirst() )
			{
				do {
					String n = c.getString(c.getColumnIndexOrThrow(SYSTEMCHOICEID));
					answer.add(n);
				}while( c.moveToNext()) ;
				return answer ;
			}
		} finally {
			if(c != null) {
				c.close();
			}
		}
		return null;
	}




	public static int getNextQn(Context context, int questionno, String response)
	{
		ContentResolver cr = context.getContentResolver();
		String where = QUESTIONNO + " = " +  questionno + " AND "
				+ RESPONSETEXT + " LIKE "+ DatabaseUtils.sqlEscapeString(response) ;
//				+ RESPONSETEXT + " LIKE \'" + response +" \'" ;
		Cursor c = cr.query(BASEURI, null, where,null,CHOICENO + " ASC") ;
		int next ;
		try {
			if(c.moveToFirst() )
			{
				return c.getInt(c.getColumnIndexOrThrow(NEXT)) ;
			}
		} finally {
			if(c != null) {
				c.close();
			}
		}
		return 0;
	}
	public static int getNextQnPartialOpenEnded(Context context, int questionno, String choiceid)
	{
		ContentResolver cr = context.getContentResolver();
		String where = QUESTIONNO + " = " +  questionno + " AND "
				+ SYSTEMCHOICEID + " = " + choiceid;
		Cursor c = cr.query(BASEURI, null, where,null,CHOICENO + " ASC") ;
		int next ;
		try {
			if(c.moveToFirst() )
			{
				return c.getInt(c.getColumnIndexOrThrow(NEXT)) ;
			}
		} finally {
			if(c != null) {
				c.close();
			}
		}
		return 0;
	}

	public static int getNextQnId(Context context, String response)
	{
		ContentResolver cr = context.getContentResolver();
		String where = SYSTEMCHOICEID + " = " +  response ;
		Cursor c = cr.query(BASEURI, null, where,null,QUESTIONNO + " ASC") ;
		Log.v("NEXTQN", " kasa "+c.getCount());
		int next ;
		try {
			if(c.moveToFirst() )
			{
				return c.getInt(c.getColumnIndexOrThrow(NEXT)) ;
			}
		} finally {
			if(c != null) {
				c.close();
			}
		}
		return 0;
	}
}
