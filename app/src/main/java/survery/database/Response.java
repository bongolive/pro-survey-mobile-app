/**
 * 
 */
package survery.database;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import networking.prosurvey.Constants;
import networking.prosurvey.JsonObjectJsonArray;

/**
 *    @author nasznjoka
 *
 *    
 *    Nov 15, 2014
 */
public class Response {
	public static final String TABLENAME = "response" ;

	public static final Uri BASEURI = Uri.parse("content://"+ContentProviderSurvey.AUTHORITY+"/"+TABLENAME) ;
	public static final String SPECIFIC_CONTENT_TYPE = "vnd.android.cursor.item/vnd.co.tz.prosurvey.response" ;
	public static final String GENERAL_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.co.tz.prosurvey.response" ;

	public static final String ID = "response_id";
	public static final String PARTICIPANTID = "participant_id";
	public static final String QUESTIONNO = "question_no";
	public static final String PREVQUESTION = "prev_qn";
	public static final String SURVEYNO = "survey_no";
	public static final String COMMENTS = "comments";
	public static final String RESPONSE = "response";
	public static final String RESPONSEID = "responseid";
	public static final String CHOICENO = "choice_no";
	public static final String DATE = "response_date";
	public static final String ACK = "ack";
	public static final String JUMPFLAG = "jumpflag";
	public static final String JUMPRANK = "jumprank";
	public static final String SYNCDATE = "syc_date";
	private static final String TAG = Response.class.getName();

	public String partid,qnno,prevqn,survno,comm,respo,dt ;

	public Response(String pid, String qn, String prev, String surv, String com, String res, String dt)
	{
		this.partid = pid;
		this.qnno = qn ;
		this.prevqn = prev ;
		this.survno = surv ;
		this.comm = com;
		this.respo = res;
		this.dt = dt;
	}
	public Response(String pid,String qn,String surv,String res)
	{
		this.partid = pid;
		this.qnno = qn;
		this.survno = surv;
		this.respo = res ;
	}

	public Response(String p, String qns, String suv, String re, String prev) {
		this.partid = p;
		this.qnno = qns;
		this.survno = suv;
		this.respo = re;
		this.prevqn = prev;
	}

	/**
	 * @return the partid
	 */
	public String getPartid() {
		return partid;
	}

	/**
	 * @return the qnno
	 */
	public String getQnno() {
		return qnno;
	}

	/**
	 * @return the prevqn
	 */
	public String getPrevqn() {
		return prevqn;
	}

	/**
	 * @return the survno
	 */
	public String getSurvno() {
		return survno;
	}

	/**
	 * @return the respo
	 */
	public String getRespo() {
		return respo;
	}

	/**
	 * @return the dt
	 */
	public String getDt() {
		return dt;
	}


	private static final String ARRAYNAME = "responses";
	 public static JsonObjectJsonArray js ;

	 public static int getCount(Context context){
		    ContentResolver cr = context.getContentResolver() ;
			Cursor c = cr.query(BASEURI, null, null, null, null);
			int count = 0 ;
			try {
			count = c.getCount() ;
			} finally {
	 		if(c != null) {
		            c.close();
		        }
	 	}
			return count;
		}

	 public static int updateResponse(Context context, String[] string)
		{
			ContentResolver cr = context.getContentResolver();
			ContentValues values = new ContentValues() ;
			values.put(RESPONSE , string[5]) ;
			Log.v("CREDINTAL",string[6]);
			if(string[6].contains(Constants.SEPARATOR)){
				Log.v("HASSEPARATOR",string[6]);
				String[] choicearray = string[6].split("["+Constants.SEPARATOR+"]");

				ArrayList<String> choicelist = new ArrayList<>();
				for (String item : choicearray)
				{
					if(!item.isEmpty()){
						choicelist.add(Responsechoice.getChoiceNumber(context,
								item).concat(Constants
								.SEPARATOR));
						Log.v("CHOICELIST","ADDING CHOICE LIST VALUES " +Responsechoice
								.getChoiceNumber(context, item));
					}
				}
				String ans = "" ;
				for(int i = 0 ; i < choicelist.size(); i++)
				{
					ans += choicelist.get(i);
					Log.v("CONCATENATING","CONCATENATING THE CHOICE LIST TO PIPES");
				}
				if(!ans.isEmpty()){
					values.put(CHOICENO, ans);
					Log.v("CHOICENO"," answer is "+ans);
				}
			} else {

				Log.v("HASNOPIPE"," anwser is "+ string[6]);
				int choice = Integer.parseInt(string[6]);
				if (choice != 0) {
					Log.v("CHOICEISNOTZERO"," CHOIC IS "+ choice);
					String choiceno = Responsechoice.getChoiceNumber(context, string[6]);
					Log.v("CHOICENO"," choice number is "+choiceno);
					values.put(CHOICENO, choiceno);
				}
				else {
					Log.v("CHOICEISZERO"," CHOIC IS "+ choice);
					values.put(CHOICENO, string[6]);
					Log.v("CHOICENO"," anser is "+string[6]);
				}
			}
			values.put(RESPONSEID, string[6]);
			values.put(ACK, "2");
			String where =  PARTICIPANTID + " = " + string[0] +
			" AND "+ QUESTIONNO + " = " + string[1] + " AND "+
					SURVEYNO + " = " + string[3];
			if( cr.update(BASEURI, values, where, null) > 0) {
				Log.d("UPDATING", "THE QUESTION NO "+string[1] + " has been updated ");

				int returnint = getResponseId(context, new int[]{Integer.parseInt(string[1]),
						Integer.parseInt(string[0]),Integer.parseInt(string[3])});
				return returnint;
			}
			return 0;
		}
	public static int deleteResponse(Context context, int data)
	{
		int flag = getAckFlag(context, String.valueOf(data));
		if(flag != 1){
			Log.v("DELETE","This response is deleted "+data +" has a flag of "+flag);
			return deleteSyncedResponse(context, String.valueOf(data));
		} else {
			ContentResolver cr = context.getContentResolver();
			ContentValues values = new ContentValues() ;
			values.put(ACK , "3") ;
			String where =  ID + " = " + data ;
			if( cr.update(BASEURI, values, where, null) > 0) {
				Log.v("HIDE","This response is hidden "+data +" has a flag of "+flag);
				return 1 ;
			}
		}
		return 0;
	}

	public static int deleteSyncedResponse(Context context, String data)
	{
		//delete a response flagged 3 but already deleted on the server
		ContentResolver cr = context.getContentResolver();
		String where =  ID + " = " + data ;
		if( cr.delete(BASEURI,where,null) > 0) {
			return 1 ;
		}
		return 0;
	}
	public static int getAckFlag(Context context, String resID)
	{
		//return the ack flag given response id
		String where = ID + " = " + resID ;
		Cursor c = context.getContentResolver().query(BASEURI, null, where,null,null) ;
		try {
			if(c.getCount() > 0)
			{
				c.moveToFirst();
				int an = c.getInt(c.getColumnIndexOrThrow(ACK));
				Log.v(TAG, " ACK FLAG IS "+an) ;
				return an ;
			}
		} finally {
			if(c != null) {
				c.close();
			}
		}
		return 0 ;
	}
	public static int updateJumpFlag(Context context, int[] data)
	{
		//data[] = id,jumpflag,jumprank
		ContentResolver cr = context.getContentResolver();
		ContentValues values = new ContentValues() ;
		values.put(JUMPFLAG , data[1]) ;
		values.put(JUMPRANK , data[2]);
		String where =  ID + " = " + data[0] ;

		if( cr.update(BASEURI, values, where, null) > 0) {
			Log.v("updatejump","jump flag is updated to "+data[1] +" and rank is "+data[2]);
			return 1 ;
		}
		return 0;
	}

	public static int updateAck(Context context, String[] data)
	{
		ContentResolver cr = context.getContentResolver();
		int ack = getAckFlag(context,data[0]);
		if(ack == 3){
			if(deleteSyncedResponse(context,data[0]) == 1){
				return 1;
			}
		} else {
			ContentValues values = new ContentValues();
			values.put(ACK, "1");
			values.put(SYNCDATE, data[1]);
			String where = ID + " = " + data[0];
			if (cr.update(BASEURI, values, where, null) > 0) {
				return 1;
			}
		}
		return 0;
	}

	 public static int isResponseStored(Context context, String[] val)
	 {
		 String where = PARTICIPANTID + " = " + val[0]+	 " AND "+ QUESTIONNO + " = " + val[1] +
				 " AND "+ SURVEYNO + " = " + val[2]+ " AND "+ACK + " != 3";
		 Log.v("test","participant no "+val[0]+" question no "+val[1]+" survey no "+val[2]);
	 	Cursor c = context.getContentResolver().query(BASEURI, null, where,null,null) ;
	 	 try {
	 	if(c.getCount() > 0)
	 	{
	 				Log.v(TAG, " RESPONSE FOR QUESTION "+val[0]+ "  IS THERE ALREADY SKIP") ;
	 				return 1 ;
	 	}
		 } finally {
			 if(c != null) {
		            c.close();
		        }
		 }
	 	return 0 ;
	 }

	 public static String[] getAnswer(Context context, String[] val)
	 {
		 String where = PARTICIPANTID + " = " + val[0]+
				 " AND "+ QUESTIONNO + " = " + val[1]+
				 " AND "+ SURVEYNO + " = " + val[2] +
				 " AND "+ ACK + " != 3";
	 	Cursor c = context.getContentResolver().query(BASEURI, null, where,null,null) ;
	 	 try {
	 	if(c.getCount() > 0)
	 	{
	 		c.moveToFirst();
	 		String[] an = {c.getString(c.getColumnIndexOrThrow(RESPONSE)),
				    c.getString(c.getColumnIndexOrThrow(ID))};
	 				Log.v(TAG, " RESPONSE FOR QUESTION "+val[0]+ "  IS "+an) ;
	 				return an ;
	 	}
		 } finally {
			 if(c != null) {
		            c.close();
		        }
		 }
	 	return new String[]{"",""} ;
	 }


	public static int getPreviousQn(Context context, String[] val)
	 {
		 String where = PARTICIPANTID + " = " + val[3]+
				 " AND "+ QUESTIONNO + " = " + val[2]
						 + " AND " + SURVEYNO + " = " + val[5];
	 	Cursor c = context.getContentResolver().query(BASEURI, null, where,null,null) ;
	 	 try {
	 	if(c.getCount() > 0)
	 	{
	 		c.moveToFirst();
	 		int an = c.getInt(c.getColumnIndexOrThrow(PREVQUESTION));
	 				Log.v(TAG, " PREVIOUS QUESTION IS "+an) ;
	 				return an ;
	 	}
		 } finally {
			 if(c != null) {
		            c.close();
		        }
		 }
	 	return Integer.parseInt(val[1]) ;
	 }

	public static int getResponseId(Context context, int[] questionid)
	{
		String where = QUESTIONNO + " = " + questionid[0] +
				" AND " +PARTICIPANTID + " = " + questionid[1] +
				" AND "+ SURVEYNO + " = "+questionid[2];
		Cursor c = context.getContentResolver().query(BASEURI, null, where,null,null) ;
		try {
			if(c.getCount() > 0)
			{
				c.moveToFirst();
				int an = c.getInt(c.getColumnIndexOrThrow(ID));
				Log.v(TAG, " RESPONSE ID IS "+an) ;
				return an ;
			}
		} finally {
			if(c != null) {
				c.close();
			}
		}
		return 0 ;
	}
	public static int getJumpedRank(Context context, int[] questionid)
	{
		String where = QUESTIONNO + " = " + questionid[0] +
				" AND " +PARTICIPANTID + " = " + questionid[1] +
				" AND "+ SURVEYNO + " = "+questionid[2];
		Cursor c = context.getContentResolver().query(BASEURI, null, where,null,null) ;
		try {
			if(c.getCount() > 0)
			{
				c.moveToFirst();
				int an = c.getInt(c.getColumnIndexOrThrow(JUMPRANK));

				return an ;
			}
		} finally {
			if(c != null) {
				c.close();
			}
		}
		return 0 ;
	}

	 public static int storeResponse(Context context, String[] credentials) {

		 if(isResponseStored(context, new String[]{credentials[0],credentials[1],credentials[3]}) != 1){
		 int value = getCount(context);
		 ContentValues cv = new ContentValues();
		 cv.put(PARTICIPANTID, credentials[0]);
		 cv.put(QUESTIONNO, credentials[1]);
		 cv.put(PREVQUESTION, credentials[2]);
		 cv.put(SURVEYNO, credentials[3]);
		 cv.put(COMMENTS, credentials[4]);
		 cv.put(RESPONSE, credentials[5]);
			 if(credentials[6].contains(Constants.SEPARATOR)){
				 String[] choicearray = credentials[6].split("["+Constants.SEPARATOR+"]");

				 ArrayList<String> choicelist = new ArrayList<>();
				 for (String item : choicearray)
				 {
					 if(!item.isEmpty()){
						 choicelist.add(Responsechoice.getChoiceNumber(context,
								 item).concat(Constants
								 .SEPARATOR));
					 }
				 }
				 String ans = "" ;
				 for(int i = 0 ; i < choicelist.size(); i++)
				 {
					 ans += choicelist.get(i);
				 }
				 if(!ans.isEmpty()){
					 cv.put(CHOICENO, ans);
				 }
			 } else {

				 int choice = Integer.parseInt(credentials[6]);
				 if (choice != 0) {
					 String choiceno = Responsechoice.getChoiceNumber(context, credentials[6]);

					 cv.put(CHOICENO, choiceno);
				 }
				 else {
					 cv.put(CHOICENO, credentials[6]);
				 }
			 }
		 cv.put(RESPONSEID, credentials[6]);
		 cv.put(DATE, Constants.getDate());
		 ContentResolver cr = context.getContentResolver() ;
		 Uri uri = cr.insert(BASEURI, cv);
		 if(getCount(context) == value + 1) {
			 String resid =  String.valueOf(Long.parseLong(uri.getLastPathSegment()));
			  int returnint = Integer.parseInt(resid);
			 return returnint;
		 } else {
			 return 0;
		 }

		 } else {
			Log.w("response_present", " the question is already answered") ;

			return getResponseId(context, new int[]{Integer.parseInt(credentials[1]),
					Integer.parseInt(credentials[0]),Integer.parseInt(credentials[3])});
		 }
	 }

	 public static JSONObject getResponse(Context context) {
		    ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("tag", Constants.TAGRESPONSE)) ;
			params.add(new BasicNameValuePair(Constants.IMEI, Constants.getIMEINO(context))) ;
			js = new JsonObjectJsonArray();
		 JSONObject jobs = new JSONObject();
			String job =  js.getJsonArray(Settings.getServerName(context), JsonObjectJsonArray.POST, jobs);
			JSONObject json;
			try {
				json = new JSONObject(job);
				if(json.length() != 0)
				{
					return json ;
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null ;
		}

	   public static int editResponse(Context context, String[] req)
	   {
		   ContentResolver cr = context.getContentResolver() ;
		   ContentValues cv = new ContentValues();
		   cv.put(RESPONSE, req[0]) ;
		   cv.put(ACK, "2");
		   String where = QUESTIONNO + " = "+ req[1] +
				   " AND " + PARTICIPANTID + " = " + req[2] ;
		   int up = cr.update(BASEURI, cv, where, null) ;
		   if(up > 0)
		   {
			   return 1;
		   }
		   return 0;
	   }

	public static int resetSync(Context context)
	{
		ContentResolver cr = context.getContentResolver() ;
		ContentValues cv = new ContentValues();
		cv.put(ACK, "2");
		String where = ACK + " != 3";
		if(cr.update(BASEURI, cv, where, null) > 0)
		{
			return 1;
		}
		return 0;
	}


	   public static ArrayList<String> getAllResponsePerSurvey(Context context, int surveyno)
		 {
			 ContentResolver cr = context.getContentResolver();
			 String where = SURVEYNO + " = " +  surveyno ;

		    	Cursor c = cr.query(BASEURI, null, where,null,null) ;
		    	ArrayList<String> answer = new ArrayList<String>();
		    	try {
		    	if(c.moveToFirst() )
		    	{
		    		do {
		    			String n = c.getString(c.getColumnIndexOrThrow(RESPONSE));
		    			answer.add(n);
		    	}while( c.moveToNext()) ;
			    	return answer ;
		    	}
		    	} finally {
		    		if(c != null) {
			            c.close();
			        }
		    	}
				return null;
		 }
	public static int getNoResponse(Context context, String surveyno)
	{
		ContentResolver cr = context.getContentResolver();
		String where = SURVEYNO + " = " +  surveyno ;

		Cursor c = cr.query(BASEURI, null, where,null,null) ;
		try {
			if(c.moveToFirst() )
			{
				int dooo = 0;
				do {
					dooo += 1;
				}while( c.moveToNext()) ;
				return dooo ;
			}
		} finally {
			if(c != null) {
				c.close();
			}
		}
		return 0;
	}

	public static JSONObject processResponse(Context context) throws JSONException
	{
		ContentResolver cr = context.getContentResolver() ;
		String where = ACK + " != 1" ;
		Cursor c = cr.query(Response.BASEURI, null, where, null, null);
		JsonObjectJsonArray js = new JsonObjectJsonArray();
		try {
//			JSONArray JSONArray = new JSONArray();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("tag", Constants.TAGRESPONSE);
			jsonObject.put(Constants.IMEI, Constants.getIMEINO(context));
//			JSONArray.put(jsonObject);
			String array ;
			if(c.moveToFirst())
			{
				JSONArray jarr = new JSONArray();
				do{
					JSONObject j = new JSONObject();
					j.put(ID, c.getString(c.getColumnIndexOrThrow(ID)));
					j.put(PARTICIPANTID, c.getString(c.getColumnIndexOrThrow(PARTICIPANTID)));
					j.put(QUESTIONNO, c.getString(c.getColumnIndexOrThrow(QUESTIONNO)));
					j.put(PREVQUESTION, c.getString(c.getColumnIndexOrThrow(PREVQUESTION)));
					j.put(SURVEYNO, c.getString(c.getColumnIndexOrThrow(SURVEYNO)));
					j.put(COMMENTS, c.getString(c.getColumnIndexOrThrow(COMMENTS)));
					j.put(RESPONSE, c.getString(c.getColumnIndexOrThrow(RESPONSE)));
					j.put(CHOICENO, c.getString(c.getColumnIndexOrThrow(CHOICENO)));
					j.put(RESPONSEID, c.getString(c.getColumnIndexOrThrow(RESPONSEID)));
					j.put(DATE, c.getString(c.getColumnIndexOrThrow(DATE)));
					j.put(ACK, c.getString(c.getColumnIndexOrThrow(ACK)));
					jarr.put(j);
				} while(c.moveToNext()) ;
				jsonObject.put("response",(Object)jarr);
				array = js.getJsonArray(Settings.getServerName(context), JsonObjectJsonArray.POST,
					jsonObject) ;
//				Log.e("Response: ", "> " + jsonObject);
				JSONObject job =  new JSONObject(array);
				return job ;
			}
		}finally {
			if(c != null) {
				c.close();
			}
		}
		return null ;
	}

	public static void validateResponseSync(Context context) throws JSONException
	{
		JSONObject json = processResponse(context) ;
		try {
			if(!TextUtils.isEmpty(json.getString(Constants.SUCCESS))){
				String res = json.getString(Constants.SUCCESS) ;
				if(Integer.parseInt(res) == 1)
				{
					if(json.has(Constants.RESPONSEARRAY)){
						JSONObject job = json.getJSONObject(Constants.RESPONSEARRAY);
						if(job.has(ARRAYNAME)) {
							JSONArray jsonArray = job.getJSONArray(ARRAYNAME);
							for (int i = 0; i < jsonArray.length(); i++) {
								JSONObject jsonObject = jsonArray.getJSONObject(i);
								String resID = jsonObject.getString(ID);
								String sydate = jsonObject.getString(SYNCDATE);
								String[] vals = {resID, sydate};
								if (updateAck(context, vals) > 0) {

									Log.e(TAG, "RESPONSE IS SYNCED AT " + vals[1]);
								}
							}
						}
					}
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}


}