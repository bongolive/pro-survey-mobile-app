/**
 * 
 */
package survery.database;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import networking.prosurvey.Constants;
import networking.prosurvey.JsonObjectJsonArray;

/**
 *    @author nasznjoka
 *
 *    
 *    Nov 15, 2014
 */
public class Participant {
	public static final String TABLENAME = "participant" ;
    
	 public static final Uri BASEURI = Uri.parse("content://"+ContentProviderSurvey.AUTHORITY+"/"+TABLENAME) ;
	 public static final String SPECIFIC_CONTENT_TYPE = "vnd.android.cursor.item/vnd.co.tz.prosurvey.participant" ;
	 public static final String GENERAL_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.co.tz.prosurvey.participant" ;

	public static final String ID = "participant_id";//wrong
	public static final String SURVEYNO = "survey_no";
	public static final String LOCATIONNO = "location_no";
	public static final String INTERVIEWERNO = "interviewer_no";//data collector
	public static final String INTDATE = "interview_date";
	public static final String STARTTIME = "start_time";
	public static final String ENDTIME = "end_time";
	public static final String PARTICIPANTNO = "participant_no"; //participantno
	public static final String ANC = "anc"; 
	public static final String STATUS = "status";
	public static final String ACK = "ack";
	public static final String USERID = "user_id";
	public static final String SYNCDATE = "syc_date";
	//star/end/date/loc/pid

	private static final String TAG = Participant.class.getName();
	private static final String ARRAYNAME = "participants";

	String st,dt,loc,pid,datacollector,pcmct,anc ;
	
	public Participant(String sm, String date, String location, String partid, String pstatus, String ancid)
	{
		this.st = sm;
		this.dt = date;
		this.loc = location;
		this.pid = partid;
		this.pcmct = pstatus;
		this.anc = ancid;
	}

/**
 * @return the st
 */
public String getSt() {
	return st;
}

public String getDataCollector()
{
	return datacollector;
	}
/**
 * @return the dt
 */
public String getDt() {
	return dt;
}

	public String getAnc() {
		return anc;
	}

	public String getPcmct() {
		return pcmct;
	}

	/**
 * @return the loc
 */
public String getLoc() {
	return loc;
}

/**
 * @return the pid
 */
public String getPid() {
	return pid;
}


	public static int getCount(Context context){ 
	    ContentResolver cr = context.getContentResolver() ;
		Cursor c = cr.query(BASEURI, null, null, null, null);
		int count = 0 ;
		try {
		count = c.getCount() ;
		} finally {
 		if(c != null) {
	            c.close();
	        }
 	}  
		return count; 
	} 
 
 public static int storeParticipant(Context context, String[] credentials)
 {
	 int value = getCount(context);
	 ContentValues cv = new ContentValues();
	 cv.put(SURVEYNO, credentials[0]);
	 cv.put(LOCATIONNO, credentials[1]);
	 cv.put(INTERVIEWERNO, credentials[2]);
	 cv.put(INTDATE, Constants.getDateOnly());
	 cv.put(STARTTIME, Constants.getDate());
	 cv.put(ENDTIME, Constants.getDate());
	 cv.put(PARTICIPANTNO, credentials[3]);
	 cv.put(STATUS, credentials[4]);
	 cv.put(ANC, credentials[5]);
	 cv.put(USERID, credentials[6]);
	 ContentResolver cr = context.getContentResolver() ;
	 cr.insert(BASEURI, cv);
	 if(getCount(context) == value + 1) {
		 Log.v("PARTICIPANT_INSERTED", " SUCCESSFULLY INSERTED A PARTICIPANT\n" +
		 		"Start time is "+ Constants.getTimeOnly() + " AND DATE "+ Constants.getDateOnly());
		 return 1 ;
	 }
	 return 0 ;
 }

	public static int updateAck(Context context, String[] data)
	{
		ContentResolver cr = context.getContentResolver();
		ContentValues values = new ContentValues() ;
		values.put(ACK , "1") ;
		values.put(SYNCDATE, data[1]);
		String where =  ID + " = " + data[0] ;
		if( cr.update(BASEURI, values, where, null) > 0) {
			return 1 ;
		}
		return 0;
	}

	public static int resetSync(Context context)
	{
		ContentResolver cr = context.getContentResolver();
		ContentValues values = new ContentValues() ;
		values.put(ACK , "2") ;
		if( cr.update(BASEURI, values, null, null) > 0) {
			return 1 ;
		}
		return 0;
	}


 public static int insetEndtime(Context context, String[] string)
	{ 
		ContentResolver cr = context.getContentResolver();
		ContentValues values = new ContentValues() ;	 
		values.put(ENDTIME , Constants.getDate()) ;
        int ack = getParticipantAck(context,string[2]);
        Log.v("acknow", " ack now is "+ack);
        if(ack == 0){
            values.put(ACK, "0");
            Log.v("ackchosen", " ackchosen is 0");
        } else {
            values.put(ACK, "2");
            Log.v("ackchosen", " ack now is 2");
        }
		String where =  INTERVIEWERNO + " = " + string[1] +
		" AND "+ SURVEYNO + " = " + string[0] +
		" AND "+ PARTICIPANTNO + " = " + string[2] ;
		if( cr.update(BASEURI, values, where, null) > 0) {
			Log.d("UPDATING", "THE END TIME "+Constants.getTimeOnly() + " has been updated ");
			return 1 ; 
		}
		return 0;
	}

    public static int getParticipantAck(Context context,String part){
        ContentResolver cr = context.getContentResolver();
        String where = PARTICIPANTNO + " = "+ part;
        Cursor c = cr.query(BASEURI,null,where,null,null);
        try {
            if(c.moveToFirst()){
                return c.getInt(c.getColumnIndexOrThrow(ACK));
            }
        } finally {
            if(c != null) {
                c.close();
            }
        }
        return 0;
    }

	public static JSONObject processParticipant(Context context) throws JSONException
	{
		ContentResolver cr = context.getContentResolver() ;
		String where = ACK + " != 1" ;
		Cursor c = cr.query(Participant.BASEURI, null, where, null, null);
		JsonObjectJsonArray js = new JsonObjectJsonArray();
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("tag", Constants.TAGPARTICIPANT);
			jsonObject.put(Constants.IMEI, Constants.getIMEINO(context));
			String array ;
			if(c.moveToFirst())
			{
				JSONArray jarr = new JSONArray();
				do{
					JSONObject j = new JSONObject();
					j.put(ID, c.getString(c.getColumnIndexOrThrow(ID))) ;
					j.put(SURVEYNO, c.getString(c.getColumnIndexOrThrow(SURVEYNO))) ;
					j.put(LOCATIONNO, c.getString(c.getColumnIndex(LOCATIONNO))) ;
					j.put(INTERVIEWERNO, c.getString(c.getColumnIndexOrThrow(INTERVIEWERNO))) ;
					j.put(INTDATE, c.getString(c.getColumnIndexOrThrow(INTDATE))) ;
					j.put(STARTTIME, c.getString(c.getColumnIndexOrThrow(STARTTIME))) ;
					j.put(ENDTIME, c.getString(c.getColumnIndexOrThrow(ENDTIME))) ;
					j.put(PARTICIPANTNO, c.getString(c.getColumnIndexOrThrow(PARTICIPANTNO))) ;
					j.put(ANC, c.getString(c.getColumnIndexOrThrow(ANC))) ;
					j.put(STATUS, c.getString(c.getColumnIndexOrThrow(STATUS) )) ;
                    j.put(ACK, c.getString(c.getColumnIndexOrThrow(ACK) )) ;
					jarr.put(j);
				} while(c.moveToNext()) ;
				jsonObject.put("Participant",(Object)jarr);
				array = js.getJsonArray(Settings.getServerName(context), JsonObjectJsonArray.POST,
						jsonObject) ;

				Log.e("Servername: ", "is " + Settings.getServerName(context));
				JSONObject job =  new JSONObject(array);
				return job ;
			}
		}finally {
			if(c != null) {
				c.close();
			}
		}
		return null ;
	}
 public static int getDataCollector(Context context, String[] id){
	    ContentResolver cr = context.getContentResolver() ;
	    String where = PARTICIPANTNO + " = " + id[0] + " AND " + SURVEYNO + " = " + id[1] ;
		Cursor c = cr.query(BASEURI, null, where, null, null); 
		try {
			if(c.moveToFirst()) { 
		 return c.getInt(c.getColumnIndexOrThrow(INTERVIEWERNO)); 
			}
		} finally {
		if(c != null) {
	            c.close();
	        }
	}
		return 0;   
	}

	public static int getUserId(Context context, String id, String survey){
		ContentResolver cr = context.getContentResolver() ;
		String where = INTERVIEWERNO + " = " + id +
				" AND "+ SURVEYNO + " = " + survey;
		Cursor c = cr.query(BASEURI, null, where, null, null);
		try {
			if(c.moveToFirst()) {
				return c.getInt(c.getColumnIndexOrThrow(USERID));
			}
		} finally {
			if(c != null) {
				c.close();
			}
		}
		return 0;
	}

	public static int isParticipantActive(Context context, String[] vals)
 {
	 String where = SURVEYNO + " = " + vals[0] + " AND " +
//			 INTERVIEWERNO + " = " + vals[2] + " AND " +
			 PARTICIPANTNO + " = " + vals[3];
 	Cursor c = context.getContentResolver().query(BASEURI, null, where,null,null) ;
 	 try { 
 	if(c.getCount() > 0)
 	{
 		Log.v(TAG, "PARTICIPANT IS INACTIVE") ;
 				return 1 ; 
 	} 
	 } finally {
		 if(c != null) {
	            c.close();
	        }
	 }
 	return 0 ;
 }

	public static void validatePartiSync(Context context) throws JSONException
	{
		JSONObject json = processParticipant(context) ;
		try {
			if(!TextUtils.isEmpty(json.getString(Constants.SUCCESS))){
				String res = json.getString(Constants.SUCCESS) ;
				if(Integer.parseInt(res) == 1)
				{
					        if(json.has(Constants.PARTICIPANTARRAY)){
						        JSONObject job = json.getJSONObject(Constants.PARTICIPANTARRAY);
						        if(job.has(ARRAYNAME)) {
							        JSONArray jsonArray = job.getJSONArray(ARRAYNAME);
							        for (int i = 0; i < jsonArray.length(); i++) {
								        JSONObject jsonObject = jsonArray.getJSONObject(i);
								        String resID = jsonObject.getString(ID);
								        String sydate = jsonObject.getString(SYNCDATE);
								        String[] vals = {resID, sydate};
								        if (updateAck(context, vals) > 0) {
									        Log.e(TAG, "PARTICIPANT IS SYNCED AT " + vals[1]);
								        }
							        }
						        }
					}
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

}
