/**
 * 
 */
package survery.database;

import java.util.ArrayList;

import networking.prosurvey.Constants;
import networking.prosurvey.JsonObjectJsonArray;
import networking.prosurvey.Validating;
import tz.co.bongolive.apps.prosurvey.AppPreference;
import tz.co.bongolive.apps.prosurvey.Mail;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

/**
 *    @author nasznjoka
 *
 *    
 *    Nov 15, 2014
 */
public class Questions {

	 public static final String TABLENAME = "question" ;
	    
	 public static final Uri BASEURI = Uri.parse("content://"+ContentProviderSurvey.AUTHORITY+"/"+TABLENAME) ;
	 public static final String SPECIFIC_CONTENT_TYPE = "vnd.android.cursor.item/vnd.co.tz.prosurvey.question" ;
	 public static final String GENERAL_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.co.tz.prosurvey.question" ;

	 public static final String ID = "qid";
	 public static final String SYSTEMQUESTIONID = "question_id" ; 
	 public static final String SURVEYNO = "survey_no";
	 public static final String QUESTION = "question_text";
	 public static final String CATEGORY = "category";
	 public static final String RANK = "question_rank";
	 public static final String TYPE = "question_type"; 
	 public static final String LANGUAGE = "language";
	public static final String SYNCDATE = "syc_date";
	public static final String REQUIRED = "required";

	public static final String ARRAYNAME = "questions_data_array";
	public static final String UPDATEARRAYNAME = "update_questions_data_array";

	private static final String TAG = Questions.class.getName();
	static JsonObjectJsonArray js ;
	private Mail mail;
	 
	 public static int getCount(Context context){ 
		    ContentResolver cr = context.getContentResolver() ;
			Cursor c = cr.query(BASEURI, null, null, null, null);
			int count = 0 ;
			try {
			count = c.getCount() ;
			} finally {
	 		if(c != null) {
		            c.close();
		        }
	 	}  
			return count; 
		} 
	 
	 public static int storeQuestion(Context context, String[] credentials)
	 {
		 int value = getCount(context);
		 ContentValues cv = new ContentValues();
		 cv.put(SYSTEMQUESTIONID, credentials[0]) ; 
		 cv.put(SURVEYNO, credentials[1]);
		 cv.put(QUESTION, credentials[2]);
		 cv.put(CATEGORY, credentials[3]);
		 cv.put(RANK, credentials[4]);
		 cv.put(TYPE, credentials[5]); 
		 cv.put(LANGUAGE, credentials[6]);
		 cv.put(REQUIRED, credentials[7]);
		 ContentResolver cr = context.getContentResolver() ;
		 cr.insert(BASEURI, cv);
		 if(getCount(context) == value + 1) {
//			 Log.v(TAG, "DATA INSERTED");
			 return 1 ;
		 }
		 return 0 ;
	 }

	public static int updateQuestion(Context context, String[] data)
	{
		ContentResolver cr = context.getContentResolver();
		ContentValues cv = new ContentValues() ;
		cv.put(QUESTION, data[1]);
		cv.put(TYPE, data[2]);
		cv.put(LANGUAGE, data[3]);
		cv.put(RANK, data[4]);
		cv.put(SURVEYNO, data[5]);
		cv.put(REQUIRED, data[6]);
		String where =  SYSTEMQUESTIONID + " = " + data[0] ;
		if( cr.update(BASEURI, cv, where, null) > 0) {
			return 1 ;
		}
		return 0;
	}

	public static JSONObject ack(Context context,int val) throws JSONException {
		JSONObject jobs = new JSONObject();
		jobs.put("tag", Constants.TAGACK);
		jobs.put("item","questions");
		jobs.put(Constants.ACKTARGETKEY,val);//val = 'normal' = 0 val = 'update' = 1
		jobs.put(Constants.IMEI, Constants.getIMEINO(context));
		js = new JsonObjectJsonArray();
		String job =  js.getJsonArray(Settings.getServerName(context), JsonObjectJsonArray.POST, jobs);
		JSONObject json;
		try {
			json = new JSONObject(job);
			if(json.length() != 0)
			{
				return json ;
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null ;
	}
	 
	 public static int isQnPresent(Context context, String vals)
	 {
		 String where = SYSTEMQUESTIONID + " = " + vals;
	 	Cursor c = context.getContentResolver().query(BASEURI, null, where,null,null) ;
	 	 try { 
	 	if(c.getCount() > 0)
	 	{ 
//	 				Log.v(TAG, "QUESTIONS "+vals[0]+ "  ARE THERE ALREADY SKIP") ;
	 				return 1 ; 
	 	} 
		 } finally {
			 if(c != null) {
		            c.close();
		        }
		 }
	 	return 0 ;
	 } 

	 
	 public static JSONObject getQuestions(Context context) throws JSONException {
		    JSONObject jobs = new JSONObject();
		    jobs.put("tag", Constants.TAGQUESTION);
		 jobs.put(Constants.IMEI, Constants.getIMEINO(context));
			js = new JsonObjectJsonArray();
			String job =  js.getJsonArray(Settings.getServerName(context), JsonObjectJsonArray.POST, jobs);
			JSONObject json;
			try {
				json = new JSONObject(job);
				if(json.length() != 0)
				{
					return json ;
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null ;
		}



	 public static void processQuestion(Context context) throws JSONException {
		 JSONArray userArray;
		 JSONObject json = getQuestions(context);
		 Mail mail = new Mail();
		 AppPreference appPreference = new AppPreference(context);
		 if(json != null)
		 try {
		 if (json.has(ARRAYNAME)) {
			 userArray = json.getJSONArray(ARRAYNAME);

				 if (!TextUtils.isEmpty(json.getString(Constants.SUCCESS))) {
					 String res = json.getString(Constants.SUCCESS);
					 if (Integer.parseInt(res) == 1) {
						 int success = 0;
						 for (int i = 0; i < userArray.length(); i++) {
							 JSONObject js = userArray.getJSONObject(i);
							 String si = js.getString(SYSTEMQUESTIONID);
							 String sv = js.getString(SURVEYNO);
							 String qt = js.getString(QUESTION);
							 String ct = js.getString(CATEGORY);
							 String rk = js.getString(RANK);
							 String ty = js.getString(TYPE);
							 String ln = js.getString(LANGUAGE);
							 String rq = "";
							 if(js.has(REQUIRED)) {
								 rq = js.getString(REQUIRED);
							 } else {
								 rq = "0";
							 }
//							 if(sv.equals("1"))
//							 Log.v("debug"," language "+ln+" rank "+rk+" survey "+sv+" question text "+qt+ " qid "+si);
							 String[] vals = new String[]{si, sv, qt, ct, rk, ty, ln,rq};
							 if(Validating.areSet(vals)) {
								 if (isQnPresent(context, vals[0]) < 1) {
									 if (storeQuestion(context, vals) == 1) {
										 success += 1;
									 }
								 } else {
									 success +=1;
								 }
							 } else {
								 //send mail here
								 String subject = Constants.getIMEINO(context)+" null question";
								 String body = " Question no "+si+" in survey no "+sv+" has null value " +
										 " today at "+Constants.getDate();
								 String[] logsdata = {"null value",body};
								 LogsTable.insert(context,logsdata);
								 StringBuffer buffer = new StringBuffer();
								 for(int j = 0; j< vals.length; j++){
									 buffer.append(vals[j]);
								 }
								 body = body.concat(" the contents of this question is "+buffer.toString());
								 String[] msg = {subject,body};
								 try {
									 mail.sendEmail(msg);
								 } catch (Exception e) {
									 e.printStackTrace();
								 }
							 }
						 }
						 if (success == userArray.length()) {
							 ack(context,0);
							 appPreference.saveSyncStatus(Constants.QUESTIONSYNC);
						 }
					 }
				 }
			 }

		 else if (json.has(UPDATEARRAYNAME)) {
			 JSONArray jsonArray = json.getJSONArray(UPDATEARRAYNAME);
			 int succes = 0;
			 int success_ = 0;
			 for (int qi = 0; qi < jsonArray.length(); qi++) {
				 JSONObject js = jsonArray.getJSONObject(qi);
				 if (Questions.isQnPresent(context, js.getString(SYSTEMQUESTIONID)) > 0) {//the question exists
				 String si = js.getString(Questions.SYSTEMQUESTIONID);
				 String qt = js.getString(Questions.QUESTION);
				 String ty = js.getString(Questions.TYPE);
				 String ln = js.getString(Questions.LANGUAGE);
				 String rank = js.getString(Questions.RANK);
				 String survno = js.getString(Questions.SURVEYNO);
//					 String rq = js.getString(REQUIRED );
					 String rq = "";
					 if(js.has(REQUIRED)) {
						 rq = js.getString(REQUIRED);
					 } else {
						 rq = "0";
					 }
				 String[] vals = new String[]{si, qt, ty, ln, rank, survno,rq};
					 if(Validating.areSet(vals)) {
						 int k = Questions.updateQuestion(context, vals);
						 if (k == 1) {
							 succes += 1;
						 }
					 } else {
						 //send mail here
						 String subject = Constants.getIMEINO(context)+" null question";
						 String body = " Question no "+si+" in survey no "+survno+" has null value " +
								 " today at "+Constants.getDate();
						 body.concat(" the contents of this question is "+vals);
						 String[] msg = {subject,body};
						 String[] logsdata = {"null value",body};
						 LogsTable.insert(context,logsdata);
						 try {
							 mail.sendEmail(msg);
						 } catch (Exception e) {
							 e.printStackTrace();
						 }
					 }
				 } else { // its an update but the question is new
					 String si = js.getString(SYSTEMQUESTIONID);
					 String sv = js.getString(SURVEYNO);
					 String qt = js.getString(QUESTION);
					 String ct = js.getString(CATEGORY);
					 String rk = js.getString(RANK);
					 String ty = js.getString(TYPE);
					 String ln = js.getString(LANGUAGE);
//					 String rq = js.getString(REQUIRED );
					 String rq = "";
					 if(js.has(REQUIRED)) {
						 rq = js.getString(REQUIRED);
					 } else {
						 rq = "0";
					 }
					 String[] vals = new String[]{si, sv, qt, ct, rk, ty, ln,rq};
					 if(Validating.areSet(vals)) {
						 if (isQnPresent(context, vals[0]) < 1) {
							 if (storeQuestion(context, vals) == 1) {
								 success_ += 1;
							 }
						 } else {
							 success_ += 1;
						 }
					 } else {
						 //send mail here
						 String subject = Constants.getIMEINO(context)+" null question";
						 String body = " Question no "+si+" in survey no "+sv+" has null value " +
								 " today at "+Constants.getDate();
						 body.concat(" the contents of this question is "+vals);
						 String[] msg = {subject,body};
						 String[] logsdata = {"null value",body};
						 LogsTable.insert(context,logsdata);
						 try {
							 mail.sendEmail(msg);
						 } catch (Exception e) {
							 e.printStackTrace();
						 }
					 }
				 }

			 }
			 if(succes > 0 || success_ > 0){
				 ack(context,1);
				 appPreference.saveSyncStatus(Constants.QUESTIONSYNC);
			 }
		 }
	 } catch (JSONException e) {
			 e.printStackTrace();
		 }
	 }
	 

 public static String getQuestionText(Context context, String[] data)
	 {
		 String lang = data[2];
		 if(lang.equals("1") || lang.equals("0")){
			 lang = "en";
		 }
		 String where = RANK + " = " + data[0] +
				 " AND " + SURVEYNO + " = " + data[1]
				 +" AND " + LANGUAGE + " = " + DatabaseUtils.sqlEscapeString(lang);
		 // it was system questio id some questions fails to fetch it restored rank 5/2/2015
		 	Cursor c = context.getContentResolver().query(BASEURI, null, where,null,null) ;
		 	 try { 
		 	if(c.getCount() > 0)
		 	{ 
		 		while(c.moveToFirst()) {
		 				String qn = c.getString(c.getColumnIndexOrThrow(QUESTION));
		 				return qn ;
		 		} c.moveToNext() ;
		 	}
			 } finally {
				 if(c != null) {
			            c.close();
			        }
			 }
		 	return null ;
	 }

	public static String getQuestionRankUsingId(Context context, String id)
	{
		String where = SYSTEMQUESTIONID + " = " + id ;
		Cursor c = context.getContentResolver().query(BASEURI, null, where,null,null) ;
		try {
			if(c.getCount() > 0)
			{
				while(c.moveToFirst()) {
					String qn = c.getString(c.getColumnIndexOrThrow(RANK));
					return qn ;
				} c.moveToNext() ;
			}
		} finally {
			if(c != null) {
				c.close();
			}
		}
		return null ;
	}

	public static String getQuestionTextLanguage(Context context, int[] data)
	{
		String where =  SURVEYNO + " = " + data[1] +
				" AND "+ SYSTEMQUESTIONID + " = " + data[0];
		Cursor c = context.getContentResolver().query(BASEURI, null, where,null,null) ;
		try {
			if(c.getCount() > 0)
			{
				while(c.moveToFirst()) {
					String qn;
						qn = c.getString(c.getColumnIndexOrThrow(QUESTION));
					return qn ;
				} c.moveToNext() ;
			}
		} finally {
			if(c != null) {
				c.close();
			}
		}
		return "" ;
	}


	public static int getQuestionId(Context context, int[] questionno, String lang)
	{
		ContentResolver cr = context.getContentResolver();
		if(lang.equals("1") || lang.equals("0")){
			lang = "en";
		}

		Log.v("language",lang +" is language");
		String  where = RANK + " = " + questionno[0] + " AND "
				+ SURVEYNO + " = " + questionno[1] + " AND "
				+ LANGUAGE + " = " + DatabaseUtils.sqlEscapeString(lang);
		Cursor c = cr.query(BASEURI, null, where,null,null) ;
		try {
			if(c.moveToFirst() )
			{
				return c.getInt(c.getColumnIndexOrThrow(SYSTEMQUESTIONID)) ;
			}
		} finally {
			if(c != null) {
				c.close();
			}
		}
		return 0;
	}

	 public static String getQuestionType(Context context, int rank, String survey)
	 {
		 String where = RANK + " = " + rank + " AND " + SURVEYNO + " = "+ survey;
		 	Cursor c = context.getContentResolver().query(BASEURI, null, where,null,null) ;
		 	 try { 
		 	if(c.getCount() > 0)
		 	{ 
		 		while(c.moveToFirst()) {
		 				String qn = c.getString(c.getColumnIndexOrThrow(TYPE)); 
		 				return qn ;
		 		} c.moveToNext() ;
		 	} 
			 } finally { 
				 if(c != null) {
			            c.close();
			        } 
			 }
		 	return null ;
	 }

	public static int isQuestionRequired(Context context, int questionid)
	{
		String where = SYSTEMQUESTIONID + " = " + questionid ;
		Cursor c = context.getContentResolver().query(BASEURI, null, where,null,null) ;
		try {
			if(c.getCount() > 0)
			{
				while(c.moveToFirst()) {
//					String qn = c.getString(c.getColumnIndexOrThrow(REQUIRED));

				} c.moveToNext() ;
			}
		} finally {
			if(c != null) {
				c.close();
			}
		}
		return 8 ;
	}

	public static String getQuestionTypeUsingQnId(Context context, int qid)
	{
		String where = SYSTEMQUESTIONID + " = " + qid;
		Cursor c = context.getContentResolver().query(BASEURI, null, where,null,null) ;
		try {
			if(c.getCount() > 0)
			{
				while(c.moveToFirst()) {
					String qn = c.getString(c.getColumnIndexOrThrow(TYPE));
					return qn ;
				} c.moveToNext() ;
			}
		} finally {
			if(c != null) {
				c.close();
			}
		}
		return null ;
	}


	public static ArrayList<String> getQuestionsArray(Context context, int survery)
	{
		ContentResolver cr = context.getContentResolver();
		String where = SURVEYNO + " = " +  survery ;

		Cursor c = cr.query(BASEURI, null, where,null,null) ;
		ArrayList<String> answer = new ArrayList<String>();
		try {
			if(c.moveToFirst() )
			{
				do {
					String n = c.getString(c.getColumnIndexOrThrow(SYSTEMQUESTIONID));
					answer.add(n);
				}while( c.moveToNext()) ;
				return answer ;
			}
		} finally {
			if(c != null) {
				c.close();
			}
		}
		return null;
	}
}
