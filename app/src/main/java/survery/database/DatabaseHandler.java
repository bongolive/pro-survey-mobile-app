/**
 * 
 */
package survery.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 *    @author nasznjoka
 *
 *    
 *    Nov 15, 2014
 */
public class DatabaseHandler extends SQLiteOpenHelper{

	private static String DBNAME = "prosurvey";
	private static int DBVERSION = 2 ;
	private static DatabaseHandler mInstance = null;
	
	public static DatabaseHandler getInstance(Context ctx) { 
	    if (mInstance == null) {
	      mInstance = new DatabaseHandler(ctx.getApplicationContext());
	    }
	    return mInstance;
	  }  
	

	public DatabaseHandler(Context context) { 
		super(context, DBNAME, null, DBVERSION); 
	}

	/* (non-Javadoc)
	 * @see android.database.sqlite.SQLiteOpenHelper#onCreate(android.database.sqlite.SQLiteDatabase)
	 */
	@Override
	public void onCreate(SQLiteDatabase db) {
		final String USERTABLE = "CREATE TABLE "+User.TABLENAME + " ("+
				 User.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"+ 
				 User.SYSTEMUSERID + " INTEGER ," + 
				 User.USERNAME + " VARCHAR(50) ," +
				 User.PASSWORD + " VARCHAR(50) ," +
				 User.ROLE + " INTEGER ," + 
				 User.STATUS + " INTEGER ," +
				 User.ACCOUNT + " INTEGER ," +
				 User.LOCATION + " VARCHAR(50)," +
				 User.ADMINNORMALSTATUS + " INTEGER DEFAULT 0, " +
				 User.DATACOLLECTORNO + " VARCHAR(50)  )"; 
		
		final String SURVEYTABLE = "CREATE TABLE "+Survey.TABLENAME + " ("+
				 Survey.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"+ 
				 Survey.SURVEYNO + " INTEGER ," +
				 Survey.TIMEINITIAL + " INTEGER ," +
				 Survey.TIMEFINAL + " INTEGER ," + 
				 Survey.TITLE + " VARCHAR(50) ," +
				 Survey.CAP +" INTEGER ,"+
				 Survey.ACCOUNT + " INTEGER ," +
				 Survey.QUESTIONS + " INTEGER ," +
				 Survey.STATUS + " INTEGER, " +
				 Survey.KEYWORD + " VARCHAR(50), " +
			   	Survey.ASSIGNMENT + " INTEGER DEFAULT 0, " +
				 Survey.DESC + " VARCHAR(50) )" ;
		

		final String PARTICIPANTTABLE = "CREATE TABLE "+Participant.TABLENAME + " ("+
				 Participant.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"+ 
				 Participant.SURVEYNO + " INTEGER ," +
				 Participant.LOCATIONNO + " INTEGER ," +
				 Participant.INTERVIEWERNO + " INTEGER ," + 
				 Participant.INTDATE + " DATETIME DEFAULT CURRENT_TIMESTAMP ," +
				 Participant.STARTTIME + " INTEGER , "+
				 Participant.ENDTIME +" INTEGER DEFAULT CURRENT_TIMESTAMP, "+
				 Participant.STATUS +" INTEGER, "+
				 Participant.ANC +" INTEGER, "+
				 Participant.USERID +" INTEGER, "+
				 Participant.SYNCDATE +" VARCHAR(50), "+
				 Participant.ACK +" INTEGER DEFAULT 0, "+
				 Participant.PARTICIPANTNO + " VARCHAR(50) )" ;
		
		final String RESPONSECHOICETABLE = "CREATE TABLE "+Responsechoice.TABLENAME + " ("+
				 Responsechoice.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"+ 
			     Responsechoice.SYSTEMCHOICEID + " INTEGER, " +
				 Responsechoice.QUESTIONNO + " INTEGER ," +
				Responsechoice.SURVEYNO + " INTEGER ," +
				 Responsechoice.RESPONSETEXT + " TEXT ," +
				 Responsechoice.NEXT + " INTEGER ," +
				Responsechoice.MINVALUE + " INTEGER ," +
				Responsechoice.MAXVALUE + " INTEGER ," +
				Responsechoice.CHOICENO + " INTEGER ," +
				Responsechoice.REQUIRED + " INTEGER ," +
				Responsechoice.DATATYPE + " VARCHAR(50) ," +
				 Responsechoice.MATCHWITH + " VARCHAR(50) )" ;
		
		final String RESPONSETABLE = "CREATE TABLE "+Response.TABLENAME + " ("+
				 Response.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"+ 
				 Response.PARTICIPANTID + " INTEGER ," +
				 Response.QUESTIONNO + " INTEGER ," +
				 Response.PREVQUESTION + " INTEGER ," + 
				 Response.SURVEYNO + " INTEGER ," +
				 Response.COMMENTS + " VARCHAR(50) ,"+
				 Response.RESPONSE +" TEXT ,"+
				 Response.RESPONSEID +" INTEGER DEFAULT 0 ,"+
				 Response.CHOICENO +" INTEGER DEFAULT 0 ,"+
				 Response.SYNCDATE +" VARCHAR(50), "+
				 Response.ACK +" INTEGER DEFAULT 0, "+
				 Response.JUMPFLAG +" INTEGER DEFAULT 0, "+
				 Response.JUMPRANK +" INTEGER DEFAULT 0, "+
				 Response.DATE + " DATETIME DEFAULT CURRENT_TIMESTAMP )" ;
		
		final String QUESTIONSTABLE = "CREATE TABLE "+Questions.TABLENAME + " ("+
				 Questions.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"+ 
				 Questions.SYSTEMQUESTIONID + " INTEGER ," +  
				 Questions.SURVEYNO+ " INTEGER ," +
				 Questions.QUESTION + " TEXT ," + 
				 Questions.CATEGORY + " VARCHAR(50) ," +
				 Questions.RANK + " INTEGER ,"+
				 Questions.REQUIRED + " INTEGER ,"+
				 Questions.TYPE +" INTEGER ,"+
		         Questions.LANGUAGE + " VARCHAR(50) )" ;

		final String SETTINGSTABLE = "CREATE TABLE "+Settings.TABLENAME + " ("+
				Settings.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"+
				Settings.SYSTEMID + " INTEGER ," +
				Settings.ACCOUNT+ " INTEGER ," +
				Settings.ACTIVESURVEY + " INTEGER ," +
				Settings.SKIPVALUE + " VARCHAR(50) ," +
				Settings.SERVER + " VARCHAR(50) ," +
				Settings.SMSREPORT + " INTEGER ,"+
				Settings.LANGUAGE + " VARCHAR(50) )" ;

		final String LOGS_TABLE = "CREATE TABLE "+LogsTable.TABLENAME + " ("+
				LogsTable.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"+
				LogsTable.SOURCE + " VARCHAR(100) ," +
				LogsTable.ISSUE+ " VARCHAR(100) ," +
				LogsTable.WHEN + " DATETIME ," +
				LogsTable.TYPE + " INTEGER DEFAULT 0 )" ;
		
		db.execSQL(USERTABLE);
		System.out.println("database is created " + USERTABLE);
		db.execSQL(SURVEYTABLE);
		System.out.println("database is created " + SURVEYTABLE);
		db.execSQL(PARTICIPANTTABLE);
		System.out.println("database is created " + PARTICIPANTTABLE);
		db.execSQL(RESPONSECHOICETABLE);
		System.out.println("database is created " + RESPONSECHOICETABLE);
		db.execSQL(RESPONSETABLE);
		System.out.println("database is created " + RESPONSETABLE);
		db.execSQL(QUESTIONSTABLE);
		System.out.println("database is created " + QUESTIONSTABLE);
		db.execSQL(SETTINGSTABLE);
		System.out.println("database is created " + SETTINGSTABLE);
		db.execSQL(LOGS_TABLE);
		System.out.println("database is created " + LOGS_TABLE);
	}

	/* (non-Javadoc)
	 * @see android.database.sqlite.SQLiteOpenHelper#onUpgrade(android.database.sqlite.SQLiteDatabase, int, int)
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		/* db.execSQL("DROP TABLE IF EXISTS "+ User.TABLENAME);
		 db.execSQL("DROP TABLE IF EXISTS "+ Survey.TABLENAME);
		 db.execSQL("DROP TABLE IF EXISTS "+ Response.TABLENAME);
		 db.execSQL("DROP TABLE IF EXISTS "+ Questions.TABLENAME);
		 db.execSQL("DROP TABLE IF EXISTS "+ Settings.TABLENAME);
		 db.execSQL("DROP TABLE IF EXISTS "+ Participant.TABLENAME);
		 db.execSQL("DROP TABLE IF EXISTS "+ LogsTable.TABLENAME);
		 db.execSQL("DROP TABLE IF EXISTS "+ Responsechoice.TABLENAME);*/
		final String LOGS_TABLE = "CREATE TABLE "+LogsTable.TABLENAME + " ("+
				LogsTable.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"+
				LogsTable.SOURCE + " VARCHAR(100) ," +
				LogsTable.ISSUE+ " VARCHAR(100) ," +
				LogsTable.WHEN + " DATETIME ," +
				LogsTable.TYPE + " INTEGER DEFAULT 0 )" ;
		db.execSQL(LOGS_TABLE);

	}

}
