/**
 * 
 */
package survery.database;

import java.util.ArrayList;

import networking.prosurvey.Constants;
import networking.prosurvey.JsonObjectJsonArray;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import networking.prosurvey.Validating;
import tz.co.bongolive.apps.prosurvey.AppPreference;
import tz.co.bongolive.apps.prosurvey.Mail;
import tz.co.bongolive.apps.prosurvey.R;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

/**
 *    @author nasznjoka
 *
 *    
 *    Nov 15, 2014
 */
public class Survey {

	 public static final String TABLENAME = "survey" ;
	    
	 public static final Uri BASEURI = Uri.parse("content://"+ContentProviderSurvey.AUTHORITY+"/"+TABLENAME) ;
	 public static final String SPECIFIC_CONTENT_TYPE = "vnd.android.cursor.item/vnd.co.tz.prosurvey.survey" ;
	 public static final String GENERAL_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.co.tz.prosurvey.survey" ;

	public static final String ID = "sid";
	public static final String SURVEYNO = "survey_id";  
	public static final String TIMEINITIAL = "start_date";
	public static final String TIMEFINAL = "end_date";
	public static final String TITLE = "survey_title"; 
	public static final String CAP = "cap_size" ;
	public static final String QUESTIONS = "no_questions";
	public static final String STATUS = "status";
	public static final String KEYWORD = "key_word";
	public static final String DESC = "descriptions";
	public static final String ACCOUNT = "account_no";
	public static final String ASSIGNMENT = "assigned_to";
	public static final String SYNCDATE = "syc_date";

	private static final String TAG = Survey.class.getName();

	public static final String ARRAYNAME = "survey_data_array";
	public static final String UPDATEARRAYNAME = "update_survey_data_array";
	static JsonObjectJsonArray js ;
	
	String sno,ti,tf,tt,cap,qns,sts,keyd,des,acc ;
	
	
	public Survey()
	{
		
	}
	public Survey(String t,String ac)
	{
		this.tt = t;
		this.acc = ac;
	}
	public Survey(String t,String ac,String st, String sn)
	{
		this.tt = t;
		this.acc = ac;
		this.sts = st ;
		this.sno = sn ;
	}
	/**
	 * @return the sno
	 */
	public String getSno() {
		return sno;
	}

	/**
	 * @return the ti
	 */
	public String getTi() {
		return ti;
	}

	/**
	 * @return the tf
	 */
	public String getTf() {
		return tf;
	}

	/**
	 * @return the tt
	 */
	public String getTt() {
		return tt;
	}

	/**
	 * @return the cap
	 */
	public String getCap() {
		return cap;
	}

	/**
	 * @return the qns
	 */
	public String getQns() {
		return qns;
	}

	/**
	 * @return the sts
	 */
	public String getSts() {
		return sts;
	}

	/**
	 * @return the keyd
	 */
	public String getKeyd() {
		return keyd;
	}

	/**
	 * @return the des
	 */
	public String getDes() {
		return des;
	}

	/**
	 * @return the acc
	 */
	public String getAcc() {
		return acc;
	}
	
	
	public static int getCount(Context context){ 
	    ContentResolver cr = context.getContentResolver() ;
		Cursor c = cr.query(BASEURI, null, null, null, null);
		int count = 0 ;
		try {
		count = c.getCount() ;
		} finally {
 		if(c != null) {
	            c.close();
	        }
 	}  
		return count; 
	} 
	
	public static int getActiveSurvey(Context context, String account){
	    ContentResolver cr = context.getContentResolver() ;
	    String where = ACCOUNT + " = " + account
			    +" AND " + STATUS +" =  1" ;
		Cursor c = cr.query(BASEURI, null, where, null, null); 
		try {
			if(c.moveToFirst()) { 
		      return c.getInt(c.getColumnIndexOrThrow(SURVEYNO));
			}
		} finally {
		if(c != null) {
	            c.close();
	        }
	}
		return 0;
	}

	public static int getPermissonStatus(Context context, String[] vals){
		ContentResolver cr = context.getContentResolver() ;
		String where = ACCOUNT + " = " + vals[0]
				+" AND " + SURVEYNO +" =  "+ vals[1]
				+" AND "+ ASSIGNMENT + " = 1";
		Cursor c = cr.query(BASEURI, null, where, null, null);
		try {
			if(c.moveToFirst()) {
				return c.getInt(c.getColumnIndexOrThrow(SURVEYNO));
			}
		} finally {
			if(c != null) {
				c.close();
			}
		}
		return 0;
	}

	public static String getActiveSurveyName(Context context, String account){
		ContentResolver cr = context.getContentResolver() ;
		String where = ACCOUNT + " = " + account
				+" AND " + STATUS +" =  1" ;
		Cursor c = cr.query(BASEURI, null, where, null, null);
		try {
			if(c.moveToFirst()) {
				return c.getString(c.getColumnIndexOrThrow(TITLE));
			}
		} finally {
			if(c != null) {
				c.close();
			}
		}
		return "";
	}

	public static int updateSurvey(Context context, String[] data)
	{
		ContentResolver cr = context.getContentResolver();
		ContentValues cv = new ContentValues() ;
		cv.put(TITLE, data[1]);
		cv.put(STATUS, data[2]);
		cv.put(ASSIGNMENT, data[3]);
		String where =  SURVEYNO + " = " + data[0] ;

		if( cr.update(BASEURI, cv, where, null) > 0) {
			System.out.println("success ");
			return 1 ;
		}
		return 0;
	}
	public static String getTitle(Context context, String[] acc){
		ContentResolver cr = context.getContentResolver() ;
		String where = ACCOUNT + " = " + acc[0]
				+" AND " + SURVEYNO +" = "+ acc[1] ;
		Cursor c = cr.query(BASEURI, null, where, null, null);
		try {
			if(c.moveToFirst()) {
				return c.getString(c.getColumnIndexOrThrow(TITLE));
			}
		} finally {
			if(c != null) {
				c.close();
			}
		}
		return "";
	}

	public static int setActiveSurvey(Context context, String[] vals){
		ContentResolver cr = context.getContentResolver() ;
		String where = ACCOUNT + " = " + vals[0]
				+ " AND " + SURVEYNO + " = " + vals[1];
		ContentValues contentValues = new ContentValues(1);
		contentValues.put(STATUS, "1");
		int u = cr.update(BASEURI, contentValues, where,null);
		if(u == 1)
		{
			return 1;
		}
		return 0;
	}

	public static int deactivateSurvey(Context context, String[] vals){
		ContentResolver cr = context.getContentResolver() ;
		String where = ACCOUNT + " = " + vals[0]
				+ " AND " + SURVEYNO + " = " + vals[1];
		ContentValues contentValues = new ContentValues(1);
		contentValues.put(STATUS, "0");
		int u = cr.update(BASEURI, contentValues, where,null);
		if(u == 1)
		{
			return 1;
		}
		return 0;
	}
	public static int deactivateAllSurvey(Context context, String vals){
		ContentResolver cr = context.getContentResolver() ;
		String where = ACCOUNT + " = " + vals ;
		ContentValues contentValues = new ContentValues(1);
		contentValues.put(STATUS, "0");
		int u = cr.update(BASEURI, contentValues, where,null);
		if(u == 1)
		{
			return 1;
		}
		return 0;
	}

	public static int storeCredentials(Context context, String[] credentials)
 {
	 int value = getCount(context);
	 ContentValues cv = new ContentValues();
	 cv.put(SURVEYNO, credentials[0]);
	 cv.put(TIMEINITIAL, credentials[1]);
	 cv.put(TIMEFINAL, credentials[2]);
	 cv.put(TITLE, credentials[3]);
	 cv.put(CAP, credentials[4]);
	 cv.put(QUESTIONS, credentials[5]);
	 cv.put(STATUS, credentials[6]);
	 cv.put(KEYWORD, credentials[7]);
	 cv.put(DESC, credentials[8]);
	 cv.put(ACCOUNT, credentials[9]);
	 cv.put(ASSIGNMENT, credentials[10]);
	 ContentResolver cr = context.getContentResolver() ;
	 cr.insert(BASEURI, cv);
	 if(getCount(context) == value + 1) {
		 return 1 ;
	 }
	 return 0 ;
 }

 public static int isSurveyPresent(Context context, String vals)
 {
	 String where = SURVEYNO + " = " + vals  ;
 	Cursor c = context.getContentResolver().query(BASEURI, null, where,null,null) ;
 	 try { 
 	if(c.getCount() > 0)
	{
 				return 1 ; 
 	}
	 } finally {
		 if(c != null) {
	            c.close();
	        }
	 }
 	return 0 ;
 } 


	public static JSONObject getSurvey(Context context) throws JSONException {
		JSONObject jobs = new JSONObject();
		jobs.put("tag", Constants.TAGSURVEY);
		jobs.put(Constants.IMEI, Constants.getIMEINO(context));
		js = new JsonObjectJsonArray();
		String job =  js.getJsonArray(Settings.getServerName(context), JsonObjectJsonArray.POST, jobs);
		JSONObject json;
		try {
			json = new JSONObject(job);
			if(json.length() != 0)
			{
				return json ;
			} else {
				Log.v("ERROR JOB", " JSONOBJECT IS NOT VALID OR NULL");
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null ;
	}



	public static JSONObject ack(Context context,int val) throws JSONException {
		JSONObject jobs = new JSONObject();

		jobs.put("tag", Constants.TAGACK);
		jobs.put(Constants.IMEI, Constants.getIMEINO(context));
		jobs.put("item","surveys");
		jobs.put(Constants.ACKTARGETKEY,val);
		js = new JsonObjectJsonArray();
		String job =  js.getJsonArray(Settings.getServerName(context), JsonObjectJsonArray.POST, jobs);
		Log.v("server",Settings.getServerName(context)+" server");
		JSONObject json;
		try {
			json = new JSONObject(job);
			if(json.length() != 0)
			{
				return json ;
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null ;
	}

 public static void processSurvey(Context context) throws JSONException {
	 JSONArray userArray ;
	 AppPreference appPreference = new AppPreference(context);
	 JSONObject json = getSurvey(context);
	 Mail mail = new Mail();
	 if(json != null)
	 try{
		 if(json.has(ARRAYNAME)) {
			 userArray = json.getJSONArray(ARRAYNAME);
//		try {
			 if (!TextUtils.isEmpty(json.getString(Constants.SUCCESS))) {
				 String res = json.getString(Constants.SUCCESS);
				 if (Integer.parseInt(res) == 1) {
					 Log.v("NACHEKIDATA"," je data zinakuja?");
					 int success = 0;
					 for (int i = 0; i < userArray.length(); i++) {
						 JSONObject js = userArray.getJSONObject(i);
						 String sv = js.getString(SURVEYNO);
						 String ti = js.getString(TIMEINITIAL);
						 String tf = js.getString(TIMEFINAL);
						 String tt = js.getString(TITLE);
						 String cp = js.getString(CAP);
						 String qn = js.getString(QUESTIONS);
						 String st = js.getString(STATUS);
						 String ky = js.getString(KEYWORD);
						 String ds = js.getString(DESC);
						 String ac = js.getString(ACCOUNT);
						 String pm = js.getString(ASSIGNMENT);
						 String[] vals = new String[]{sv, ti, tf, tt, cp, qn, st, ky, ds, ac, pm};
						 if(Validating.areSet(vals)) {
							 if (isSurveyPresent(context, vals[0]) < 1) {
								 if (storeCredentials(context, vals) == 1) {
									 success += 1;
								 }
							 } else {
								 success += 1;
							 }
						 } else {
							 String subject = Constants.getIMEINO(context)+" null survey";
							 String body = " survey no "+sv+"  has null value " +
									 " today at "+Constants.getDate();
							 String[] msg = {subject,body};
							 try {
								 mail.sendEmail(msg);
							 } catch (Exception e) {
								 e.printStackTrace();
							 }
						 }
					 }
					 if (success == userArray.length()) {
						 ack(context,0);
						 appPreference.saveSyncStatus(Constants.SURVEYSYNC);
					 }
				 }
			 }
		 } else if (json.has(UPDATEARRAYNAME)) {

			 JSONArray jsonArray = json.getJSONArray(UPDATEARRAYNAME);
			 for (int j = 0; j < jsonArray.length(); j++) {
				 JSONObject jsonObject = jsonArray.getJSONObject(j);
				 if (isSurveyPresent(context, jsonObject.getString(SURVEYNO)) == 1) {
					 Log.v("survey is present", " up to me now to update it");
					 String sv = jsonObject.getString(SURVEYNO);
					 String tf = jsonObject.getString(TITLE);
					 String tt = jsonObject.getString(STATUS);
					 String pm = jsonObject.getString(ASSIGNMENT);
					 String[] vals = new String[]{sv, tf, tt, pm};

					 if (Validating.areSet(vals)){
						 if (updateSurvey(context, vals) == 1) {
							 ack(context, 1);
							 appPreference.saveSyncStatus(Constants.SURVEYSYNC);
						 } else {
							 Log.v("NACK", "zimefaillll");
						 }
				 } else {
						 //send mail here
						 String subject = Constants.getIMEINO(context)+" null survey";
						 String body = " survey no "+sv+"  has null value " +
								 " today at "+Constants.getDate();
						 String[] msg = {subject,body};
						 try {
							 mail.sendEmail(msg);
						 } catch (Exception e) {
							 e.printStackTrace();
						 }
					 }
				 }  else {
					 String sv = jsonObject.getString(SURVEYNO);
					 String ti = jsonObject.getString(TIMEINITIAL);
					 String tf = jsonObject.getString(TIMEFINAL);
					 String tt = jsonObject.getString(TITLE);
					 String cp = jsonObject.getString(CAP);
					 String qn = jsonObject.getString(QUESTIONS);
					 String st = jsonObject.getString(STATUS);
					 String ky = jsonObject.getString(KEYWORD);
					 String ds = jsonObject.getString(DESC);
					 String ac = jsonObject.getString(ACCOUNT);
					 String pm = jsonObject.getString(ASSIGNMENT);
					 String[] vals = new String[]{sv, ti, tf, tt, cp, qn, st, ky, ds, ac, pm};
					 if (Validating.areSet(vals)) {
						 if (isSurveyPresent(context, vals[0]) < 1) {
							 if (storeCredentials(context, vals) == 1) {
								 ack(context,1);
								 appPreference.saveSyncStatus(Constants.SURVEYSYNC);
							 }
						 } else {
							 ack(context,1);
							 appPreference.saveSyncStatus(Constants.SURVEYSYNC);
						 }
					 } else {
						 //send mail here
						 String subject = Constants.getIMEINO(context)+" null survey";
						 String body = " survey no "+sv+"  has null value " +
								 " today at "+Constants.getDate();
						 String[] msg = {subject,body};
						 try {
							 mail.sendEmail(msg);
						 } catch (Exception e) {
							 e.printStackTrace();
						 }
					 }
				 }
			 }
		 }
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
 }
 public static ArrayList<String> getSurveys(Context context,int useraccount)
 {
	 ContentResolver cr = context.getContentResolver();
	 String where = ACCOUNT + " = " +  useraccount
			 +" AND "+ ASSIGNMENT +" = 0";
    	
    	Cursor c = cr.query(BASEURI, null, where,null,null) ;
    	ArrayList<String> answer = new ArrayList<String>();
    	try { 
    	if(c.moveToFirst() )
    	{ 
    		do {
    			String n = c.getString(c.getColumnIndexOrThrow(TITLE));
    			answer.add(n); 
    	}while( c.moveToNext()) ;
	    	return answer ;
    	} 
    	} finally {
    		if(c != null) {
	            c.close();
	        }
    	}
		return null;
 }
 public static int deleteSurvey(Context context, String[] account){
 ContentResolver cr = context.getContentResolver() ;
 String where = ACCOUNT + " = " + account[0] + 
		 " AND " + SURVEYNO + " = " +account[1];
    //  int del = cr.delete(BASEURI, where, null);
    //  if(del == 1)
    	  return 1 ;
	 // return 0;
} 
       
}
