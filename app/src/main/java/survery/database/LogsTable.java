package survery.database;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import networking.prosurvey.Constants;
import networking.prosurvey.JsonObjectJsonArray;
import networking.prosurvey.Validating;
import tz.co.bongolive.apps.prosurvey.AppPreference;
import tz.co.bongolive.apps.prosurvey.Mail;

public class LogsTable {
	public static final String TABLENAME = "logs" ;
	public static final String ID = "id" ;
	public static final String ISSUE = "issue" ;
	public static final String SOURCE = "source" ;
	public static final String WHEN = "time" ;
	public static final String TYPE = "error_type" ;
	public static final Uri BASEURI = Uri.parse("content://" + ContentProviderSurvey.AUTHORITY + "/" + TABLENAME) ;
	public static final String GENERAL_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.com.prosurvey" +
			".logs" ;
	public static final String SPECIFIC_CONTENT_TYPE = "vnd.android.cursor.item/vnd.com.prosurvey.logs" ;
    public static final String ARRAYNAME = "logs_data_array";
	public static final String UPDATEARRAYNAME = "logs_data_array";
	private static final String TAG = "LOGSDATE";
    private static  JSONArray settingsarray;
	public static String time,cause,info;

	public static String getTime() {
		return time;
	}

	public static String getCause() {
		return cause;
	}

	public static String getInfo() {
		return info;
	}

	public LogsTable(String t,String c,String i){
		time = t;
		cause = c;
		info = i;
	}

	public static int getCount(Context context){
			ContentResolver cr = context.getContentResolver() ;
		    ContentValues cv = new ContentValues();
		    cv.put(SOURCE,"soruce");
		    cv.put(TYPE,"1");
		    cv.put(ISSUE,"App crushed");
		    cv.put(WHEN,Constants.getDate());
		    cr.insert(BASEURI,cv);
			int count = 0 ;
		Cursor c = cr.query(BASEURI, null, null, null, null);
			try {
				count = c.getCount() ;
			} finally {
				if(c != null) {
					c.close();
				}
			}
			return count;
	}
	
	public static int insert(Context context,String[] settings)
	{
		int settingsCount = getCount(context) ;

		ContentResolver cr = context.getContentResolver() ;
		ContentValues cv = new ContentValues() ;
		cv.put(ISSUE, settings[0]) ;
		cv.put(SOURCE, settings[1]);
		cv.put(WHEN, Constants.getDate());
		cr.insert(BASEURI, cv);
		if(getCount(context) == settingsCount +1)
		{
			return 1 ;
		} else {
			return 0 ;
		}
	}

	public static void processLogs(Context context) throws JSONException{
		AppPreference appPreference = new AppPreference(context);

	}

	public static String getLogName(Context context){
		ContentResolver cr = context.getContentResolver() ;
		Cursor c = cr.query(BASEURI, null, null, null, null);
		try {
			if(c.moveToFirst()) {
				return c.getString(c.getColumnIndexOrThrow(SOURCE));
			}
		} finally {
			if(c != null) {
				c.close();
			}
		}
		return Constants.SERVER ;
	}

}
