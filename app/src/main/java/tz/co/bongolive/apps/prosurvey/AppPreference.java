package tz.co.bongolive.apps.prosurvey;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import java.util.ArrayList;

import networking.prosurvey.Constants;
import survery.database.User;

/**
 * Created by nasznjoka on 1/2/2015.
 */
public class AppPreference {
	private SharedPreferences _sharedPrefs,_settingsPrefs;
	private SharedPreferences.Editor _prefsEditor;

	public AppPreference(Context context) {
		this._sharedPrefs = context.getSharedPreferences(Constants.USERDATA, Activity.MODE_MULTI_PROCESS);
		PreferenceManager.setDefaultValues(context, R.xml.settings, false);
		PreferenceManager.setDefaultValues(context, R.xml.admin, false);
		this._settingsPrefs = PreferenceManager.getDefaultSharedPreferences(context);
		this._prefsEditor = _sharedPrefs.edit();
	}

	public int getUserRole() {
		return _sharedPrefs.getInt(Constants.USERROLE, 0);
	}
	public int getAdminStatus(Context context) {
		return User.getAdminUser(context, getUserId());
	}
	public int getUserId() {
		return _sharedPrefs.getInt(Constants.USERID, 0);
	}
    public boolean getDbChoice() {
		return _settingsPrefs.getBoolean(Constants.PREF_IMPORT_KEY, false);
	}

    public String getImei() {
		return _settingsPrefs.getString(Constants.PREF_IMEI_KEY, "");
	}
    public String getServer() {
		return _settingsPrefs.getString(Constants.PREF_URL, "");
	}

	public String getDbnm()
	{
		return this._sharedPrefs.getString(Constants.KEY_DB, "");
	}

	public String getDbpath()
	{
		return this._sharedPrefs.getString(Constants.KEY_DBPATH, "");
	}


	public int getSyS()
	{
		String sv =	_sharedPrefs.getString(Constants.SURVEYSYNC,"");
		if(sv.equals("1")){
			return 1 ;
		}
		return 0;
	}
	public int getSyQ()
	{
		String sv =	_sharedPrefs.getString(Constants.QUESTIONSYNC,"");
		if(sv.equals("1"))
			return 2 ;
		return 0;
	}
	public int getSyO()
	{
		String sv =	_sharedPrefs.getString(Constants.RESPONSESYNC,"");
		if(sv.equals("1"))
			return 3 ;

		return 0;
	}
	public int getSyU()
	{
		String sv =	_sharedPrefs.getString(Constants.USERSSYNC,"");
		if(sv.equals("1")){
			return 4 ;
		}
		return 0;
	}
	public int getSySt()
	{
		String sv =	_sharedPrefs.getString(Constants.SETTINGSSYNC,"");
		if(sv.equals("1")){
			return 5 ;
		}
		return 0;
	}


	public void saveUserRole(int role) {
		_prefsEditor.putInt(Constants.USERROLE, role);
		_prefsEditor.commit();
	}

    public void reset_imei(){
        _settingsPrefs.edit().putString(Constants.PREF_IMEI_KEY,"").commit();
        _settingsPrefs.edit().remove(Constants.PREF_IMEI_KEY).commit();
    }
    public void reset_import(){
        _settingsPrefs.edit().putBoolean(Constants.PREF_IMPORT_KEY, false).commit();
    }

	public void makeAdminNormal(Context context, int value) {
		User.inAppChangeAdminMode(context, getUserId(), value);
	}

	public void saveUserId(int id) {
		_prefsEditor.putInt(Constants.USERID, id);
		_prefsEditor.commit();
	}

	public void reset_dbname()
	{
		this._sharedPrefs.edit().remove(Constants.KEY_DB).commit();
		Log.v("PATHRESETTING", "PATH is reset " + getDbnm());
	}

	public void reset_path()
	{
		this._sharedPrefs.edit().remove(Constants.KEY_DBPATH).commit();
		Log.v("PATHRESETTING", "PATH is reset " + getDbpath());
	}

	public void saveDbPath(String paramString)
	{
		this._prefsEditor.putString(Constants.KEY_DBPATH, paramString);
		this._prefsEditor.commit();
	}

	public void saveDbnm(String paramString)
	{
		this._prefsEditor.putString(Constants.KEY_DB, paramString);
		this._prefsEditor.commit();
	}

	public void saveUrl(String paramString)
	{
		this._settingsPrefs.edit().putString(Constants.PREF_URL, paramString).apply();
	}

	public void saveSyncStatus(String data){
		switch (data){
			case Constants.SURVEYSYNC:
				_prefsEditor.putString(Constants.SURVEYSYNC,Constants.SURVEYSYNCVALUE);
				break;
			case Constants.QUESTIONSYNC:
				_prefsEditor.putString(Constants.QUESTIONSYNC,Constants.QUESTIONSYNCVALUE);
				break;
			case Constants.RESPONSESYNC:
				_prefsEditor.putString(Constants.RESPONSESYNC,Constants.RESPONSESYNCVALUE);
				break;
			case Constants.USERSSYNC:
				_prefsEditor.putString(Constants.USERSSYNC,Constants.USERSSYNCVALUE);
				break;
			case Constants.SETTINGSSYNC:
				_prefsEditor.putString(Constants.SETTINGSSYNC,Constants.SETTINGSSYNCVALUE);
				break;
		}
		_prefsEditor.commit();
	}

	public void clearSync(){
		_prefsEditor.remove(Constants.QUESTIONSYNC);
		_prefsEditor.remove(Constants.RESPONSESYNC);
		_prefsEditor.remove(Constants.SETTINGSSYNC);
		_prefsEditor.remove(Constants.SURVEYSYNC);
		_prefsEditor.remove(Constants.USERSSYNC);
		_prefsEditor.apply();
	}


	public String getDefaultLanguage()
	{
		return _settingsPrefs.getString(Constants.PREF_LANG_KEY, "");
	}
	public String getDefaultSync()
	{
		return _settingsPrefs.getString(Constants.PREF_SYNC_KEY, "");
	}
	public void clearRole() {
		_prefsEditor.clear();
		_prefsEditor.commit();
	}
}
