package tz.co.bongolive.apps.prosurvey;

/**
 * Created by nasznjoka on 2/12/2015.
 */


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import networking.prosurvey.Constants;
import networking.prosurvey.Validating;
import survery.database.Participant;
import survery.database.Questions;
import survery.database.Response;
import survery.database.Responsechoice;
import survery.database.Survey;
import survery.database.User;

/**
 *    @author nasznjoka
 *
 *
 *    Dec 2, 2014
 */
@SuppressLint("DefaultLocale") public class OpenEnded {


	public static void open(final Context context, final String[] info) {
		//info[0] rank, nextqn, prevqn,participant[3],
		final Dialog opend = new Dialog(context, R.style.CustomDialog);
		opend.setCancelable(false);

		final int rank = Integer.parseInt(info[0]);
		final int next = Integer.parseInt(info[1]);
		final int prev = Integer.parseInt(info[2]);
		final int participant = Integer.parseInt(info[3]);
		final String flag = info[4];
		final int survey = Integer.parseInt(info[5]);
		final String interviewerno = info[6];
		LayoutInflater infl = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = infl.inflate(R.layout.questions, null);
		AppPreference appPreference = new AppPreference(context.getApplicationContext());
		final String language = appPreference.getDefaultLanguage();
		final int questionid = Questions.getQuestionId(context, new int[]{rank, survey}, language);
		String questionText = Questions.getQuestionTextLanguage(context, new int[]{questionid,
				survey});
		final ArrayList<String> listqn = Responsechoice.getQuestionAnswer(context, questionid);
		final ArrayList<String> requiredlist = new ArrayList<>();

//		final int listsize = list.size();
		final int listsizeqn = listqn.size();
		final boolean fired = false;

		opend.setContentView(view);

		final LinearLayout layout = (LinearLayout) opend.findViewById(R.id.qnlayout);


		final ArrayList<String> enteredvalus = new ArrayList<String>(listsizeqn);
		final ArrayList<String> listchoice = Responsechoice.getQuestionAnswerId(context,
				questionid, survey);

		final int width = LayoutParams.MATCH_PARENT;
		int height = LayoutParams.MATCH_PARENT;
		final int viewheight = LayoutParams.WRAP_CONTENT;
		final int[] checkupdate = {0};

		String account = String.valueOf(User.getUserAccount(context,
				Integer.parseInt(flag)));
		String titlee = Survey.getTitle(context, new String[]{account, String.valueOf(survey)});
		TextView txttitle = (TextView) opend.findViewById(R.id.txttitle);
		txttitle.setText(titlee.toUpperCase());

		TextView title = (TextView) opend.findViewById(R.id.txtqno);
		title.setText(context.getString(R.string.strquestionnotitle)+ " "+ rank) ;

		TextView dc = (TextView) opend.findViewById(R.id.txtqn);
		dc.setText(questionText);

		final EditText[] et = new EditText[listsizeqn];
		final TextView[] txtv = new TextView[listsizeqn];
		for (int i = 0; i < listsizeqn; i++) {
			txtv[i] = new TextView(context);
			txtv[i].setText(listqn.get(i));
			et[i] = new EditText(context);
			et[i].setId(i);
			et[i].clearFocus();
			GeneralViews.setDataType(context, listchoice.get(i), et[i]);
			layout.addView(txtv[i], width, viewheight);
			layout.addView(et[i], width, viewheight);
			final int k = i;

					int datatype = Responsechoice.getDataType(context, listchoice.get(i));
						if(datatype == 3 || datatype == 4){

							et[i].setFocusable(true);
							et[i].setFocusableInTouchMode(false);
							et[i].setOnClickListener(new OnClickListener() {
								@Override
								public void onClick(View v) {

									et[k].setFocusable(true);
						et[k].setFocusableInTouchMode(true);
					}
				});
				et[i].setOnFocusChangeListener(new View.OnFocusChangeListener() {
					@Override
					public void onFocusChange(View v, boolean hasFocus) {
						if (hasFocus) {
							GeneralViews.setDateTimeField(context, et[k]);
						}
					}
				});
			}

		}


		final Button bedit = (Button) opend.findViewById(R.id.btnedit);
		final Button bst = (Button) opend.findViewById(R.id.goforward);

		bedit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				for (int j = 0; j < listsizeqn; j++) {
					et[j].setEnabled(true);
				}
				bedit.setText(context.getString(R.string.strupdate));
				checkupdate[0] = 1;
				int check = 0;
				int checkrequired = 0;
				int checkminmax = 0;
				for (int i = 0; i < listsizeqn; i++) {

					String chked = et[i].getText().toString();
					if (!chked.isEmpty()) {
						int datatype = Responsechoice.getDataType(context, listchoice.get(i));
						String minvalue = Responsechoice.getMin(context, listchoice.get(i));
						String maxvalue = Responsechoice.getMax(context, listchoice.get(i));
						switch (datatype) {
							case 1:
								int ansint = Integer.parseInt(chked);
								int min = Integer.parseInt(minvalue);
								int max = Integer.parseInt(maxvalue);
								if (!(ansint >= min)) {
									Toast.makeText(context, context.getString(R.string.strmin)
													+ " " + min,
											Toast.LENGTH_LONG).show();
									et[i].setText("");
									chked = "";
									checkminmax += 1;
								}
								if (max != 0) {
									if (!(ansint <= max)) {
										Toast.makeText(context, context.getString(R.string.strmax)
														+ " " + max,
												Toast.LENGTH_LONG).show();
										et[i].setText("");
										chked = "";
										checkminmax += 1;
									}
								}
								break;
							case 2:

								break;
							case 3:
								SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
								try {
									Date entered = df.parse(chked);
									Date mindate = df.parse(minvalue);
									Date maxdate = df.parse(maxvalue);
									Date common = df.parse("0000-00-00");
									Log.v("dates", " MIN DATE " + mindate + " MAX DATE " +
											maxdate + " entered value is " + entered);
									if (entered.compareTo(mindate) < 0) {
										Toast.makeText(context, context.getString(R.string.strmin)
														+ " " + minvalue,
												Toast.LENGTH_LONG).show();
										et[i].setText("");
										chked = "";
										et[i].clearFocus();
										checkminmax += 1;
									}
									if (entered.compareTo(common) != 0) {
										if (entered.compareTo(maxdate) > 0) {
											Toast.makeText(context, context.getString(R.string.strmax)
															+ " " + maxvalue,
													Toast.LENGTH_LONG).show();
											et[i].setText("");
											chked = "";
											checkminmax += 1;
										}
									}
								} catch (ParseException e) {
									e.printStackTrace();
								}
								break;
							case 4:
								break;
						}
					}
					if (!chked.isEmpty()) {
						chked = chked.concat(Constants.SEPARATOR);
						enteredvalus.add(chked);
					}
					String req = listchoice.get(i);
					int reqq = Responsechoice.isQuestionRequired(context, req);
					if (reqq == 1) {
						if (chked.isEmpty()) {
							Toast.makeText(context, context.getString(R.string.strreqres) + " " +
											listqn.get(i),
									Toast.LENGTH_LONG).show();
							checkrequired += 1;
						}
					}

				}
				String answer = "";

				for (int i = 0; i < enteredvalus.size(); i++) {
					answer += enteredvalus.get(i);
				}

//				}

				if (!answer.isEmpty() && checkrequired == 0 && checkminmax == 0/* && enteredvalus
				.size
				() ==
				listsizeqn*/) {
					final String qn = String.valueOf(rank);
					String prv = String.valueOf(prev);
					String part = String.valueOf(participant);

					final String[] j = {part, qn, prv, String.valueOf(survey), "comments", answer, "0"};
					if (Validating.areSet(j)) {
						int storesponse = Response.updateResponse(context, j);
						if (storesponse != 0) {
							final int nextqn = Responsechoice.getNextQn(context, questionid, j[5]);
							System.out.print("\n\n THIS IS SUPPOSED TO BE THE NEXT QUESTION  (" + nextqn + ")");

							if (nextqn != 0) {
								Log.w("tayana", " the next qn flag is not 0 but its " + nextqn + "\n");
								int quid = Questions.getQuestionId(context, new int[]{nextqn, survey},
										language);//rank,survey
								String nextqntxt = Questions.getQuestionTextLanguage(context,
										new int[]{quid, survey});
								if (Validating.areSet(nextqntxt)) {
									String[] info = {j[1], String.valueOf(nextqn), qn, j[0], flag, String.valueOf(survey), interviewerno};

									//info[0] rank, nextqn, prevqn,survey,participant,
									GeneralViews.getNextQuestion(context, info);
									info = null;
									listqn.clear();
									opend.dismiss();
								}
								else {
									String _surv = String.valueOf(survey);
									String _part = String.valueOf(participant);
									int end = Participant.insetEndtime(context, new String[]{_surv, interviewerno, _part});
									if (end == 1) {
										Toast.makeText(context, context.getString(R.string.strsurveycomplete), Toast.LENGTH_LONG).show();
										if (opend.isShowing()) {
											opend.dismiss();
										}
									}
								}
							}
							else {
								Log.w("tayana", " the next qn flag is 0 where we follow a norma flow \n");
								int rnk = rank + 1;
								int quid = Questions.getQuestionId(context, new int[]{rnk, survey},
										language);//rank,survey
								String nextqntxt = "";
								if (quid > 0) {
									nextqntxt = Questions.getQuestionTextLanguage(context,
											new int[]{quid, survey});
								}
								if (Validating.areSet(nextqntxt)) {
									String nextq = String.valueOf(rnk);
									Log.w("tayana", " increment rank by 1 to be" + rnk + "\n");
									String[] info = {j[1], nextq, qn, j[0], flag, String.valueOf(survey), interviewerno};
									//info[0] rank, nextqn, prevqn,survey,participant,
									GeneralViews.getNextQuestion(context, info);
									info = null;
									listqn.clear();
									opend.dismiss();
								}
								else {
									String _surv = String.valueOf(survey);
									String _part = String.valueOf(participant);
									int end = Participant.insetEndtime(context, new String[]{_surv, interviewerno, _part});
									if (end == 1) {
										Toast.makeText(context, context.getString(R.string.strsurveycomplete), Toast.LENGTH_LONG).show();
										if (opend.isShowing()) {
											opend.dismiss();
										}
									}
								}
							}
						}
					}
				}
				else {
					enteredvalus.clear();
					answer = "";
					checkrequired = 0;
					checkminmax = 0;
					Toast.makeText(context, context.getString(R.string.strfillanswer), Toast.LENGTH_LONG).show();
				}

			}
		});

		bst.setVisibility(View.VISIBLE);
		int answered = Response.isResponseStored(context, new String[]{info[3], info[0], info[5]});
		TextView txtinfo = new TextView(context);
		TextView txtanswer = new TextView(context);
		txtinfo.setText(context.getString(R.string.stransweredalready));
		if (answered == 1) {
			bedit.setEnabled(true);
			layout.addView(txtinfo, width, viewheight);
			for (int i = 0; i < listsizeqn; i++) {
				et[i].setEnabled(false);
			}
			String[] ans = Response.getAnswer(context, new String[]{info[3], info[0], info[5]});
			String an = ans[0];
			an = "<b>" + an + "</b>";
			an = an.toUpperCase();

			txtanswer.setTextColor(Color.BLUE);
			if (an.contains(Constants.SEPARATOR)) {
				an = an.replace(Constants.SEPARATOR, ",");
				txtanswer.setText("\n " + context.getString(R.string.stransweris) + " " + Html.fromHtml(an));
			}
			else {
				txtanswer.setText("\n " + context.getString(R.string.stransweris) + " " + Html.fromHtml(an));
			}
			layout.addView(txtanswer, width, viewheight);
			bst.setVisibility(View.GONE);
		}
		else {
			txtanswer.setVisibility(View.GONE);
			bedit.setEnabled(false);
			txtinfo.setVisibility(View.GONE);
		}


		bst.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				int check = 0;
				int checkrequired = 0 ;
				int checkminmax = 0;
				for (int i = 0; i < listsizeqn; i++) {

					String chked = et[i].getText().toString();
					if (!chked.isEmpty()) {
						int datatype = Responsechoice.getDataType(context, listchoice.get(i));
						String minvalue = Responsechoice.getMin(context, listchoice.get(i));
						String maxvalue = Responsechoice.getMax(context, listchoice.get(i));
						switch (datatype) {
							case 1:
								int ansint = Integer.parseInt(chked);
								int min = Integer.parseInt(minvalue);
								int max = Integer.parseInt(maxvalue);
								if (!(ansint >= min)) {
									Toast.makeText(context, context.getString(R.string.strmin)
													+ " " + min,
											Toast.LENGTH_LONG).show();
									et[i].setText("");
									chked = "";
									checkminmax += 1;
								}
								if (max != 0) {
									if (!(ansint <= max)) {
										Toast.makeText(context, context.getString(R.string.strmax)
														+ " " + max,
												Toast.LENGTH_LONG).show();
										et[i].setText("");
										chked = "";
										checkminmax += 1;
									}
								}
								break;
							case 2:

								break;
							case 3:
								SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
								try {
									Date entered = df.parse(chked);
									Date mindate = df.parse(minvalue);
									Date maxdate = df.parse(maxvalue);
									Date common = df.parse("0000-00-00");
									Log.v("dates"," MIN DATE "+mindate + " MAX DATE " +
											maxdate + " entered value is "+entered);
									if (entered.compareTo(mindate) < 0) {
										Toast.makeText(context, context.getString(R.string.strmin)
														+ " " + minvalue,
												Toast.LENGTH_LONG).show();
										et[i].setText("");
										chked = "";
										et[i].clearFocus();
										checkminmax += 1;
									}
									if (entered.compareTo(common) != 0) {
										if (entered.compareTo(maxdate) > 0) {
											Toast.makeText(context, context.getString(R.string.strmax)
															+ " " + maxvalue,
													Toast.LENGTH_LONG).show();
											et[i].setText("");
											chked = "";
											checkminmax += 1;
										}
									}
								} catch (ParseException e) {
									e.printStackTrace();
								}
								break;
							case 4:
								break;
						}
					}
					if (!chked.isEmpty()) {
						chked = chked.concat(Constants.SEPARATOR);
						enteredvalus.add(chked);
					}
					String req = listchoice.get(i);
					int reqq = Responsechoice.isQuestionRequired(context, req);
					if(reqq == 1){
						if(chked.isEmpty()) {
							Toast.makeText(context, context.getString(R.string.strreqres) + " "+
											listqn.get(i),
									Toast.LENGTH_LONG).show();
							checkrequired += 1;
						}
					}

				}
				String answer = "";

					for (int i = 0; i < enteredvalus.size(); i++) {
						answer += enteredvalus.get(i);
					}

//				}

				if (!answer.isEmpty() && checkrequired == 0 && checkminmax == 0/* && enteredvalus
				.size
				() ==
				listsizeqn*/) {
					final String qn = String.valueOf(rank);
					String prv = String.valueOf(prev);
					String part = String.valueOf(participant);

					final String[] j = {part, qn, prv, String.valueOf(survey), "comments", answer, "0"};
					if (Validating.areSet(j)) {
						int storesponse = Response.storeResponse(context, j);
						if (storesponse != 0) {
							final int nextqn = Responsechoice.getNextQn(context, questionid,  j[5]);
//							int responseid = Response.getResponseId(context, questionid);

							Log.v("GETNEXT", " QN NO " + rank + " responseid " +
									""+storesponse);
							if (nextqn != 0) {
								Log.w("tayana", " the next qn flag is not 0 it is " + nextqn  );
								int quid = Questions.getQuestionId(context,new int[]{nextqn,survey},
										language);
								String nextqntxt = Questions.getQuestionTextLanguage(context,
										new int[]{quid, survey});
								Log.v("storeresponse","store response is "+ storesponse);
								if (Validating.areSet(nextqntxt)) {
									int jumpflag = Response.updateJumpFlag(context,
											new int[]{storesponse,1,nextqn});
									if(jumpflag == 1) {
										String[] info = {j[1], String.valueOf(nextqn), qn, j[0],
												flag, String.valueOf(survey), interviewerno};
										GeneralViews.getNextQuestion(context, info);
										info = null;
										listqn.clear();

										opend.dismiss();
									}
								}
								else {
									String _surv = String.valueOf(survey);
									String _part = String.valueOf(participant);
									int end = Participant.insetEndtime(context, new String[]{_surv, interviewerno, _part});
									if (end == 1) {
										Toast.makeText(context, context.getString(R.string.strsurveycomplete), Toast.LENGTH_LONG).show();
										if (opend.isShowing()) {
											opend.dismiss();
										}
									}
								}
							}
							else {
								Log.w("tayana", " the next qn flag is 0 where we follow a norma flow " +
										"\n");
								int rnk = rank + 1;
								int quid = Questions.getQuestionId(context,new int[]{rnk,survey},
										language);//rank,survey
								String nextqntxt = "";
								if(quid > 0) {
									nextqntxt = Questions.getQuestionTextLanguage(context,
											new int[]{quid, survey});
								}
								if (Validating.areSet(nextqntxt)) {
									String nextq = String.valueOf(rnk);
									Log.w("tayana", " increment rank by 1 to be" + rnk + "\n");
									String[] info = {j[1], nextq, qn, j[0], flag, String.valueOf(survey), interviewerno};
									//info[0] rank, nextqn, prevqn,survey,participant,
									GeneralViews.getNextQuestion(context, info);
									info = null;
									listqn.clear();
									opend.dismiss();
								}
								else {
									String _surv = String.valueOf(survey);
									String _part = String.valueOf(participant);
									int end = Participant.insetEndtime(context, new String[]{_surv, interviewerno, _part});
									if (end == 1) {
										Toast.makeText(context, context.getString(R.string.strsurveycomplete), Toast.LENGTH_LONG).show();
										if (opend.isShowing()) {
											opend.dismiss();
										}
									}
								}
							}
						}
					}
				} else {
					enteredvalus.clear();
					answer = "";
					checkrequired = 0;
					checkminmax = 0;
				}
			}
		});
		ImageView bspt = (ImageView) opend.findViewById(R.id.goback);
		bspt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				int pre = Response.getPreviousQn(context, info);
				String prevofpre = String.valueOf(pre);
				Log.v("PREVIOUS", "rank qn is " + info[2]);
				Log.v("PREVIOUS", "previous qn is " + prevofpre);
				Log.v("PREVIOUS", "next qn is " + info[2]);
				Log.v("PREVIOUS", "previous qn before " + info[2] + " qn is " + prevofpre);
				String[] data = {info[2], info[2], prevofpre, info[3], info[4], info[5], info[6]};
				GeneralViews.getPrevQuestion(context, data, Integer.parseInt(info[2]));
				opend.dismiss();
			}
		});

		ImageView skip = (ImageView)opend.findViewById(R.id.btnskip);
		final String[] answeravailable = Response.getAnswer(context, new String[]{info[3],info[0],
				info[5]});
		if(answeravailable[0].isEmpty()){
			skip.setVisibility(View.GONE);
		}
		skip.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (checkupdate[0] == 1) {

					AlertDialogManager alertDialogManager = new AlertDialogManager();
					alertDialogManager.showAlertDialog(context, "ERROR",
							context.getString(R.string.strnoskip), false);
				}
				else {
					int rnk;
					int rank = Integer.parseInt(info[0]);
					rnk = rank + 1;
					String pr = info[2];
					String p = info[3];
					String flag = info[4];
					String s = info[5];
					String inter = info[6];
					int quid = Questions.getQuestionId(context, new int[]{rnk, survey},
							language);//rank,survey
					String nextqntxt = "";
					if (quid > 0) {
						nextqntxt = Questions.getQuestionTextLanguage(context,
								new int[]{quid, survey});
					}
					if (Validating.areSet(nextqntxt)) {
						int responseid = Integer.parseInt(answeravailable[1]);
						Log.w("responseid", " response id is "+responseid);
						int newrank = Response.getJumpedRank(context,
								new int[]{questionid,participant,survey});
						if(newrank == 0){
							Log.v("nojump","this question has no jump skip normally");
							String[] info = {String.valueOf(rnk), String.valueOf(rnk), pr, p, flag, s, inter};


							GeneralViews.getNextQuestion(context, info);
							info = null;
							listqn.clear();
							opend.dismiss();
						} else if (newrank > 0){
							Log.v("nojump","this question has jump skip to the jumped rank");
							String[] info = {String.valueOf(newrank), String.valueOf(newrank), pr, p,
									flag, s, inter};


							GeneralViews.getNextQuestion(context, info);
							info = null;
							listqn.clear();
							opend.dismiss();
						}
					}
					else {
						String _surv = String.valueOf(survey);
						String _part = String.valueOf(participant);
						int end = Participant.insetEndtime(context, new String[]{_surv, interviewerno, _part});
						if (end == 1) {
							Toast.makeText(context, context.getString(R.string.strsurveycomplete), Toast.LENGTH_LONG).show();
							if (opend.isShowing()) {
								opend.dismiss();
							}
						}
					}
				}
			}
		});
		Button masterexit = (Button) opend.findViewById(R.id.btnexit);
		masterexit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String _surv = String.valueOf(survey);
				String _part = String.valueOf(participant);
				if (rank > 1) {
					int end = Participant.insetEndtime(context, new String[]{_surv, interviewerno, _part});

					if (end == 1) {
//						if (opend.isShowing()) {

						Toast.makeText(context, context.getString(R.string.strsurveyexited),
								Toast.LENGTH_LONG).show();
							opend.dismiss();
						Intent intent = new Intent(context, Dashboard.class);
						intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
						context.startActivity(intent);

//						}
					}
				}
			}
		});

		opend.show();
	}




}
