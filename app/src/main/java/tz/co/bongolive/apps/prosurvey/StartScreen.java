/**
 * 
 */
package tz.co.bongolive.apps.prosurvey;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.ResponseCache;
import java.net.URL;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import networking.prosurvey.Constants;
import networking.prosurvey.Validating;
import survery.database.ContentProviderSurvey;
import survery.database.LogsTable;
import survery.database.Questions;
import survery.database.Responsechoice;
import survery.database.Survey;
import survery.database.User;
import survery.database.Settings;

/**
 *    @author nasznjoka
 *
 *    
 *    Nov 29, 2014
 */
public  class StartScreen extends ActionBarActivity implements OnClickListener {

	private static final String TAG = StartScreen.class.getName();
	Button login, cancel;
    EditText etuser,etpass,etpass2 ;
    Account mAccount ;
	AppPreference appPreference;
	AlertDialogManager ma;
	private final int REQUEST_CODE_PICK_DIR = 1;
	private final int REQUEST_CODE_PICK_FILE = 2;
	final Activity activityForButton = this;
	private int mInterval = 5000; // 5 seconds by default, can be changed later
	private Handler handler;
	int mProgressStatus = 0;
	Button importbtn,syncbtn;
	boolean sent = false;
	ProgressDialog pDialog;
	ProgressBar progressBar;
	TextView txtsyncsta ;
	private static int svcheck,qncheck,optcheck,uscheck,settcheck;
	Timer myTimer;
		@Override
	    public void onCreate(Bundle savedInstanceState)
	    { 
	        super.onCreate(savedInstanceState);

			appPreference = new AppPreference(getApplicationContext());
		    String languageToLoad  = appPreference.getDefaultLanguage();

		    ma = new AlertDialogManager();
		    Locale locale = new Locale(languageToLoad);
		    Locale.setDefault(locale);
		    Configuration config = new Configuration();
		    config.locale = locale;
		    getBaseContext().getResources().updateConfiguration(config,
				    getBaseContext().getResources().getDisplayMetrics());

		    setContentView(R.layout.login);

			String str4 = Settings.getServerName(this);
			if (str4.isEmpty()){
				appPreference.saveUrl(Constants.SERVER);
			} else
			if(!str4.equals(Constants.SERVER))
			{
				appPreference.saveUrl(str4);
			}

			String str2 = appPreference.getDbnm();
			String str3 = appPreference.getDbpath();

			 importbtn = (Button)findViewById(R.id.btnimport);
			importbtn.setVisibility(View.GONE);
			progressBar = (ProgressBar)findViewById(R.id.progressbar);

			importbtn.setOnClickListener( new View.OnClickListener()
			{
				public void onClick(View paramAnonymousView)
				{
					Intent localIntent = new Intent("SELECT_FILE_ACTION", null, activityForButton, PickFile.class);
					localIntent.putExtra("showCannotRead", false);
					StartScreen.this.startActivityForResult(localIntent, REQUEST_CODE_PICK_FILE);
				}
			});
			txtsyncsta = (TextView)findViewById(R.id.syncstat);


			mAccount = createSyncAccount(this);
		    String time = appPreference.getDefaultSync();

		    if(time.isEmpty())
		    {
			    time = "2130000";
		    }
		    final int t = Integer.parseInt(time);

			svcheck = appPreference.getSyS();
			qncheck = appPreference.getSyQ();
			optcheck  = appPreference.getSyO();
			uscheck = appPreference.getSyU();
			settcheck = appPreference.getSySt();
			Log.v("check", "sys "+svcheck+ " qn "+qncheck+ " opt "+optcheck+" us "+uscheck + " sett "+settcheck);
//			Log.v("logscount ","count is "+ LogsTable.getCount(this));
		    if(svcheck != 1 || qncheck != 2 || optcheck != 3 || uscheck != 4 || settcheck != 5 ) {
				txtsyncsta.setVisibility(View.VISIBLE);
				if(Constants.isNetworkAvailable(this)){
					progressBar.setVisibility(View.VISIBLE);
					txtsyncsta.setText(getString(R.string.strsyncsstartstatus));
					new SurveySync(this,progressBar,txtsyncsta).execute();
				} else {
					txtsyncsta.setText(getString(R.string.strconnstatus));
				}
			} else {
			    periodicadapersync(this, mAccount, t);
				progressBar.setProgress(View.GONE);
		    }


            final TextView txtv = (TextView)findViewById(R.id.versioncode);

            String versioncode = Constants.getVersionNumber(this);
            txtv.setText("Safi " + versioncode);

	        login = (Button)findViewById(R.id.btnsubmit);
			cancel = (Button)findViewById(R.id.btncancel);
			login.setOnClickListener(this);
			cancel.setOnClickListener(this);
			etuser = (EditText)findViewById(R.id.etusername);
			etpass = (EditText)findViewById(R.id.etpass);
			etpass2 = (EditText)findViewById(R.id.etconfirmpass);
			etpass2.setVisibility(View.GONE);
		    ActionBar ab = getSupportActionBar();
		    ab.setDisplayShowTitleEnabled(true);
		    ab.setDisplayShowHomeEnabled(true);
		   /* ab.setTitle(Html.fromHtml("<font color=#ffffff>"+getString(R.string
				    .app_name)+"</font>"));*/


	    }




		
		/* (non-Javadoc)
		 * @see android.view.View.OnClickListener#onClick(android.view.View)
		 */
		@Override
		public void onClick(View view) {
			switch(view.getId())
			{
			case R.id.btnsubmit: 
				String stuser, stpass;
				stuser = etuser.getText().toString();
				stuser = stuser.trim();
				stpass = etpass.getText().toString();
				stpass = stpass.trim(); 
				String p = null ;
				try {
					 p = Constants.makeSHA1Hash(stpass) ;
				} catch (Exception e) { 
					e.printStackTrace();
				}
				String[] vals = {stuser,p} ;
				if(Validating.areSet(vals))
				{

					int count= 0;
//					appPreference.clearRole();
                    int create =  User.login(this, vals);
						if(create >= 0)
						{
							int role = User.getUserlRole(this, create);
							appPreference.saveUserRole(role);
							appPreference.saveUserId(create);
							if(role == 1) {
								Log.v("adminmode","This user is admin set default mode to admin");
								User.setAdminMode(this, create);
							}
							Intent intent = new Intent(this, Dashboard.class);
							intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(intent);
							finish();
							// login success 
						} else { 
							// login failed
							count = count + 1;
//							Toast.makeText(this,  , Toast.LENGTH_LONG).show();
//							etuser.setText("");
							ma.showAlertDialog(this,getString(R.string.strlogin) ,
									getString(R.string.strloginmsg), false);
							etpass.setText("");
							if(count >= 2)
							etpass.setInputType(InputType.TYPE_CLASS_TEXT);
						}
				} else {
//					Toast.makeText(this, getString(R.string.strfillall), Toast.LENGTH_LONG).show();

					ma.showAlertDialog(this,getString(R.string.strlogin) ,
							getString(R.string.strfillall), false);
					etuser.setHint(getString(R.string.strusername));
					etuser.setHintTextColor(Color.RED);
					etpass.setHint(getString(R.string.strpassword)); 
					etpass.setHintTextColor(Color.RED);
				 return ;	
				}
				break;
			case R.id.btncancel:  
	            finish();  
			}
		}  
		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
				getMenuInflater().inflate(R.menu.startscreen, menu);
			return true ;
		}
 		
		@Override
		public boolean onOptionsItemSelected(MenuItem item) { 
			int id = item.getItemId();
			switch(id){ 
			case R.id.action_syncnow: 
				ondemandsync();
				break;
			}
			return super.onOptionsItemSelected(item);
		}
	public void periodicadapersync(Context context,Account mAccount, int time)
	{

		if(time == 0) {
			 time = 10000000;
		} else {
			ContentResolver.addPeriodicSync(
					mAccount,
					ContentProviderSurvey.AUTHORITY,
					Bundle.EMPTY, time);
		}
	}
	public void ondemandsync()
	{
		txtsyncsta.setVisibility(View.VISIBLE);
		if(Constants.isNetworkAvailable(this)){
			progressBar.setVisibility(View.VISIBLE);
			txtsyncsta.setText(getString(R.string.strsyncsstartstatus));
			new SurveySync(this,progressBar,txtsyncsta).execute();
		} else {
			txtsyncsta.setText(getString(R.string.strconnstatus));
		}
	}

	public Account createSyncAccount(Context context)
	{
		Account newaccount = new Account(Constants.ACCOUNT, Constants.ACCOUNT_TYPE) ;
		AccountManager mAccountManager = (AccountManager)context.getSystemService(Context.ACCOUNT_SERVICE) ;

		if(mAccountManager.addAccountExplicitly(newaccount, null, null))
		{
			ContentResolver.setSyncAutomatically(newaccount, ContentProviderSurvey.AUTHORITY, true);
			Log.v("CREATE ACCOUNT", "PROSURVEY  ACCOUNT IS GOOD TO GO") ;
		} else {
			Log.v("ACCOUNT EXISTS", "PROSURVEY  ACCOUNT FAILED TO BE SET UP IT MAY BE EXISTING") ;
		}
		return newaccount;

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_CODE_PICK_FILE) {
			if(resultCode == RESULT_OK) {
				String str1 = data.getStringExtra(PickFile.returnFileParameter);
				Toast.makeText(this, "Received FILE path from file browser:\n" + str1, Toast.LENGTH_LONG).show();
				appPreference.saveDbPath(str1);
				Log.v("DBPATH", "db path is " + str1);
				String[] arrayOfString = str1.split("/");
				String str2 = arrayOfString[(-1 + arrayOfString.length)];
				appPreference.saveDbnm(str2);
				Log.v("DBNAME", "dbname is " + str2);
				Intent localIntent = new Intent(getApplicationContext(), StartScreen.class);
				PendingIntent localPendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, localIntent, PendingIntent.FLAG_UPDATE_CURRENT);
				Context localContext = getApplicationContext();
				getApplicationContext();
				((AlarmManager) localContext.getSystemService(ALARM_SERVICE)).set(1, 100L + System.currentTimeMillis(), localPendingIntent);
				System.exit(0);

			} else {
				Toast.makeText(
						this,
						"Received NO result from file browser",
						Toast.LENGTH_LONG).show();
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

}
