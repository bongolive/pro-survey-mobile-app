package tz.co.bongolive.apps.prosurvey;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;

public class DbExportImport extends SQLiteOpenHelper
{
  public static final String DATABASE_NAME = "prosurvey";
  public static final int DATABASE_VERSION = 1;
  private static String DB_PATH = "";
  private static String TAG = "DBIMPORT";
  AppPreference appPreference;
  public final Context mContext;
  private SQLiteDatabase mDataBase;

  public DbExportImport(Context paramContext)
  {
    super(paramContext, "prosurvey", null, 1);
    StringBuilder localStringBuilder = new StringBuilder();
    DB_PATH = "/data/data/" + paramContext.getPackageName() + "/databases/";
    this.mContext = paramContext;
    AppPreference localAppPreference = new AppPreference(paramContext);
    this.appPreference = localAppPreference;
  }

  private boolean checkDataBase()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    File localFile = new File(DB_PATH + "prosurvey");
    return localFile.exists();
  }

  private void copyDataBase()
    throws IOException
  {
    FileInputStream localFileInputStream = readDb(this.appPreference.getDbpath());
    StringBuilder localStringBuilder = new StringBuilder();
    String str = DB_PATH + "prosurvey";
    FileOutputStream localFileOutputStream = new FileOutputStream(str);
    byte[] arrayOfByte = new byte[1024];
    while (true)
    {
      int i = localFileInputStream.read(arrayOfByte);
      if (i <= 0)
        break;
      localFileOutputStream.write(arrayOfByte, 0, i);
    }
    localFileOutputStream.flush();
    localFileOutputStream.close();
    localFileInputStream.close();
  }

  public static FileInputStream readDb(String paramString)
  {
    Object localObject = null;
    try
    {
      String str1 = Environment.getExternalStorageDirectory().toString();
      File localFile1 = new File(str1);
      if (!localFile1.exists())
        return null;
      File localFile2 = new File(paramString);
      String str2 = localFile2.getAbsolutePath();
      FileInputStream localFileInputStream = new FileInputStream(localFile2);

      Log.v("DBPATH", "DB PATH IS " + str2);
      return localFileInputStream;
    }
    catch (Exception localException)
    {
      while (true)
        localException.printStackTrace();
    }
  }

  public void close()
  {
    try
    {
      if (this.mDataBase != null)
        this.mDataBase.close();
      super.close();
      return;
    }
    finally
    {
    }
  }

  public void createDataBase()
    throws IOException
  {
    if (!checkDataBase())
    {
      getReadableDatabase();
      close();
      try
      {
        copyDataBase();
        Log.e(TAG, "createDatabase database created");
        return;
      }
      catch (IOException localIOException)
      {
        Error localError = new Error("ErrorCopyingDataBase");
        throw localError;
      }
    }
    this.mContext.deleteDatabase("prosurvey");
    createDataBase();
  }

  public void onCreate(SQLiteDatabase paramSQLiteDatabase)
  {
  }

  public void onUpgrade(SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
  {
    if (paramInt2 > paramInt1)
    {
      String str = TAG;
      StringBuilder localStringBuilder = new StringBuilder();
      Log.w(str, "migrating from database version" + paramInt1 + "to database version" + paramInt2 + "where all data are gon be erased");
    }
  }

  public boolean openDataBase()
    throws SQLException
  {
    StringBuilder localStringBuilder = new StringBuilder();
    this.mDataBase = SQLiteDatabase.openDatabase(DB_PATH + "prosurvey", null, 268435456);
    if (this.mDataBase != null);
    for (boolean bool = true; ; bool = false)
      return bool;
  }
}

/* Location:           /home/nasznjoka/Dev/Android/Tools/Decompilation/sources/SAFI 1.6.6_30/SAFI 1.6.6_30_dex2jar.jar
 * Qualified Name:     tz.co.bongolive.apps.prosurvey.DbExportImport
 * JD-Core Version:    0.6.2
 */