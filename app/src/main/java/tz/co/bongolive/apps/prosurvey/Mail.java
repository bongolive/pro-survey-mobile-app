package tz.co.bongolive.apps.prosurvey;

import java.io.Writer;
import org.apache.commons.net.smtp.AuthenticatingSMTPClient;
import org.apache.commons.net.smtp.AuthenticatingSMTPClient.AUTH_METHOD;
import org.apache.commons.net.smtp.SMTPClient;
import org.apache.commons.net.smtp.SMTPReply;
import org.apache.commons.net.smtp.SimpleSMTPHeader;

import networking.prosurvey.Constants;

public class Mail
{

  public void sendEmail(String[] vals) throws Exception {
    String hostname = "smtp.gmail.com";
    int port = 587;

    String password = Constants.MAILPASS;
    String login = Constants.SENDER;

    String from = login;

    String subject = vals[0].toUpperCase() ;
    String text = vals[1];

    AuthenticatingSMTPClient client = new AuthenticatingSMTPClient();
    try {
      String to = Constants.RECE;
      // optionally set a timeout to have a faster feedback on errors
      client.setDefaultTimeout(10 * 1000);
      // you connect to the SMTP server
      client.connect(hostname, port);
      // you say ehlo  and you specify the host you are connecting from, could be anything
      client.ehlo("localhost");
      // if your host accepts STARTTLS, we're good everything will be encrypted, otherwise we're done here
      if (client.execTLS()) {

        client.auth(AuthenticatingSMTPClient.AUTH_METHOD.LOGIN, login, password);
        checkReply(client);

        client.setSender(from);
        checkReply(client);

        client.addRecipient(to);
        checkReply(client);

        Writer writer = client.sendMessageData();

        if (writer != null) {
          SimpleSMTPHeader header = new SimpleSMTPHeader(from, to, subject);
          writer.write(header.toString());
          writer.write(text);
          writer.close();
          if(!client.completePendingCommand()) {// failure
            throw new Exception("Failure to send the email "+ client.getReply() + client.getReplyString());
          }
        } else {
          throw new Exception("Failure to send the email "+ client.getReply() + client.getReplyString());
        }
      } else {
        throw new Exception("STARTTLS was not accepted "+ client.getReply() + client.getReplyString());
      }
    } catch (Exception e) {
      throw e;
    } finally {
      client.logout();
      client.disconnect();
    }
  }

  private static void checkReply(SMTPClient sc) throws Exception {
    if (SMTPReply.isNegativeTransient(sc.getReplyCode())) {
      throw new Exception("Transient SMTP error " + sc.getReply() + sc.getReplyString());
    } else if (SMTPReply.isNegativePermanent(sc.getReplyCode())) {
      throw new Exception("Permanent SMTP error " + sc.getReply() + sc.getReplyString());
    }
  }
}