package tz.co.bongolive.apps.prosurvey;

import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import networking.prosurvey.Constants;
import networking.prosurvey.Validating;
import survery.database.Questions;
import survery.database.User;

/**
 * Created by nasznjoka on 5/18/15.
 */
public class UsersSync extends AsyncTask<String, Integer, Integer> {
    private static final String TAG = UsersSync.class.getName();
    Context context;
    ProgressBar bar;
    TextView txt;

    public UsersSync(Context context, ProgressBar pbar,TextView textView){
        this.context = context;
        this.bar = pbar;
        this.txt = textView;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        if (this.bar != null) {
            bar.setProgress(values[0]);
            txt.setText("saving users");
            bar.setMax(values[1]);
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        bar.setIndeterminate(false);
        txt.setText("downloading users");

       /* pDialog.setMessage("Please wait ...");
        pDialog.setCancelable(true);
        pDialog.show();*/
    }


    @Override
    protected Integer doInBackground(String... params) {
        JSONArray userArray;
        int[] ret = {0};
        JSONObject json = null;
        try {
            json = User.getCredentials(context);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Mail mail = new Mail();
        AppPreference appPreference = new AppPreference(context);
        try {
            if(json.has(User.ARRAYNAME)) {
                userArray = json.getJSONArray(User.ARRAYNAME);

                if (!TextUtils.isEmpty(json.getString(Constants.SUCCESS))) {
                    String res = json.getString(Constants.SUCCESS);
                    if (Integer.parseInt(res) == 1) {
                        int success = 0;
                        for (int i = 0; i < userArray.length(); i++) {
                            JSONObject js = userArray.getJSONObject(i);
                            String sid = js.getString(User.SYSTEMUSERID);
                            String un = js.getString(User.USERNAME);
                            String ps = js.getString(User.PASSWORD);
                            String rl = js.getString(User.ROLE);
                            String st = js.getString(User.STATUS);
                            String lc = js.getString(User.LOCATION);
                            String dc = js.getString(User.DATACOLLECTORNO);
                            String ac = js.getString(User.ACCOUNT);
                            String[] vals = new String[]{sid, un, ps, rl, st, lc, dc, ac};
                            if(Validating.areSet(vals)) {
                                if (User.isUserPresent(context, vals[0]) < 1) {
                                    if (User.storeCredentials(context, vals) == 1) {
                                        success += 1;
//                                        ret[0] = 1;
                                        publishProgress(i,userArray.length());
                                    }
                                } else {
                                    success += 1;
//                                    ret[0] = 1;
                                    publishProgress(i,userArray.length());
                                }
                            } else {
                                String subject = Constants.getIMEINO(context)+" null user";
                                String body = " User no "+sid+" username "+un+" account no" +ac+
                                        " has null value " +
                                        " today at "+Constants.getDate();
                                String[] msg = {subject,body};
                                try {
                                    mail.sendEmail(msg);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
//                                ret[0] = 1;
                                publishProgress(i,userArray.length());
                            }
                        }
                        if (success == userArray.length()) {
//                            ack(context,0);
                            ret[0] = 1;
//                            publishProgress(i,userArray.length());
                            appPreference.saveSyncStatus(Constants.USERSSYNC);
                        }
                    }
                }
            } else if (json.has(User.UPDATEARRAYNAME)){
                JSONArray jsonArray = json.getJSONArray(User.UPDATEARRAYNAME);
                for(int ui = 0; ui < jsonArray.length(); ui ++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(ui);
                    String sid = jsonObject.getString(User.SYSTEMUSERID);
                    if(User.isUserPresent(context, sid) == 1){
                        String rl = jsonObject.getString(User.ROLE);
                        String lc = jsonObject.getString(User.LOCATION);
                        String st = jsonObject.getString(User.STATUS);
                        String ps = jsonObject.getString(User.PASSWORD);
                        String un = jsonObject.getString(User.USERNAME);
                        String ac = jsonObject.getString(User.ACCOUNT);
                        String[] vals = new String[]{sid, rl, lc, ps, un};
                        if (Validating.areSet(vals)){
                            int uu = User.updateUser(context, vals);
                            if (uu == 1) {
//                                ack(context,1);
                                ret[0] = 1;
                                publishProgress(ui,jsonArray.length());
                                appPreference.saveSyncStatus(Constants.USERSSYNC);
                            }
                        }  else {
                            String subject = Constants.getIMEINO(context)+" null user";
                            String body = " User no "+sid+" username "+un+" account no" +ac+
                                    " has null value " +
                                    " today at "+Constants.getDate();
                            String[] msg = {subject,body};
                            try {
                                mail.sendEmail(msg);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
//                            ret[0] = 1;
                            publishProgress(ui,jsonArray.length());
                        }
                    }  else {
                        String siid = jsonObject.getString(User.SYSTEMUSERID);
                        String un = jsonObject.getString(User.USERNAME);
                        String ps = jsonObject.getString(User.PASSWORD);
                        String rl = jsonObject.getString(User.ROLE);
                        String st = jsonObject.getString(User.STATUS);
                        String lc = jsonObject.getString(User.LOCATION);
                        String dc = jsonObject.getString(User.DATACOLLECTORNO);
                        String ac = jsonObject.getString(User.ACCOUNT);
                        String[] vals = new String[]{siid, un, ps, rl, st, lc, dc, ac};
                        if (Validating.areSet(vals)) {
                            if (User.isUserPresent(context, vals[0]) < 1) {
                                if (User.storeCredentials(context, vals) == 1) {
//                                    ack(context,1);
                                    ret[0] = 1;
                                    publishProgress(ui,jsonArray.length());
                                    appPreference.saveSyncStatus(Constants.USERSSYNC);
                                }
                            } else {
//                                ack(context,1);
                                ret[0] = 1;
                                publishProgress(ui,jsonArray.length());
                                appPreference.saveSyncStatus(Constants.USERSSYNC);
                            }
                        } else {
                            String subject = Constants.getIMEINO(context)+" null user";
                            String body = " User no "+sid+" username "+un+" account no" +ac+
                                    " has null value " +
                                    " today at "+Constants.getDate();
                            String[] msg = {subject,body};
                            try {
                                mail.sendEmail(msg);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
//                            ret[0] = 1;
                            publishProgress(ui,jsonArray.length());
                        }
                    }
                }
            }
        } catch (JSONException e)
        {
            e.printStackTrace();
        }
        return ret[0] ;
    }

    protected void onPostExecute(final Integer ret) {
        super.onPostExecute(ret);
        AppPreference appPreference = new AppPreference(context);
        Log.v("returned", "returned is "+ret);

        if(appPreference.getSyU() == 4){
            if(ret == 1) {
                bar.setProgress(100);
                new ProcessAck(context).execute(new String[]{"users"});
                bar.setIndeterminate(true);
            }else {
                bar.setProgress(100);
            }
            new SettingsSync(context,bar,txt).execute();
        } else {
            bar.setProgress(100);
        new SettingsSync(context,bar,txt).execute();
        }
      /*
        if(ap.getSyncStats() > 4*//*User.getUserCount(context) > 0 && Survey.getCount(context) > 0 && Settings.getSettingsCount(context) > 0 &&
                Questions.getCount(context) > 50 && Responsechoice.getCount(context) > 150*//*){

            Toast.makeText(context," survey sync is complete you can login now ",Toast.LENGTH_SHORT).show();
            bar.setProgress(100);
        }*/
    }
}
