package tz.co.bongolive.apps.prosurvey;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;

import networking.prosurvey.Constants;


public class Settings extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
ListPreference listsync,listlang;
	CheckBoxPreference adminpref;
	EditTextPreference editTextPreference;
	int role ;
	AppPreference appPreference;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		addPreferencesFromResource(R.xml.settings);
		appPreference  = new AppPreference(this);
		listlang = (ListPreference)findPreference(Constants.PREF_LANG_KEY);
		listsync = (ListPreference)findPreference(Constants.PREF_SYNC_KEY);
		editTextPreference = (EditTextPreference) findPreference(Constants.PREF_URL);

		editTextPreference.setText(survery.database.Settings.getServerName(this));


		setListPreferenceData(listlang);
		listlang.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			@Override
			public boolean onPreferenceClick(Preference preference) {
				setListPreferenceData(listlang);
				return false;
			}
		});
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		if(key.equals(Constants.PREF_LANG_KEY))
		{
			Preference langpref = findPreference(key);
			langpref.setSummary(sharedPreferences.getString(key,""));
		}
		if(key.equals(Constants.PREF_SYNC_KEY))
		{
			Preference syncpref = findPreference(key);
			syncpref.setSummary(sharedPreferences.getString(key,""));
		}
	}

	protected static void setListPreferenceData(ListPreference lp) {

		lp.setEntries(R.array.languageSelection);
		lp.setEntryValues(R.array.languageSelectionValues);
		lp.setDefaultValue("1");
	}
}
