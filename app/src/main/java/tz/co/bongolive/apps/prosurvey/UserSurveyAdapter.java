package tz.co.bongolive.apps.prosurvey;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import networking.prosurvey.Validating;
import survery.database.Response;
import survery.database.Survey;
import survery.database.User;

public class UserSurveyAdapter extends BaseAdapter {
private LayoutInflater mInflater ;
private List<Survey> mItems = new ArrayList<Survey>() ;
Context _context ;
int userid ;
public UserSurveyAdapter(Context context, List<Survey> items, int user)
{
	mInflater = LayoutInflater.from(context);
	mItems = items ;
	this._context = context;
	this.userid = user ;
}  
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() { 
		return mItems.size() ;
	}
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public Object getItem(int position) { 
		return mItems.get(position);
	}
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int arg0) { 
		return 0;
	}
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) { 
		final ItemViewHolder holder ;
		if(convertView == null)
		{
			convertView = mInflater.inflate(R.layout.survey_active, null);
			holder = new ItemViewHolder() ;
			holder.txttitle = (TextView)convertView.findViewById(R.id.txtsurvname);
			holder.switchCompat = (SwitchCompat)convertView.findViewById(R.id.switchBtn);
			holder.switchCompat.setVisibility(View.GONE);
			convertView.setTag(holder);
		} else {
			holder = (ItemViewHolder)convertView.getTag();
		}
		final Survey cl = mItems.get(position); 
		if(Validating.areSet(cl.getAcc())){ 
			holder.txttitle.setText(cl.getTt()) ;
		} else {
		}
		
		holder.switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener
				() {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
				 holder.switchCompat.setChecked(true);
				} else {
					holder.switchCompat.setChecked(false);
				}
			}

		}) ;
		
        holder.txttitle.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				 Intent intent = new Intent(_context, Peruse.class);
				 intent.putExtra(Survey.ACCOUNT, cl.getAcc());
				 intent.putExtra(Survey.SURVEYNO, cl.getSno());
				 intent.putExtra(User.SYSTEMUSERID, String.valueOf(userid));
				int response = 0;
				response = Response.getNoResponse(_context, cl.getSno());
				if(response > 0) {
					_context.startActivity(intent);
				} else {

					Toast.makeText(_context, _context.getString(R.string.stremptyresponsesurvey)

							, Toast.LENGTH_LONG).show();
				}
			}
		}) ;
		return convertView ;
	} 
	
	
	static class ItemViewHolder {
		TextView txttitle ;
		SwitchCompat switchCompat ;
	}
  
}
