package tz.co.bongolive.apps.prosurvey;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import networking.prosurvey.Validating;
import survery.database.Participant;
 
public class PeruseAdapter extends BaseAdapter {
private LayoutInflater mInflater ;
private List<Participant> mItems = new ArrayList<Participant>() ;
Context _context ;
int userid ;
public PeruseAdapter(Context context, List<Participant> items, int user)
{
	mInflater = LayoutInflater.from(context);
	mItems = items ;
	this._context = context;
	this.userid = user ;
}  
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() { 
		return mItems.size() ;
	}
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public Object getItem(int position) { 
		return mItems.get(position);
	}
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int arg0) { 
		return 0;
	}
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) { 
		ItemViewHolder holder ;
		if(convertView == null)
		{
			convertView = mInflater.inflate(R.layout.peruse_items, null); 
			holder = new ItemViewHolder() ;
			holder.txtst = (TextView)convertView.findViewById(R.id.txtstarttime); 
			holder.txtdt = (TextView)convertView.findViewById(R.id.txtdate); 
			holder.txtloc = (TextView)convertView.findViewById(R.id.txtlocation); 
			holder.txtpid = (TextView)convertView.findViewById(R.id.txtpartid); 
			holder.txtstatus = (TextView)convertView.findViewById(R.id.txtstatus);
			holder.txtanc = (TextView)convertView.findViewById(R.id.txtanc);
			convertView.setTag(holder);
		} else {
			holder = (ItemViewHolder)convertView.getTag();
		}
		final Participant cl = mItems.get(position); 
		if(Validating.areSet(cl.getSt())){ 
			String dataconc = _context.getString(R.string.strdate);
			String starttime = _context.getString(R.string.strstarttime);
			String loca = _context.getString(R.string.strlocation);
			String pino = _context.getString(R.string.strpid);
			String pmctc = _context.getString(R.string.strpmstatus);
			String anc = _context.getString(R.string.stranc);
			String datacollect  = cl.getDataCollector();
			holder.txtdt.setText(dataconc + " : "+cl.getDt()) ;
			holder.txtloc.setText(loca + " : "+cl.getLoc()); 
			holder.txtst.setText(starttime + " : " +cl.getSt());
			holder.txtpid.setText(pino + " : "+cl.getPid());
			holder.txtstatus.setText(pmctc + " : "+cl.getPcmct());
			holder.txtanc.setText(anc + " : "+cl.getAnc());
		}  else {
			//holder.txtst.setText("no data");
			holder.txtst.setVisibility(View.GONE);
			holder.txtloc.setVisibility(View.GONE);
			holder.txtdt.setVisibility(View.GONE);
			holder.txtpid.setVisibility(View.GONE);
			holder.txtstatus.setVisibility(View.GONE);
			holder.txtanc.setVisibility(View.GONE);

		}
		 
		return convertView ;
	} 
	
	
	static class ItemViewHolder {
		TextView txtst,txtloc,txtdt,txtpid,txtstatus,txtanc;
	}

}
