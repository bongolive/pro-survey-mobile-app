package tz.co.bongolive.apps.prosurvey;

import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import networking.prosurvey.Constants;
import networking.prosurvey.Validating;
import survery.database.LogsTable;
import survery.database.Questions;
import survery.database.Responsechoice;

/**
 * Created by nasznjoka on 5/18/15.
 */
public class OptionsSync extends AsyncTask<String, Integer, Integer> {
    private static final String TAG = OptionsSync.class.getName();
    Context context;
    ProgressBar bar;
    TextView txt;

    public OptionsSync(Context context, ProgressBar pbar,TextView textView){
        this.context = context;
        this.bar = pbar;
        this.txt = textView;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        if (this.bar != null) {
            bar.setProgress(values[0]);
            txt.setText("saving question options");
            bar.setMax(values[1]);
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        bar.setIndeterminate(false);

       /* pDialog.setMessage("Please wait ...");
        pDialog.setCancelable(true);
        pDialog.show();*/
    }


    @Override
    protected Integer doInBackground(String... params) {

        int[] ret = {0};
        JSONArray userArray ;
        Mail mail = new Mail();
        AppPreference appPreference = new AppPreference(context);
        try{
            JSONObject json = Responsechoice.getResponseChoice(context);
            if(json.has(Responsechoice.ARRAYNAME)) {
                userArray = json.getJSONArray(Responsechoice.ARRAYNAME);

                if (!TextUtils.isEmpty(json.getString(Constants.SUCCESS))) {
                    String res = json.getString(Constants.SUCCESS);
                    if (Integer.parseInt(res) == 1) {
                        int succss = 0;
                        for (int i = 0; i < userArray.length(); i++) {
                            JSONObject js = userArray.getJSONObject(i);
                            String sid = js.getString(Responsechoice.SYSTEMCHOICEID);
                            String qn = js.getString(Responsechoice.QUESTIONNO);
                            String resp = js.getString(Responsechoice.RESPONSETEXT);
                            String nxt = js.getString(Responsechoice.NEXT);
                            String mt = js.getString(Responsechoice.MATCHWITH);
                            String surv = js.getString(Responsechoice.SURVEYNO);
                            String minv = js.getString(Responsechoice.MINVALUE);
                            String maxv = js.getString(Responsechoice.MAXVALUE);
                            String dt = js.getString(Responsechoice.DATATYPE);
                            String req = js.getString(Responsechoice.REQUIRED);
                            String chno = js.getString(Responsechoice.CHOICENO);
                            String[] vals = new String[]{sid, qn, resp, nxt, mt, surv,minv,maxv,dt,
                                    req,chno};
                            if(Validating.areSet(vals)){
                                if (Responsechoice.isResponsePresent(context, vals[0]) != 1) {

                                    if(Validating.areSet(vals)) {
                                        Log.v("response text",resp);
                                        int jk = Responsechoice.storeResponseChoice(context, vals);
                                        if (jk == 1) {
                                            succss += 1;
                                            publishProgress(i,userArray.length());
                                        }
                                    }
                                } else {
                                    succss +=1;
                                    publishProgress(i,userArray.length());
                                }
                            } else {
                                String subject = Constants.getIMEINO(context)+" null reponsechoice";
                                String body = " Choice no "+sid+" in question no "+qn+" survey no "+surv+" has null value " +
                                        " today at "+Constants.getDate();
                                String[] msg = {subject,body};
                                String[] logsdata = {"null value",body};
                                LogsTable.insert(context, logsdata);
                                try {
                                    mail.sendEmail(msg);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
//                                ret[0] = 1;
                                publishProgress(i,userArray.length());
                            }
                        }
                        if (succss == userArray.length()) {
//                            ack(context,0);
                            ret[0] = 1;
//                            publishProgress(i,userArray.length());
                            appPreference.saveSyncStatus(Constants.RESPONSESYNC);
                        }
                    }
                }
            } else if (json.has(Responsechoice.UPDATEARRAYNAME)){
                JSONArray jsonArray = json.getJSONArray(Responsechoice.UPDATEARRAYNAME);
                int succes = 0;
                int success_ = 0;
                for (int ii = 0; ii < jsonArray.length(); ii++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(ii);

                    if (Responsechoice.isResponsePresent(context, jsonObject.getString(Responsechoice.SYSTEMCHOICEID)) == 1) {
                        //the response choice is there so update it
                        String scid = jsonObject.getString(Responsechoice.SYSTEMCHOICEID);
                        String restxt = jsonObject.getString(Responsechoice.RESPONSETEXT);
                        String match = jsonObject.getString(Responsechoice.MATCHWITH);
                        String next = jsonObject.getString(Responsechoice.NEXT);
                        String minv = jsonObject.getString(Responsechoice.MINVALUE);
                        String maxv = jsonObject.getString(Responsechoice.MAXVALUE);
                        String dt = jsonObject.getString(Responsechoice.DATATYPE);
                        String req = jsonObject.getString(Responsechoice.REQUIRED);
                        String chno = jsonObject.getString(Responsechoice.CHOICENO);
                        String s = jsonObject.getString(Responsechoice.SURVEYNO);
                        String q = jsonObject.getString(Responsechoice.QUESTIONNO);
                        String[] vals = new String[]{scid, restxt, match, next,minv,maxv,dt,req,chno};
                        if(Validating.areSet(vals)) {
                            Log.v("response text",restxt);
                            int jk = Responsechoice.updateResponseChoice(context, vals);
                            if (jk == 1) {
                                succes += 1;
//                                ret[0] = 1;
                                publishProgress(ii,jsonArray.length());
                            }
                        } else {
                            String subject = Constants.getIMEINO(context)+" null reponsechoice";
                            String body = " Choice no "+scid+" in question no "+s+" survey no "+q+" has null value " +
                                    " today at "+Constants.getDate();
                            String[] msg = {subject,body};
                            String[] logsdata = {"null value",body};
                            LogsTable.insert(context,logsdata);
                            try {
                                mail.sendEmail(msg);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
//                            ret[0] = 1;
                            publishProgress(ii,jsonArray.length());
                        }
                    } else {
                        //
                        Log.v("responsechoicenew","the response choice is new on update flag " +
                                ""+jsonObject.getString(Responsechoice.SYSTEMCHOICEID));
                        String sid = jsonObject.getString(Responsechoice.SYSTEMCHOICEID);
                        String qn = jsonObject.getString(Responsechoice.QUESTIONNO);
                        String resp = jsonObject.getString(Responsechoice.RESPONSETEXT);
                        String nxt = jsonObject.getString(Responsechoice.NEXT);
                        String mt = jsonObject.getString(Responsechoice.MATCHWITH);
                        String surv = jsonObject.getString(Responsechoice.SURVEYNO);
                        String minv = jsonObject.getString(Responsechoice.MINVALUE);
                        String maxv = jsonObject.getString(Responsechoice.MAXVALUE);
                        String dt = jsonObject.getString(Responsechoice.DATATYPE);
                        String req = jsonObject.getString(Responsechoice.REQUIRED);
                        String cho = jsonObject.getString(Responsechoice.CHOICENO);
                        String[] vals = new String[]{sid, qn, resp, nxt, mt, surv,minv,maxv,dt,req,cho};
                        if(Validating.areSet(vals)){
                            if (Responsechoice.isResponsePresent(context, vals[0]) < 1) {

                                if(Validating.areSet(vals)) {
                                    int jk = Responsechoice.storeResponseChoice(context, vals);
                                    if (jk == 1) {
                                        success_ += 1;
//                                        ret[0] = 1;
                                        publishProgress(ii,jsonArray.length());
                                    }
                                }
                            } else {
                                success_ += 1;
//                                ret[0] = 1;
                                publishProgress(ii,jsonArray.length());
                            }
                        } else {
                            String subject = Constants.getIMEINO(context)+" null reponsechoice";
                            String body = " Choice no "+sid+" in question no "+qn+" survey no "+surv+" has null value " +
                                    " today at "+Constants.getDate();
                            String[] msg = {subject,body};
                            String[] logsdata = {"null value",body};
                            LogsTable.insert(context,logsdata);
                            try {
                                mail.sendEmail(msg);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
//                            ret[0] = 1;
                            publishProgress(ii,jsonArray.length());
                        }
                    }
                }
                if(succes > 0 || success_ > 0){
//                    ack(context,1);
                    ret[0] = 1;
//                    publishProgress(ii,jsonArray.length());
                    appPreference.saveSyncStatus(Constants.RESPONSESYNC);
                }
            }
        } catch (JSONException e)
        {
            e.printStackTrace();
        }
        return ret[0] ;
    }

    protected void onPostExecute(final Integer ret) {
        super.onPostExecute(ret);
        AppPreference appPreference = new AppPreference(context);
        Log.v("returned", "returned is "+ret);

        if(appPreference.getSyO() == 3){
            if(ret == 1){
                bar.setProgress(100);
                new ProcessAck(context).execute(new String[]{"responses"});
                bar.setIndeterminate(true);
            }else {
                bar.setProgress(100);
            }
            new UsersSync(context,bar,txt).execute();

        } else {
            bar.setProgress(100);
        new UsersSync(context,bar,txt).execute();
        }
    }
}
