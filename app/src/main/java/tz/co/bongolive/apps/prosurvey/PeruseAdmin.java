/**
 * 
 */
package tz.co.bongolive.apps.prosurvey;

import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import survery.database.Participant;
import survery.database.Response;
import survery.database.Survey;
import survery.database.User;

/**
 * @author nasznjoka
 *
 */
public class PeruseAdmin extends ActionBarActivity implements OnItemClickListener{
/*
* to do make sure that when the admin is viewing surveys he see all.
* If the admin is perusing on the user's response see only user's related response
* all under this page so either change it or leave it to be the same
* */
	PeruseAdapter adapter ;
	ListView listview;
	int survey ;
	String[] data = null ;
	int role ;
	AppPreference appPreference;
	String datacollector = "";
	String title = "";
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.list_items);

		appPreference = new AppPreference(getApplicationContext());
		role = appPreference.getUserRole();
		listview = (ListView)findViewById(R.id.list);
		Intent intent = getIntent();
		String sacc = intent.getStringExtra(Survey.ACCOUNT);
		String ssno = intent.getStringExtra(Survey.SURVEYNO);
		String userid = intent.getStringExtra(User.SYSTEMUSERID);
		Log.v("alldata", sacc + " account\n" + ssno + " survey no\n" + userid);
		data = new String[]{sacc,ssno,userid};
		 adapter = new PeruseAdapter(this, getList(data), Integer.parseInt(data[2]));
	      listview.setAdapter(adapter);
	      listview.setOnItemClickListener(this); 
	      datacollector = String.valueOf(User.getDataCollector(this, Integer.parseInt(userid)));

		  title = Survey.getTitle(this, data);
		  title = getString(R.string.strsurveytitle)+ " "+title;
	      ActionBar ab = getSupportActionBar();
	      ab.setHomeButtonEnabled(true);
		  ab.setDisplayShowHomeEnabled(true);
		  ab.setDisplayHomeAsUpEnabled(true);
	      ab.setDisplayUseLogoEnabled(true);
		  ab.setDisplayShowTitleEnabled(true);
		  ab.setTitle(title.toUpperCase());
//	      ab.setLogo(R.drawable.ic_arrow_back_white_36dp);
	}
	public ArrayList<Participant> getList(String[] acc){
		ContentResolver cr = getContentResolver() ;
		String datacollector = String.valueOf(User.getDataCollector(this, Integer.parseInt(acc[2])));
		String where = "";
		if(role == 0) {
			where = Participant.SURVEYNO + " = " + acc[1] + " AND " + Participant.INTERVIEWERNO +
					" = " + datacollector;
		} else if (role == 1)
		{
			where = Participant.SURVEYNO + " = " + acc[1] ;
		}
        Cursor c = cr.query(Participant.BASEURI, null, where, null,Participant.SURVEYNO+ " ASC");
        System.out.println(" cursor is "+ c.getCount());
        ArrayList<Participant> list = new ArrayList<Participant>();
        try{
        if(c.getCount() > 0)
        { 
        	c.moveToFirst() ;
        	do{
                int st = c.getColumnIndex(Participant.STARTTIME);
                int et = c.getColumnIndex(Participant.ENDTIME);
                int dt = c.getColumnIndex(Participant.INTDATE);
                int lc = c.getColumnIndex(Participant.LOCATIONNO);
                int pi= c.getColumnIndex(Participant.PARTICIPANTNO);
                int pcmtcid= c.getColumnIndex(Participant.STATUS);
                int ancid= c.getColumnIndex(Participant.ANC);
                String stt = c.getString(st);
                String ent = c.getString(et);
                String d = c.getString(dt);
                String loc = c.getString(lc);
                String pno = c.getString(pi);
                String pmtc = c.getString(pcmtcid);
                String anc = c.getString(ancid);
                list.add(new Participant(stt+" - " +ent, d, loc, pno, pmtc,anc));
        	} while (c.moveToNext()) ; 
        } else {
        	String u = "no data" ;  
        	String d = null ;
        	list.add(new Participant(u, d, d,d,d,d));
        } 
        } finally{
        	if(c != null) {
	            c.close();
	        }
        }
        return list ;
	}
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) { 
		 
		Object o = adapter.getItem(position);
		Participant p = (Participant)o; 
		String[] args = {p.getPid(),data[1]} ;
		Log.v("DATA", " THE SURVY IS "+ args[1] + " participant "+args[0]);
		showResponse(this, args);
	}
	
 
	private void showResponse(Context context, String[] args) {
		final Dialog initiald = new Dialog(context, R.style.CustomDialog);
		initiald.setCancelable(true);
		LayoutInflater infl = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		 
		View view = infl.inflate(R.layout.list, null);
		initiald.setContentView(view);
		ListView l = (ListView)initiald.findViewById(R.id.listview);
		ResponseAdapter _adapter = new ResponseAdapter(this, getListResponse(args));
		l.setAdapter(_adapter);
		TextView txttitl = (TextView)initiald.findViewById(R.id.txttitle);
		txttitl.setText(title.toUpperCase());
		ImageView img = (ImageView)initiald.findViewById(R.id.goback);
		img.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(initiald.isShowing())
					initiald.dismiss();
			}
		});

		initiald.show();
	}
	
	public ArrayList<Response> getListResponse(String[] acc){
		ContentResolver cr = getContentResolver() ;  
		
		String where = Response.SURVEYNO + " = " + acc[1] +" AND "
				+ Response.PARTICIPANTID + " = " + acc[0] + " AND "
				+ Response.ACK + " != 3" ;
        Log.v("WHERE", where);
        Cursor c = cr.query(Response.BASEURI, null, where, null,Response.QUESTIONNO+ " ASC");
        Log.v("THE OUTPUT", "IF CURSOR SIZE IS 0 THAT MEANS THE OUTPUT IS ZERO AND IT IS "+c.getCount());
        ArrayList<Response> list = new ArrayList<Response>();
        try{
        if(c.getCount() > 0)
        { 
        	c.moveToFirst() ;
        do{
        	int st = c.getColumnIndex(Response.SURVEYNO);
        	int qn = c.getColumnIndex(Response.QUESTIONNO);
        	int re = c.getColumnIndex(Response.RESPONSE);
        	int pi= c.getColumnIndex(Response.PARTICIPANTID);
        	String suv = c.getString(st);
        	String qns = c.getString(qn);
        	String r = c.getString(re);
        	String _p = c.getString(pi);
            System.out.println(" qn "+ qns + " response "+r);
        	list.add(new Response(_p,qns,suv,r));
        } while(c.moveToNext());
        } else {
        	String u = "no data" ;  
        	String d = "" ;
        	list.add(new Response(u, d, d,d)); 
        }
        
        } finally{
        	if(c != null) {
	            c.close();
	        }
        }
        return list ;
	}
	 @Override
     public boolean onOptionsItemSelected(MenuItem item) {
         switch(item.getItemId())
         {
         case android.R.id.home:
//	         NavUtils.navigateUpFromSameTask(this);
			 Intent intent = new Intent(this, Dashboard.class);
			 intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			 startActivity(intent);
	         return true;
         }
         return super.onOptionsItemSelected(item);
     }
}