package tz.co.bongolive.apps.prosurvey;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.util.Log;
import android.widget.Toast;

import networking.prosurvey.Constants;
import survery.database.*;


public class PrefActivity extends PreferenceActivity implements SharedPreferences
		.OnSharedPreferenceChangeListener {

	private final int REQUEST_CODE_PICK_DIR = 1;
	private final int REQUEST_CODE_PICK_FILE = 2;
	final Activity activityForButton = this;
	CheckBoxPreference adminpref;
	AppPreference appPreference;
	CheckBoxPreference dbimportpref;
	EditTextPreference editTextPreference;
	CheckBoxPreference exportdata;
	ListPreference listlang;
	ListPreference listsync;
	CheckBoxPreference resetsyncpref;
	int role;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		addPreferencesFromResource(R.xml.admin);
		appPreference = new AppPreference(getApplicationContext());
		listlang = (ListPreference) findPreference(Constants.PREF_LANG_KEY);
		listsync = (ListPreference) findPreference(Constants.PREF_SYNC_KEY);
		adminpref = (CheckBoxPreference) findPreference(Constants.PREF_ADMIN_KEY);
		resetsyncpref = (CheckBoxPreference) findPreference(Constants.PREF_RESETSYNC_KEY);
		exportdata = (CheckBoxPreference) findPreference(Constants.PREF_EXPORT_KEY);
		dbimportpref = (CheckBoxPreference) findPreference(Constants.PREF_IMPORT_KEY);
		editTextPreference = (EditTextPreference) findPreference(Constants.PREF_URL);

		editTextPreference.setText(survery.database.Settings.getServerName(this));

		int rolestatus = appPreference.getAdminStatus(this);
		if (rolestatus == 1) {
			adminpref.setChecked(true);
		} else if (rolestatus == 0) {
			adminpref.setChecked(false);
		}


//		exportdata.setChecked(false);
		exportdata.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			@Override
			public boolean onPreferenceClick(Preference preference) {
				if (exportdata.isChecked()) {
//					Dialog d = new Dialog(getApplicationContext());
					ExportDatabase ex = new ExportDatabase(PrefActivity.this, exportdata);
					ex.execute();
				}
				return false;
			}
		});

		if (dbimportpref.isChecked()) {
			dbimportpref.setChecked(false);
			dbimportpref.getEditor().putBoolean(Constants.PREF_IMPORT_KEY, false).commit();
		}

		dbimportpref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			@Override
			public boolean onPreferenceClick(Preference preference) {
				if (dbimportpref.isChecked()) {
					Intent localIntent = new Intent("SELECT_FILE_ACTION", null, activityForButton, PickFile.class);
					localIntent.putExtra("showCannotRead", false);
					startActivityForResult(localIntent, REQUEST_CODE_PICK_FILE);
				}
				return false;
			}
		});

		resetsyncpref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			@Override
			public boolean onPreferenceClick(Preference preference) {
				if (resetsyncpref.isChecked()) {
					int part = Participant.resetSync(getApplicationContext());
					int res = Response.resetSync(getApplicationContext());
					if (part == 1 && res == 1) {
						AlertDialogManager al = new AlertDialogManager();
						al.showAlertDialog(PrefActivity.this, "SYNC MASTER RESET",
								"The reset is successful wait for it to start pushing", false);
						resetsyncpref.setChecked(false);
					}
				}
				return false;
			}
		});
		role = appPreference.getUserRole();
		if (role == 1) {
			adminpref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
				@Override
				public boolean onPreferenceClick(Preference preference) {
					if (adminpref.isChecked())
						appPreference.makeAdminNormal(getApplicationContext(), 1);
					if (!adminpref.isChecked())
						appPreference.makeAdminNormal(getApplicationContext(), 0);
					return false;
				}
			});
		}

		setListPreferenceData(listlang);
		listlang.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			@Override
			public boolean onPreferenceClick(Preference preference) {
				setListPreferenceData(listlang);
				return false;
			}
		});
	}


	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		if (key.equals(Constants.PREF_LANG_KEY)) {
			Preference langpref = findPreference(key);
			langpref.setSummary(sharedPreferences.getString(key, ""));

			/*Intent inte = new Intent(getApplicationContext(), Dashboard.class);
			inte.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(inte);
			finish();*/
		}
		if (key.equals(Constants.PREF_SYNC_KEY)) {
			Preference syncpref = findPreference(key);
			syncpref.setSummary(sharedPreferences.getString(key, ""));
		}
	}

	protected static void setListPreferenceData(ListPreference lp) {

		lp.setEntries(R.array.languageSelection);
		lp.setEntryValues(R.array.languageSelectionValues);
		lp.setDefaultValue(0);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_CODE_PICK_FILE) {
			if(resultCode == RESULT_OK) {
				String str1 = data.getStringExtra(PickFile.returnFileParameter);
				Toast.makeText(this, "Received FILE path from file browser:\n" + str1, Toast.LENGTH_LONG).show();
				appPreference.saveDbPath(str1);
				Log.v("DBPATH", "db path is " + str1);
				String[] arrayOfString = str1.split("/");
				String str2 = arrayOfString[(-1 + arrayOfString.length)];
				appPreference.saveDbnm(str2);
				Log.v("DBNAME", "dbname is " + str2);
				Intent localIntent = new Intent(getApplicationContext(), StartScreen.class);
				PendingIntent localPendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, localIntent, PendingIntent.FLAG_UPDATE_CURRENT);
				Context localContext = getApplicationContext();
				getApplicationContext();
				((AlarmManager) localContext.getSystemService(ALARM_SERVICE)).set(1, 100L + System.currentTimeMillis(), localPendingIntent);
				System.exit(0);

			} else {
				Toast.makeText(
						this,
						"Received NO result from file browser",
						Toast.LENGTH_LONG).show();
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
}
