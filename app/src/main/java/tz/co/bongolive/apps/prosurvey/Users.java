package tz.co.bongolive.apps.prosurvey;

import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import networking.prosurvey.Constants;
import networking.prosurvey.Validating;
import survery.database.Survey;
import survery.database.User;

public class Users extends Fragment implements OnItemClickListener {
	private static final String ARG_SECTION_NUMBER = "section_number";
	View _rootView;
	UsersAdapter adapter ;
	ListView listview;
	AppPreference appPreference;
	int statusflag;
	Dialog initiald;
	String[] chpassdata;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		if(_rootView == null)
		{
			_rootView = inflater.inflate(R.layout.list_items, container, false);
		} else {
			((ViewGroup)_rootView.getParent()).removeView(_rootView) ;
		}
		return _rootView ;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		((Dashboard)getActivity()).onSectionAttached(getArguments().getInt(ARG_SECTION_NUMBER));
		listview = (ListView)getActivity().findViewById(R.id.list);

		appPreference = new AppPreference(getActivity());
		statusflag = appPreference.getUserId();// superuserid
		int useraccount = User.getUserAccount(getActivity(), statusflag);
		adapter = new UsersAdapter(getActivity(), getList(useraccount));
		listview.setAdapter(adapter);
		listview.setOnItemClickListener(this);
      /*  
        System.out.println(" args is "+ statusflag);
        GeneralViews.initialQuestion(getActivity(), statusflag);*/
	}
	public ArrayList<User> getList(int account){
		ContentResolver cr = getActivity().getContentResolver() ;
		String where = User.ACCOUNT + " = " + account
				+" AND " + User.ROLE +" = 0";
		Cursor c = cr.query(User.BASEURI, null, where, null,User.SYSTEMUSERID+ " ASC");
		ArrayList<User> list = new ArrayList<User>();
		try{
			if(c.getCount() > 0)
			{
				c.moveToFirst() ;
				do{
					int name = c.getColumnIndex(User.USERNAME);
					int acc = c.getColumnIndex(User.ACCOUNT);
					int dcn = c.getColumnIndex(User.DATACOLLECTORNO);
					int lc= c.getColumnIndex(User.LOCATION);
					int uid = c.getColumnIndex(User.SYSTEMUSERID);
					String t = c.getString(name);
					String a = c.getString(acc);
					String l = c.getString(lc);
					String sno = c.getString(dcn);
					String sid = c.getString(uid);
					list.add(new User(t,sid,l,a,sno));
				} while (c.moveToNext()) ;
			} else {
				String u = "no data" ;
				String d = "" ;
				list.add(new User(u, d, d,d,d));
			}
		} finally{
			if(c != null) {
				c.close();
			}
		}
		return list ;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		Object o = adapter.getItem(position);
		User user = (User)o ;
		String sno = user.getSysid();
		showSingleUser(getActivity(), new String[]{user.getName(), user.getAccount(),
				user.getDatacoll(), user.getSysid(),user.getLoc()});//name,acc,dcn,uid
	}

	private void showSingleUser(Context context, final String[] args) {
		final Dialog initiald = new Dialog(context, R.style.CustomDialog);
		initiald.setCancelable(true);
		LayoutInflater infl = (LayoutInflater)context.getSystemService(Context
				.LAYOUT_INFLATER_SERVICE);

		View view = infl.inflate(R.layout.singleuser, null);
		initiald.setContentView(view);

//		args = //name,acc,dcn,uid
		ImageView chnguser ;
		chnguser = (ImageView)initiald.findViewById(R.id.settings);
		chnguser.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showChangePass(getActivity(), args);
			}
		});

		TextView tn,tloc;
		tn = (TextView)initiald.findViewById(R.id.txtnm);
		tloc = (TextView)initiald.findViewById(R.id.txtloc);
		String n = tn.getText().toString() + " " +args[0];
		tn.setText(n);
		String l = tloc.getText().toString() + " " +args[4];
		tloc.setText(l);


		 int width = ViewGroup.LayoutParams.MATCH_PARENT;
		int height = ViewGroup.LayoutParams.MATCH_PARENT ;
		 int viewheight = ViewGroup.LayoutParams.WRAP_CONTENT ;
		LinearLayout ll = (LinearLayout)initiald.findViewById(R.id.laycontent);
		ArrayList<String> list = Survey.getSurveys(getActivity(), Integer.parseInt(args[1]));
		ListView listView = new ListView(getActivity());
		if(list.size() > 0) {
//name,acc,dcn,uid
			UserSurveyAdapter adapter1 = new UserSurveyAdapter(getActivity(),getListSurvey(args[1]),
					Integer.parseInt(args[3]));
			listView.setAdapter(adapter1);
			listView.setVerticalScrollBarEnabled(false);
			ll.addView(listView, width, viewheight);
			Utility.getListViewSize(listView);
				}


		ImageView img = (ImageView)initiald.findViewById(R.id.goback);
		img.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (initiald.isShowing())
					initiald.dismiss();
			}
		});

		initiald.show();
	}


	private void showChangePass(Context context, final String[] args) {
		final Dialog dd = new Dialog(context, R.style.CustomDialog);
		dd.setCancelable(true);
		LayoutInflater infl = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View view = infl.inflate(R.layout.changepass, null);
		dd.setContentView(view);

		Button submit;
		submit = (Button) dd.findViewById(R.id.btnsubmit);

		submit.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				EditText p1, p2;
				p1 = (EditText) dd.findViewById(R.id.etpass);
				p2 = (EditText) dd.findViewById(R.id.etconfirmpass);
				String pass = p1.getText().toString();
				String conf = p2.getText().toString();
				String[] da = {pass, conf};
				if (Validating.areSet(da)) {
					if (da[0].equals(da[1])) {
//						handle the action
						try {
							String hashedpass = Constants.makeSHA1Hash(da[0]);
							String[] passd = {args[3], hashedpass};
							int update = User.changePass(getActivity(), passd);
							if (update == 1) {
								p1.setText("");p2.setText("");
								Toast.makeText(getActivity(), getString(R.string.strchngpasssuccess),
										Toast.LENGTH_LONG).show();
							}
						} catch (NoSuchAlgorithmException e) {
							e.printStackTrace();
						}
					}
					else {
						p1.setText("");
						p2.setText("");
						Toast.makeText(getActivity(), getString(R.string.strpassdontmatch),
								Toast.LENGTH_LONG).show();
					}
				}
				else {
					p1.setText("");
					p2.setText("");
					p1.setHintTextColor(Color.RED);
					p2.setHintTextColor(Color.RED);
				}
			}
	});
		ImageView img = (ImageView)dd.findViewById(R.id.goback);
		img.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(dd.isShowing())
					dd.dismiss();
			}
		});
		dd.show();
	}

	public ArrayList<Survey> getListSurvey(String acct){
		ContentResolver cr = getActivity().getContentResolver() ;
		String where = Survey.ACCOUNT + " = " + acct ;
		Cursor c = cr.query(Survey.BASEURI, null, where, null,Survey.SURVEYNO+ " ASC");
		ArrayList<Survey> list = new ArrayList<Survey>();
		try{
			if(c.getCount() > 0)
			{
				c.moveToFirst() ;
				do{
					int title = c.getColumnIndex(Survey.TITLE);
					int acc = c.getColumnIndex(Survey.ACCOUNT);
					int sta = c.getColumnIndex(Survey.STATUS);
					int sn= c.getColumnIndex(Survey.SURVEYNO);
					String t = c.getString(title);
					String a = c.getString(acc);
					String s = c.getString(sta);
					String sno = c.getString(sn);
					list.add(new Survey(t, a,s,sno));
				} while (c.moveToNext()) ;
			} else {
				String u = "no data" ;
				String d = "" ;
				list.add(new Survey(u, d, d,d));
			}
		} finally{
			if(c != null) {
				c.close();
			}
		}
		return list ;
	}
}
