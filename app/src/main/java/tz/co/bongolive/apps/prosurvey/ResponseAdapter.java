package tz.co.bongolive.apps.prosurvey;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import networking.prosurvey.Constants;
import networking.prosurvey.Validating;
import survery.database.Participant;
import survery.database.Questions;
import survery.database.Response;
import survery.database.Responsechoice;
 
public class ResponseAdapter extends BaseAdapter {
private LayoutInflater mInflater ;
private List<Response> mItems = new ArrayList<Response>() ;
Context _context ;
int userid ;
public ResponseAdapter(Context context, List<Response> items)
{
	mInflater = LayoutInflater.from(context);
	mItems = items ;
	this._context = context; 
}  
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() { 
		return mItems.size() ;
	}
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public Object getItem(int position) { 
		return mItems.get(position);
	}
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int arg0) { 
		return 0;
	}
	
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) { 
		ItemViewHolder holder ;

		final AppPreference appPreference = new AppPreference(_context.getApplicationContext());
		if(convertView == null)
		{
			convertView = mInflater.inflate(R.layout.responseitem, null); 
			holder = new ItemViewHolder() ;
			holder.txtqn = (TextView)convertView.findViewById(R.id.txtquestion); 
			holder.txtres = (TextView)convertView.findViewById(R.id.txtresponse);  
			holder.txtqntext = (TextView)convertView.findViewById(R.id.txtqntext);   
			convertView.setTag(holder);
		} else {
			holder = (ItemViewHolder)convertView.getTag();
		}
		final Response cl = mItems.get(position); 
		if(Validating.areSet(cl.getPartid())){  
			holder.txtqn.setText(cl.getQnno()) ;
			int qn = Integer.parseInt(cl.getQnno());
			int sur = Integer.parseInt(cl.getSurvno());
			String[] getQuestion = {cl.getQnno(), cl.getSurvno(), appPreference.getDefaultLanguage()};
			String qntext =  Questions.getQuestionText(_context, getQuestion);
			holder.txtqntext.setText(qntext);
			String respoonse = cl.getRespo();
			respoonse = respoonse.replace(Constants.SEPARATOR, ",");
			holder.txtres.setText(respoonse);
		}

		final int r = appPreference.getUserRole();
		final int admin = appPreference.getAdminStatus(_context);

		if(r == 0 || admin == 1) {
			if (Validating.areSet(cl.getPartid())) {
				holder.txtres.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						String rank, next, prev, participant, flag, survey, interviewerno,questionid;
						questionid = cl.getQnno();
//						rank = Questions.getQuestionRankUsingId(_context,questionid);
						rank = cl.getQnno();
						int rankint = Integer.parseInt(rank);
						participant = cl.getPartid();
						prev = cl.getPrevqn();

						flag = String.valueOf(appPreference.getUserId());
						survey = cl.getSurvno();
						String[] vals_ = {participant, survey};
						interviewerno = String.valueOf(Participant.getDataCollector(_context,vals_));
						int userid = Participant.getUserId(_context,interviewerno, survey);
						//rank//next/prev

						String[] info = new String[6];
						if(r == 0) {
							 info = new String[]{rank, rank, prev, participant, flag, survey,
									interviewerno};
						} else if(r == 1)
						{
							info = new String[]{rank, rank, prev, participant,
								String.valueOf(userid),
									survey,
									interviewerno};
						}
						GeneralViews.getNextQuestion(_context, info);
					}
				});
			}
		}
		return convertView ;
	} 
	
	
	static class ItemViewHolder {
		TextView txtqn,txtqntext, txtres ;
	}

}
