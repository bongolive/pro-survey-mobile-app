package tz.co.bongolive.apps.prosurvey;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class Home extends Fragment {
	private static final String ARG_SECTION_NUMBER = "section_number"; 
	View _rootView;
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {  
		if(_rootView == null)
		{
			_rootView = inflater.inflate(R.layout.single, container, false);
		} else {
			((ViewGroup)_rootView.getParent()).removeView(_rootView) ;
		}
		return _rootView ;
    } 
	
	@Override
    public void onActivityCreated(Bundle savedInstanceState)
    { 
        super.onActivityCreated(savedInstanceState);
        ((Dashboard)getActivity()).onSectionAttached(getArguments().getInt(ARG_SECTION_NUMBER)); 
     }
}
