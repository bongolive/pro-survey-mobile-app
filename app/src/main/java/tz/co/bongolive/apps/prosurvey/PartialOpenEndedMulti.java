/**
 * 
 */
package tz.co.bongolive.apps.prosurvey;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import networking.prosurvey.Constants;
import networking.prosurvey.Validating;
import survery.database.Participant;
import survery.database.Questions;
import survery.database.Response;
import survery.database.Responsechoice;
import survery.database.Survey;
import survery.database.User;

/**
 *    @author nasznjoka
 *
 *    
 *    Dec 2, 2014
 */
@SuppressLint("DefaultLocale") public class PartialOpenEndedMulti {

	public static void multiselectother(final Context context, final String[] info)
	{
		final Dialog partiald = new Dialog(context, R.style.CustomDialog);
		partiald.setCancelable(false);

		final int rank = Integer.parseInt(info[0]) ;
		int next = Integer.parseInt(info[1]) ;
		final int prev = Integer.parseInt(info[2]);
		final int participant = Integer.parseInt(info[3]);
		final String flag= info[4];
		final int survey = Integer.parseInt(info[5]);
		final String interviewerno = info[6] ;
		final ArrayList<String> checkedvalue = new ArrayList<String>();
		final ArrayList<String> checkedvalueid = new ArrayList<String>();
		LayoutInflater infl = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = infl.inflate(R.layout.questions, null);
		AppPreference appPreference = new AppPreference(context.getApplicationContext());
		final String language = appPreference.getDefaultLanguage();

		final int questionid = Questions.getQuestionId(context, new int[]{rank,survey},language);
		final ArrayList<String> list = Responsechoice.getQuestionAnswer(context, questionid) ;
		final ArrayList<String> listchoice = Responsechoice.getQuestionAnswerId(context,
				questionid, survey) ;
		final int listsize = list.size();
		String othertextquestion = list.get(list.size() - 1) ;
		partiald.setContentView(view);

		final int width = LayoutParams.MATCH_PARENT;
		int height = LayoutParams.MATCH_PARENT ;
		final int viewheight = LayoutParams.WRAP_CONTENT ;
		final  int[] checkupdate = {0};

		final LinearLayout layout = (LinearLayout)partiald.findViewById(R.id.qnlayout);

		String account = String.valueOf(User.getUserAccount(context,
				Integer.parseInt(flag)));
		String titlee = Survey.getTitle(context, new String[]{account, String.valueOf(survey)});
		TextView txttitle = (TextView)partiald.findViewById(R.id.txttitle);
		txttitle.setText(titlee.toUpperCase());

		TextView title = (TextView)partiald.findViewById(R.id.txtqno);
		title.setText(context.getString(R.string.strquestionnotitle)+ " "+ rank) ;


		TextView dc = (TextView)partiald.findViewById(R.id.txtqn);
		dc.setText(Questions.getQuestionTextLanguage(context,  new int[]{questionid, survey}));

		final CheckBox[] cb = new CheckBox[listsize] ;
		final EditText et = new EditText(context);
		et.setHint(othertextquestion);


		for(int i = 0; i < listsize; i++) {
			String value = list.get(i);
			cb[i] = new CheckBox(context);
			cb[i].setText(value);
			layout.addView(cb[i], width, viewheight);
		}


		layout.addView(et, width, viewheight);
//		et.setVisibility(View.GONE);
		GeneralViews.setDataType(context, listchoice.get(listchoice.size() - 1), et);
		int datatype = Responsechoice.getDataType(context, listchoice.get(listchoice.size()-1));
		if(datatype == 3 || datatype == 4){

			et.setFocusable(true);
			et.setFocusableInTouchMode(false);
			et.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					et.setFocusable(true);
					et.setFocusableInTouchMode(true);
				}
			});
			et.setOnFocusChangeListener(new View.OnFocusChangeListener() {
				@Override
				public void onFocusChange(View v, boolean hasFocus) {
					if (hasFocus) {
						GeneralViews.setDateTimeField(context, et);
					}
				}
			});
		}

		final Button bedit = (Button)partiald.findViewById(R.id.btnedit);
		final Button bst = (Button)partiald.findViewById(R.id.goforward);

		bedit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				int validate = 0;
				for (int i = 0; i < listsize; i++) {
					cb[i].setEnabled(true);
				}
				bedit.setText(context.getString(R.string.strupdate));
				checkupdate[0] = 1;
				for (int i = 0; i < listsize; i++) {
					if(cb[i].isChecked())
					{
						String q = cb[i].getText().toString();
						String qid = "";
						if(i != listsize-1) {
							qid = listchoice.get(i);
						}
						if(!q.isEmpty() && !qid.isEmpty()){
							checkedvalue.add(q.concat(Constants.SEPARATOR));
							checkedvalueid.add(qid.concat(Constants.SEPARATOR));
						}
					}
					String req = listchoice.get(i);
					int reqq = Responsechoice.isQuestionRequired(context, req);
					if(reqq == 1){
						if(!cb[i].isChecked()) {
							Toast.makeText(context, context.getString(R.string.strreqres) + " " +

											list.get(i),
									Toast.LENGTH_LONG).show();
							validate += 1;
						}
					}
				}

				String ans = "" ;
				String ansid = "";
				if(!checkedvalue.isEmpty()) {
					for (int i = 0; i < checkedvalue.size(); i++) {
						ans += checkedvalue.get(i);
					}
				}
				if(!checkedvalueid.isEmpty()) {
					for (int i = 0; i < checkedvalueid.size(); i++) {
						ansid += checkedvalueid.get(i);
					}
				}
				String oid = listchoice.get(listsize-1);
//				if(cb[listsize-1].isChecked()) {
//					et.setVisibility(View.VISIBLE);
				if(!ans.isEmpty() && !ansid.isEmpty()) {
					String o = et.getText().toString();
					String ansetid = String.valueOf(listchoice.size() - 1);
					int datatype_ = Responsechoice.getDataType(context, ansetid);
					String minvalue = Responsechoice.getMin(context, ansetid);
					String maxvalue = Responsechoice.getMax(context, ansetid);
					switch (datatype_) {
						case 1:
							if (!o.isEmpty()) {
								int ansint = Integer.parseInt(o);
								int min = Integer.parseInt(minvalue);
								int max = Integer.parseInt(maxvalue);
								if (!(ansint >= min)) {
									Toast.makeText(context, context.getString(R.string.strmin)
													+ " " + min,
											Toast.LENGTH_LONG).show();
									et.setText("");
									o = "";
								}
								if (max != 0) {
									if (!(ansint <= max)) {
										Toast.makeText(context, context.getString(R.string.strmax)
														+ " " + max,
												Toast.LENGTH_LONG).show();
										et.setText("");
										o = "";
									}
								}
							}
							break;
						case 2:

							break;
						case 3:
							if (!o.isEmpty()) {
								SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
								try {
									Date entered = df.parse(o);
									Date mindate = df.parse(minvalue);
									Date maxdate = df.parse(maxvalue);
									Date common = df.parse("0000-00-00");
									Log.v("dates", " MIN DATE " + mindate + " MAX DATE " +
											maxdate + " entered value is " + entered);
									if (entered.compareTo(mindate) < 0) {
										Toast.makeText(context, context.getString(R.string.strmin)
														+ " " + minvalue,
												Toast.LENGTH_LONG).show();
										et.setText("");
										o = "";
										et.clearFocus();
									}
									if (entered.compareTo(common) != 0) {
										if (entered.compareTo(maxdate) > 0) {
											Toast.makeText(context, context.getString(R.string.strmax)

															+ " " + maxvalue,
													Toast.LENGTH_LONG).show();
											et.setText("");
											o = "";
										}
									}
								} catch (ParseException e) {
									e.printStackTrace();
								}
							}
							break;
						case 4:
							break;
					}
					if (!o.isEmpty()) {
						Log.v("onotempty", " oo sio empty value ya o ni " + o);
						ans = ans.concat(o.concat(Constants.SEPARATOR));
						ansid = ansid.concat(oid.concat(Constants.SEPARATOR));
					}
//				Log.v("OTHER QUESTION", otheranswer);
					if (!ans.isEmpty() && !ansid.isEmpty()) {
						final String qn = String.valueOf(rank);
						String prv = String.valueOf(prev);
						String part = String.valueOf(participant);
						Log.v("ANSWER", ans);
						final String[] j = {part, qn, prv, String.valueOf(survey), "comments", ans,
								ansid};
						if (Validating.areSet(j) && validate == 0) {
							int storesponse = Response.updateResponse(context, j);
							if (storesponse != 0) {
								Log.v("updated","the answer is updated");
								final int nextqn = Responsechoice.getNextQn(context, questionid, j[5]);
								System.out.print("\n\n THIS IS SUPPOSED TO BE THE NEXT QUESTION  (" +
										nextqn + ")");

								if (nextqn != 0) {
									Log.w("tayana", " the next qn flag is not 0 but its " + nextqn + "\n");
									int quid = Questions.getQuestionId(context, new int[]{nextqn,
													survey},
											language);//rank,survey
									String nextqntxt = Questions.getQuestionTextLanguage(context,
											new int[]{quid, survey});
									if (Validating.areSet(nextqntxt)) {
										String[] info = {j[1], String.valueOf(nextqn), qn, j[0], flag, String.valueOf(survey), interviewerno};

										//info[0] rank, nextqn, prevqn,survey,participant,
										GeneralViews.getNextQuestion(context, info);
										info = null;
										list.clear();
									}
									partiald.dismiss();
								}
								else {
									Log.w("tayana", " the next qn flag is 0 where we follow a norma flow " +
											"\n");
									int rnk = rank + 1;
									int quid = Questions.getQuestionId(context, new int[]{rnk, survey},
											language);//rank,survey
									String nextqntxt = "";
									if (quid > 0) {
										nextqntxt = Questions.getQuestionTextLanguage(context,
												new int[]{quid, survey});
									}
									if (Validating.areSet(nextqntxt)) {
										String nextq = String.valueOf(rnk);
										Log.w("tayana", " increment rank by 1 to be" + rnk + "\n");
										String[] info = {j[1], nextq, qn, j[0], flag, String.valueOf(survey), interviewerno};
										//info[0] rank, nextqn, prevqn,survey,participant,
										GeneralViews.getNextQuestion(context, info);
										info = null;
										list.clear();
										partiald.dismiss();
									}
									else {
										String _surv = String.valueOf(survey);
										String _part = String.valueOf(participant);
										int end = Participant.insetEndtime(context, new String[]{_surv, interviewerno, _part});
										if (end == 1) {
											Toast.makeText(context, context.getString(R.string.strsurveycomplete), Toast.LENGTH_LONG).show();
											if (partiald.isShowing()) {
												partiald.dismiss();
											}
										}
									}
								}
							}
						} else {
							checkedvalue.clear();
							checkedvalueid.clear();
						}
					}
					else {

					}
				}else {
					Toast.makeText(context, context.getString(R.string.strfillanswer),
							Toast.LENGTH_LONG).show();
				}
			}
		}) ;

		bst.setVisibility(View.VISIBLE);
		int answered = Response.isResponseStored(context, new String[]{info[3],info[0],info[5]});
		TextView txtinfo = new TextView(context);
		TextView txtanswer = new TextView(context);
		txtinfo.setText(context.getString(R.string.stransweredalready));
		if(answered == 1)
		{
			bedit.setEnabled(true);
			layout.addView(txtinfo, width, viewheight);
			for(int i = 0 ; i < listsize; i++)
			{
				cb[i].setEnabled(false);
			}
			String[] ans = Response.getAnswer(context, new String[]{info[3],info[0],info[5]});
			String an = ans[0];
			an = "<b>"+an+"</b>";
			an = an.toUpperCase();
			txtanswer.setTextColor(Color.BLUE);
			if(an.contains("||"))
			{
				txtanswer.setText("\n "+context.getString(R.string.stransweris)+" "+Html.fromHtml(an.replace("||", ",")));
			} else {
				txtanswer.setText("\n "+context.getString(R.string.stransweris)+" "+Html.fromHtml(an));
			}
			layout.addView(txtanswer, width, viewheight);
			bst.setVisibility(View.GONE);
		} else {
			txtanswer.setVisibility(View.GONE);
			bedit.setEnabled(false);
			txtinfo.setVisibility(View.GONE);
		}

		bst.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

				int checkrequired = 0 ;
				int checkminmax = 0;
				for (int i = 0; i < listsize; i++) {
					String q = "";
						if(cb[i].isChecked())
						{
							q = cb[i].getText().toString();
							String qid = "";
							if(i != listsize-1) {
								 qid = listchoice.get(i);
							}
							if(!q.isEmpty() && !qid.isEmpty()){
								checkedvalue.add(q.concat(Constants.SEPARATOR));
								checkedvalueid.add(qid.concat(Constants.SEPARATOR));
							}
						}


						String req = listchoice.get(i);

						int reqq = Responsechoice.isQuestionRequired(context, req);
						if (reqq == 1) {
							if(!cb[i].isChecked()){
							Toast.makeText(context, context.getString(R.string.strreqres) + " " +
											list.get(i),
									Toast.LENGTH_LONG).show();
							checkrequired += 1;

						}
					}
				}

				String ans = "" ;
				String ansid = "";
				if(!checkedvalue.isEmpty()) {
					for (int i = 0; i < checkedvalue.size(); i++) {
						ans += checkedvalue.get(i);
					}
				}
				if(!checkedvalueid.isEmpty()) {
					for (int i = 0; i < checkedvalueid.size(); i++) {
						ansid += checkedvalueid.get(i);
					}
				}
				String oid = listchoice.get(listsize-1);
					String o = et.getText().toString();
					String ansetid = listchoice.get(listchoice.size()-1);
				    Log.v("ansetid", " the anset id is "+ ansetid);
					int datatype_ = Responsechoice.getDataType(context,ansetid);
					String minvalue = Responsechoice.getMin(context, ansetid);
					String maxvalue = Responsechoice.getMax(context, ansetid);
				switch (datatype_) {
					case 1:
						int ansint = Integer.parseInt(o);
						int min = Integer.parseInt(minvalue);
						int max = Integer.parseInt(maxvalue);
						if (!(ansint >= min)) {
							Toast.makeText(context, context.getString(R.string.strmin)
											+ " " + min,
									Toast.LENGTH_LONG).show();
							et.setText("");
							o = "";
							checkminmax +=1 ;
						}
						if (max != 0) {
							if (!(ansint <= max)) {
								Toast.makeText(context, context.getString(R.string.strmax)
												+ " " + max,
										Toast.LENGTH_LONG).show();
								et.setText("");
								o = "";
								checkminmax +=1 ;
							}
						}
						break;
					case 2:

						break;
					case 3:
						SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
						try {
							Date entered = df.parse(o);
							Date mindate = df.parse(minvalue);
							Date maxdate = df.parse(maxvalue);
							Date common = df.parse("0000-00-00");
							Log.v("dates"," MIN DATE "+mindate + " MAX DATE " +
									maxdate + " entered value is "+entered);
							if (entered.compareTo(mindate) < 0) {
								Toast.makeText(context, context.getString(R.string.strmin)
												+ " " + minvalue,
										Toast.LENGTH_LONG).show();
								et.setText("");
								o = "";
								et.clearFocus();
								checkminmax +=1 ;
							}
							if (entered.compareTo(common) != 0) {
								if (entered.compareTo(maxdate) > 0) {
									Toast.makeText(context, context.getString(R.string.strmax)
													+ " " + maxvalue,
											Toast.LENGTH_LONG).show();
									et.setText("");
									o = "";
									checkminmax +=1 ;
								}
							}
						} catch (ParseException e) {
							e.printStackTrace();
						}
						break;
					case 4:
						break;
				}
					if (!o.isEmpty()) {
						Log.v("onotempty", " oo sio empty value ya o ni " + o);
						ans = ans.concat(o.concat(Constants.SEPARATOR));
						ansid = ansid.concat(oid.concat(Constants.SEPARATOR));
					}
//				}
				final String qn = String.valueOf(rank);
				String prv = String.valueOf(prev) ;
				String part = String.valueOf(participant);
				Log.v("ANSWER", ans);
				final String[] j = { part,qn,prv,String.valueOf(survey),"comments",ans,ansid};
				if(Validating.areSet(j) && checkrequired == 0)
				{
					int storesponse = Response.storeResponse(context, j);
					if (storesponse != 0) {
						final int nextqn = Responsechoice.getNextQn(context, questionid,  j[5]);
//							int responseid = Response.getResponseId(context, questionid);

						Log.v("GETNEXT", " QN NO " + rank + " ANSWER " + ans + " responseid " +
								""+storesponse);
						if (nextqn != 0) {
							Log.w("tayana", " the next qn flag is not 0 it is " + nextqn  );
							int quid = Questions.getQuestionId(context,new int[]{nextqn,survey},
									language);
							String nextqntxt = Questions.getQuestionTextLanguage(context,
									new int[]{quid, survey});
							Log.v("storeresponse","store response is "+ storesponse);
							if (Validating.areSet(nextqntxt)) {
								int jumpflag = Response.updateJumpFlag(context,
										new int[]{storesponse,1,nextqn});
								if(jumpflag == 1) {
									String[] info = {j[1], String.valueOf(nextqn), qn, j[0],
											flag, String.valueOf(survey), interviewerno};
									GeneralViews.getNextQuestion(context, info);
									info = null;
									list.clear();

									partiald.dismiss();
								}
							}
							else {
								String _surv = String.valueOf(survey);
								String _part = String.valueOf(participant);
								int end = Participant.insetEndtime(context, new String[]{_surv, interviewerno, _part});
								if (end == 1) {
									Toast.makeText(context, context.getString(R.string.strsurveycomplete), Toast.LENGTH_LONG).show();
									if (partiald.isShowing()) {
										partiald.dismiss();
									}
								}
							}
						}
						else {
							Log.w("tayana", " the next qn flag is 0 where we follow a norma flow " +
									"\n");
							int rnk = rank + 1;
							int quid = Questions.getQuestionId(context,new int[]{rnk,survey},
									language);//rank,survey
							String nextqntxt = "";
							if(quid > 0) {
								nextqntxt = Questions.getQuestionTextLanguage(context,
										new int[]{quid, survey});
							}
							if (Validating.areSet(nextqntxt)) {
								String nextq = String.valueOf(rnk);
								Log.w("tayana", " increment rank by 1 to be" + rnk + "\n");
								String[] info = {j[1], nextq, qn, j[0], flag, String.valueOf(survey), interviewerno};
								//info[0] rank, nextqn, prevqn,survey,participant,
								GeneralViews.getNextQuestion(context, info);
								info = null;
								list.clear();
								partiald.dismiss();
							}
							else {
								String _surv = String.valueOf(survey);
								String _part = String.valueOf(participant);
								int end = Participant.insetEndtime(context, new String[]{_surv, interviewerno, _part});
								if (end == 1) {
									Toast.makeText(context, context.getString(R.string.strsurveycomplete), Toast.LENGTH_LONG).show();
									if (partiald.isShowing()) {
										partiald.dismiss();
									}
								}
							}
						}
					}
				} else{
					checkedvalue.clear();
					checkedvalueid.clear();
				}
			}
		}) ;

		ImageView bspt = (ImageView)partiald.findViewById(R.id.goback);
		bspt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				int pre = Response.getPreviousQn(context, info);
				String prevofpre = String.valueOf(pre);
				Log.v("PREVIOUS", "rank qn is "+ info[2]);
				Log.v("PREVIOUS", "previous qn is "+ prevofpre);
				Log.v("PREVIOUS", "next qn is "+ info[2]);
				Log.v("PREVIOUS", "previous qn before "+ info[2] +" qn is "+ prevofpre);
				String[] data = {info[2],info[2],prevofpre,info[3],info[4],info[5],info[6]} ;
				GeneralViews.getPrevQuestion(context, data, Integer.parseInt(info[2]));
				partiald.dismiss();
			}
		}) ;
		ImageView skip = (ImageView)partiald.findViewById(R.id.btnskip);
		final String[] answeravailable = Response.getAnswer(context, new String[]{info[3],info[0],
				info[5]});
		if(answeravailable[0].isEmpty()){
			skip.setVisibility(View.GONE);
		}
		skip.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (checkupdate[0] == 1) {

					AlertDialogManager alertDialogManager = new AlertDialogManager();
					alertDialogManager.showAlertDialog(context, "ERROR",
							context.getString(R.string.strnoskip), false);
				}
				else {
					int rnk;
					int rank = Integer.parseInt(info[0]);
					rnk = rank + 1;
					String pr = info[2];
					String p = info[3];
					String flag = info[4];
					String s = info[5];
					String inter = info[6];
					int quid = Questions.getQuestionId(context, new int[]{rnk, survey},
							language);//rank,survey
					String nextqntxt = "";
					if (quid > 0) {
						nextqntxt = Questions.getQuestionTextLanguage(context,
								new int[]{quid, survey});
					}
					if (Validating.areSet(nextqntxt)) {
						int responseid = Integer.parseInt(answeravailable[1]);
						Log.w("responseid", " response id is "+responseid);
						int newrank = Response.getJumpedRank(context,
								new int[]{questionid,participant,survey});
						if(newrank == 0){
							Log.v("nojump","this question has no jump skip normally");
							String[] info = {String.valueOf(rnk), String.valueOf(rnk), pr, p, flag, s, inter};


							GeneralViews.getNextQuestion(context, info);
							info = null;
							list.clear();
							partiald.dismiss();
						} else if (newrank > 0){
							Log.v("nojump","this question has jump skip to the jumped rank");
							String[] info = {String.valueOf(newrank), String.valueOf(newrank), pr, p,
									flag, s, inter};


							GeneralViews.getNextQuestion(context, info);
							info = null;
							list.clear();
							partiald.dismiss();
						}
					}
					else {
						String _surv = String.valueOf(survey);
						String _part = String.valueOf(participant);
						int end = Participant.insetEndtime(context, new String[]{_surv, interviewerno, _part});
						if (end == 1) {
							Toast.makeText(context, context.getString(R.string.strsurveycomplete), Toast.LENGTH_LONG).show();
							if (partiald.isShowing()) {
								partiald.dismiss();
							}
						}
					}
				}
			}
		});
		Button masterexit = (Button)partiald.findViewById(R.id.btnexit);
		masterexit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String _surv = String.valueOf(survey);
				String _part = String.valueOf(participant);
				int end = Participant.insetEndtime(context, new String[]{_surv,interviewerno,_part});
				if(end == 1){
					Toast.makeText(context, context.getString(R.string.strsurveyexited),
							Toast.LENGTH_LONG).show();
//					if(partiald.isShowing()){
						partiald.dismiss();

					Intent intent = new Intent(context, Dashboard.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
					context.startActivity(intent);
//					}
				}
			}
		});

		partiald.show();
	}
}