package tz.co.bongolive.apps.prosurvey;

import android.content.ContentResolver;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import java.util.ArrayList;

import survery.database.Survey;
import survery.database.User;

public class Surveys extends Fragment implements OnItemClickListener{
	private static final String ARG_SECTION_NUMBER = "section_number";  
	View _rootView;  
	SurveyAdapter adapter ;
	ListView listview;
	AppPreference appPreference;
	int statusflag ;
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {  
		if(_rootView == null)
		{
			_rootView = inflater.inflate(R.layout.list_items, container, false);
		} else {
			((ViewGroup)_rootView.getParent()).removeView(_rootView) ;
		}
		return _rootView ;
    } 
	
	@Override
    public void onActivityCreated(Bundle savedInstanceState)
    { 
        super.onActivityCreated(savedInstanceState);
        ((Dashboard)getActivity()).onSectionAttached(getArguments().getInt(ARG_SECTION_NUMBER)); 
      listview = (ListView)getActivity().findViewById(R.id.list); 

	  appPreference = new AppPreference(getActivity());
	    statusflag = appPreference.getUserId();
      int useraccount = User.getUserAccount(getActivity(), statusflag);
      adapter = new SurveyAdapter(getActivity(), getList(useraccount), statusflag);
      listview.setAdapter(adapter);
      listview.setOnItemClickListener(this);

}
	public ArrayList<Survey> getList(int acct){
		ContentResolver cr = getActivity().getContentResolver() ;
		int userrole = appPreference.getUserRole();
		String where = "";
		if(userrole == 0) {
			where = Survey.ACCOUNT + " = " + acct + " AND " + Survey.ASSIGNMENT + " = 0";
		} else if (userrole == 1){
			where = Survey.ACCOUNT + " = " + acct ;
		}
        Cursor c = cr.query(Survey.BASEURI, null, where, null,Survey.SURVEYNO+ " ASC");
        ArrayList<Survey> list = new ArrayList<Survey>();
        try{
        if(c.getCount() > 0)
        { 
        	c.moveToFirst() ;
        	do{
        	int title = c.getColumnIndex(Survey.TITLE);
        	int acc = c.getColumnIndex(Survey.ACCOUNT);
        	int sta = c.getColumnIndex(Survey.STATUS);
        	int sn= c.getColumnIndex(Survey.SURVEYNO);
        	String t = c.getString(title);
        	String a = c.getString(acc); 
        	String s = c.getString(sta); 
        	String sno = c.getString(sn); 
        	list.add(new Survey(t, a,s,sno));
        	} while (c.moveToNext()) ; 
        } else {
        	String u = "no data" ;  
        	String d = "" ;
        	list.add(new Survey(u, d, d,d)); 
        } 
        } finally{
        	if(c != null) {
	            c.close();
	        }
        }
        return list ;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
	   Object o = adapter.getItem(position);
	   Survey survey = (Survey)o ;
	   String account = survey.getAcc();
	   String sno = survey.getSno(); 
	}
}
