package tz.co.bongolive.apps.prosurvey;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import networking.prosurvey.Validating;
import survery.database.Survey;
import survery.database.User;

public class UsersAdapter extends BaseAdapter {
private LayoutInflater mInflater ;
private List<User> mItems = new ArrayList<User>() ;
Context _context ;
public UsersAdapter(Context context, List<User> items)
{
	mInflater = LayoutInflater.from(context);
	mItems = items ;
	this._context = context;
}  
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() { 
		return mItems.size() ;
	}
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public Object getItem(int position) { 
		return mItems.get(position);
	}
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int arg0) { 
		return 0;
	}
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) { 
		ItemViewHolder holder ;
		if(convertView == null)
		{
			convertView = mInflater.inflate(R.layout.users, null);
			holder = new ItemViewHolder() ;
			holder.txtnm = (TextView)convertView.findViewById(R.id.txtuser);
			holder.txtloc = (TextView)convertView.findViewById(R.id.txtloc);
			holder.txtsur = (TextView)convertView.findViewById(R.id.txtsurv);
			convertView.setTag(holder);
		} else {
			holder = (ItemViewHolder)convertView.getTag();
		}
		final User cl = mItems.get(position); 
		if(Validating.areSet(cl.getSysid())){
			String prename = _context.getString(R.string.struser);
			String presurv = _context.getString(R.string.strsurvey);
			String preloc = _context.getString(R.string.strlocation);
			String acc = cl.getAccount();
			String sur = String.valueOf(Survey.getActiveSurveyName(_context, acc));
			holder.txtnm.setText(prename + " : "+cl.getName()) ;
			holder.txtloc.setText(preloc + " : "+cl.getLoc()); 
			holder.txtsur.setText(presurv + " : "+sur);
		}  else {
//			to do when no data available
		}
		 
		return convertView ;
	} 
	
	
	static class ItemViewHolder {
		TextView txtnm,txtloc,txtsur;
	}
  
}
