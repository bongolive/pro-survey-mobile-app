/**
 * 
 */
package tz.co.bongolive.apps.prosurvey;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import networking.prosurvey.Validating;
import survery.database.Participant;
import survery.database.Questions;
import survery.database.Responsechoice;
import survery.database.Survey;
import survery.database.User;

/**
 *    @author nasznjoka
 *
 *    
 *    Dec 2, 2014
 */
@SuppressLint("DefaultLocale") public class GeneralViews {
	static int rank = 0 ; 
	static Dialog initiald = null;
	public GeneralViews() {
	}

	public static void initialQuestion(final Context context, final int statusflag, final String account, final String survey) {

		initiald = new Dialog(context, R.style.CustomDialog);
		initiald.setCancelable(false);
		
		LayoutInflater infl = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = infl.inflate(R.layout.questions, null);
		initiald.setContentView(view);

		final boolean checkDeclaration ;
		LinearLayout layout = (LinearLayout)initiald.findViewById(R.id.qnlayout);
		int width = LayoutParams.MATCH_PARENT;
		int height = LayoutParams.MATCH_PARENT ;
		int viewheight = LayoutParams.WRAP_CONTENT ;

		int surveyint = Integer.parseInt(survey);

		String title = Survey.getTitle(context, new String[]{account,survey});
		TextView txttitle = (TextView)initiald.findViewById(R.id.txttitle);
		txttitle.setText(title.toUpperCase());
		final EditText dc = new EditText(context);
		final String dcn = User.getDataCollectorNO(context, statusflag);
		dc.setHint(context.getString(R.string.strdatacollector)+ " " + dcn);
		dc.setInputType(InputType.TYPE_CLASS_NUMBER) ;
		dc.setEnabled(false);
		
		final EditText hf = new EditText(context);
		final String location = User.getFacilityNO(context, statusflag);
		hf.setHint(context.getString(R.string.strhealthfacility)+ " " + location);
		hf.setInputType(InputType.TYPE_CLASS_NUMBER) ;
		hf.setEnabled(false);
		
		final EditText anc = new EditText(context);
		anc.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {

					@Override
					public void onDateSet(DatePicker view, int year, int monthOfYear,
					                      int dayOfMonth) {
						anc.setText(year +" / "+ monthOfYear + " /" + dayOfMonth);
					}
				} ;
				return false;
			}
		});

		anc.setHint(context.getString(R.string.stranc));
		anc.setInputType(InputType.TYPE_CLASS_NUMBER) ;
		anc.requestFocus() ;
		anc.setFilters(new InputFilter[] {new InputFilter.LengthFilter(6)});
		
		final EditText ptc = new EditText(context);
		ptc.setHint(context.getString(R.string.strparticipant));
		ptc.setInputType(InputType.TYPE_CLASS_NUMBER) ;
        ptc.setFilters(new InputFilter[] {new InputFilter.LengthFilter(5)});

		final ArrayList<String> stat = new ArrayList<>();
		stat.add(" 1");
		stat.add(" 2");
		RadioGroup rg = new RadioGroup(context);
		TextView txtstatus = new TextView(context);
		final RadioButton[] status = new RadioButton[2];
		txtstatus.setText(context.getString(R.string.strpmstatus));
		rg.addView(txtstatus, width, viewheight);
//		rg.setOrientation(LinearLayout.HORIZONTAL);
		for(int i = 0; i < stat.size(); i++){
			status[i] = new RadioButton(context);
			status[i].setText(stat.get(i));
			status[i].setId(i);
			rg.addView(status[i], width, viewheight);
		}

		final Button bst = (Button)initiald.findViewById(R.id.goforward);
		final CheckBox cb = new CheckBox(context);

		cb.setText(context.getString(R.string.strdeclaration));

		if(surveyint == 1){
			cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton buttonView, final boolean isChecked) {
					if(isChecked)
					{
						bst.setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View view) {
								String _anc, _status = "", _ptc;
								_anc = anc.getText().toString();
								_ptc = ptc.getText().toString();
								for (int i = 0; i < stat.size(); i++) {
                                    if(status[i].isChecked())
									_status = status[i].getText().toString();
                                    Log.v("status","status clicked is "+ _status);
								}
								if(!_status.isEmpty()){
									String[] strings = {dcn, location, _status, _anc, _ptc};
									if (Validating.areSet(strings)) {
										int partid = Integer.parseInt(strings[4]);
										if(partid > 11000 && partid < 40000) {
											rank = 1;

											String participantid = strings[4];
											int flag = statusflag;
											String[] vals = {survey, strings[1], strings[0], strings[4], strings[2], strings[3],

													String.valueOf(flag)};
											if (Participant.isParticipantActive(context, vals) ==
													1) {
												Toast.makeText(context, context.getString(R.string.strpidinactive), Toast.LENGTH_LONG).show();
											}
											else {
												int j = Participant.storeParticipant(context, vals);
												if (j == 1) {
													String[] info = {"1", "1", "1", participantid, String.valueOf(flag), survey, strings[0]};//String.valueOf(survey)} ;
													System.out.println(info.length);
													//info[0] rank, nextqn, prevqn,participant,
													// flag,survey,account

													getNextQuestion(context, info);
													initiald.dismiss();
												}
												else {
													return;
												}
											}
										}  else {
											AlertDialogManager am = new AlertDialogManager();
											am.showAlertDialog(context,context.getString(R.string
													.strerror), context.getString(R.string
													.strparticipantinfo), false);
										}
									}
								}
							}
						}) ;
					}
				}
			});
		} else {
			bst.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					String _anc, _status = "", _ptc;
					_anc = anc.getText().toString();
					_ptc = ptc.getText().toString();
                    for (int i = 0; i < stat.size(); i++) {
                        if(status[i].isChecked())
                            _status = status[i].getText().toString();
                        Log.v("status","status clicked is "+ _status);
                    }
                    if(!_status.isEmpty()){
						String[] strings = {dcn, location, _status, _anc, _ptc};
						if (Validating.areSet(strings)) {
							rank = 1;

							String participantid = strings[4];
							int flag = statusflag;
							String[] vals = {survey, strings[1], strings[0], strings[4], strings[2], strings[3],
									String.valueOf(flag)};
							if (Participant.isParticipantActive(context, vals) == 1) {
								Toast.makeText(context, context.getString(R.string.strpidinactive), Toast.LENGTH_LONG).show();
							}
							else {
								int j = Participant.storeParticipant(context, vals);
								if (j == 1) {
									String[] info = {"1", "1", "1", participantid, String.valueOf(flag), survey, strings[0]};//String.valueOf(survey)} ;
									System.out.println(info.length);
									//info[0] rank, nextqn, prevqn,participant,flag,survey,account

									getNextQuestion(context, info);
									initiald.dismiss();
								}
								else {
									return;
								}
							}
						}
					}
				}
			}) ;
		}



        ImageView bspt = (ImageView)initiald.findViewById(R.id.goback); 
        bspt.setOnClickListener(new OnClickListener() {

	        @Override
	        public void onClick(View arg0) {
		        initiald.dismiss();
	        }
        }) ;
		layout.addView(dc, width, viewheight);
		layout.addView(hf, width, viewheight);
		layout.addView(anc, width, viewheight); 
		layout.addView(ptc, width, viewheight); 
		layout.addView(rg, width, viewheight);
		if(surveyint ==1) {
			layout.addView(cb, width, viewheight);
		}

		Button masterexit = (Button)initiald.findViewById(R.id.btnexit);
		masterexit.setVisibility(View.GONE);

		ImageView skip = (ImageView)initiald.findViewById(R.id.btnskip);
	    Button bedit = (Button)initiald.findViewById(R.id.btnedit); 
	    skip.setVisibility(View.GONE);
	    bedit.setVisibility(View.GONE);
		
		initiald.show();
	}

	  
	public static void getNextQuestion(Context context, String[] data) 
	{
		 final String[] type = new String[]{"Open_ended","Closed_ended","Partial_open_ended",
				 "Closed_ended_mult","Partial_open_ended_mult"};
		 int checktype = 0;
		 int rank = Integer.parseInt(data[0]) ;
		 int next = Integer.parseInt(data[1]) ;/*next is rank not id*/
		 int prev = Integer.parseInt(data[2]); /*previous should also be rank not question id*/
			Log.w("Tag", " the rank is" + rank + " next qn is " + next + " survey is "+ data[5]) ;
		 String types = Questions.getQuestionType(context, next, data[5]);
			Log.w("Tag", " next qn is type is " + types) ;
		if(types.equals(type[0]))
		  {checktype = 1;} else if(types.equals(type[1])) {checktype = 2;} 
		  else if(types.equals(type[2])) {checktype = 3 ;}
		  else if(types.equals(type[3])){ checktype = 4 ;} else if(types.equals(type[4])){checktype = 5;};
		  
		switch(checktype)
		{
		case 1:
			String[] newdata = {data[1],data[1],data[2],data[3],data[4],data[5],data[6]};
				Log.v("question type"," question type equals");
				OpenEnded.open(context,newdata);
			Log.w("Tag", " opening question no "+ next) ;
			newdata = null ;
			break;
		case 2: 
			String[] newdataclosed = {data[1],data[1],data[2],data[3],data[4],data[5],data[6]};
				CloseEnded.mcq(context, newdataclosed);
			Log.w("Tag", " opening question no "+ next) ;
			newdataclosed = null ;
			break;
		case 3:
			String[] infod =  {data[1],data[1],data[2],data[3],data[4],data[5],data[6]};
				PartialOpenEnded.mcqother(context, infod);
			Log.w("Tag", " opening question no "+ next) ;
			infod = null ;
			break;
		case 4:
			String[] infof ={data[1],data[1],data[2],data[3],data[4],data[5],data[6]};
			
				CloseEndedMultiple.multiselect(context, infof); ; 
			Log.w("Tag", " opening question no "+ next) ;
			infof = null ;
			break; 
		case 5:
			String[] infog ={data[1],data[1],data[2],data[3],data[4],data[5],data[6]};  
				PartialOpenEndedMulti.multiselectother(context, infog); 
			Log.w("Tag", " opening question no "+ next) ;
			infog = null ;
			break; 
		}
		
	}
	public static void getPrevQuestion(Context context, String[] data, int currentrank)
	{
		final String[] type = new String[]{"Open_ended","Closed_ended","Partial_open_ended",
				"Closed_ended_mult","Partial_open_ended_mult"};
		 int checktype = 0;
		 int rank = Integer.parseInt(data[0]) ;
		 int next = Integer.parseInt(data[1]) ;
		 int prev = Integer.parseInt(data[2]);

		 Log.v("testing", " rank is "+ rank + " nextqn is "+next+" prev qn is "+prev + " survey " +
						 "is "+ data[5]
		 ) ;

		String types = Questions.getQuestionType(context, next, data[5]);
			Log.w("Tag", " prev qn is type is " + types) ;
		if(types.equals(type[0]))
		  {checktype = 1;} else if(types.equals(type[1])) {checktype = 2;} 
		  else if(types.equals(type[2])) {checktype = 3 ;}
		  else if(types.equals(type[3])){ checktype = 4 ;} else if(types.equals(type[4])){checktype = 5;};
		  
		  switch(checktype)
			{
			case 1:
				String[] newdata = {data[0],data[1],data[2],data[3],data[4],data[5],data[6]};
					OpenEnded.open(context, newdata) ;
				Log.w("Tag", " opening question no "+ next) ;
				newdata = null ;
				break;
			case 2: 
				String[] newdataclosed = {data[0],data[1],data[2],data[3],data[4],data[5],data[6]};
					CloseEnded.mcq( context, newdataclosed) ;
				Log.w("Tag", " opening question no "+ next) ;
				newdataclosed = null ;
				break;
			case 3:
				String[] infod = {data[0],data[1],data[2],data[3],data[4],data[5],data[6]};
				PartialOpenEnded.mcqother(context, infod) ;
				Log.w("Tag", " opening question no "+ next) ;
				infod = null ;
				break;
			case 4:
				String[] infof = {data[0],data[1],data[2],data[3],data[4],data[5],data[6]};
				CloseEndedMultiple.multiselect(context, infof) ;
				Log.w("Tag", " opening question no "+ next) ;
				infof = null ;
				break; 
			case 5:
				String[] infog = {data[0],data[1],data[2],data[3],data[4],data[5],data[6]};
					PartialOpenEndedMulti.multiselectother(context, infog) ;
				Log.w("Tag", " opening question no "+ next) ;
				infog = null ;
				break;
			}

	}

	public static void setDataType(Context context, String choiceId, EditText editText){
		int datatype = Responsechoice.getDataType(context, choiceId) ;
		switch(datatype)
		{
			case 1:
				//integer
				editText.setInputType(InputType.TYPE_CLASS_NUMBER);
//				String min = Responsechoice.getMin(context, choiceId);
				Log.w("INPUTTYPE"," TYPE IS NUMBER ONLY");
				break;
			case 2:
				editText.setInputType(InputType.TYPE_CLASS_TEXT);
				Log.w("INPUTTYPE"," TYPE IS ALPHANUMERIC ONLY");
				//varchar
				break;
			case 3:
				editText.setInputType(InputType.TYPE_DATETIME_VARIATION_DATE);
				Log.w("INPUTTYPE"," TYPE IS DATE ONLY");
				//date
				break;
			case 4:
				editText.setInputType(InputType.TYPE_DATETIME_VARIATION_TIME);
				//time
				break;
		}
	}

	public static void setDateTimeField(Context context, final EditText fromDateEtxt) {

		Calendar newCalendar = Calendar.getInstance();
		final SimpleDateFormat dateFormatter;
		dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
		DatePickerDialog fromDatePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {

			public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
				Calendar newDate = Calendar.getInstance();
				newDate.set(year, monthOfYear, dayOfMonth);
				fromDateEtxt.setText(dateFormatter.format(newDate.getTime()));

			}

		}, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
		fromDatePickerDialog.show();
	}
	public static void checkMinMax(Context context, int datatype, String anset, EditText et,
	                               String minvalue,
	                               String maxvalue){
		switch (datatype) {
			case 1:
				int ansint = Integer.parseInt(anset);
				int min = Integer.parseInt(minvalue);
				int max = Integer.parseInt(maxvalue);
				if (!(ansint >= min)) {
					Toast.makeText(context, context.getString(R.string.strmin)
									+ " " + min,
							Toast.LENGTH_LONG).show();
					et.setText("");
					anset = "";
				}
				if (max != 0) {
					if (!(ansint <= max)) {
						Toast.makeText(context, context.getString(R.string.strmax)
										+ " " + max,
								Toast.LENGTH_LONG).show();
						et.setText("");
						anset = "";
					}
				}
				break;
			case 2:

				break;
			case 3:
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				try {
					Date entered = df.parse(anset);
					Date mindate = df.parse(minvalue);
					Date maxdate = df.parse(maxvalue);
					Date common = df.parse("0000-00-00");
					Log.v("dates"," MIN DATE "+mindate + " MAX DATE " +
							maxdate + " entered value is "+entered);
					if (entered.compareTo(mindate) < 0) {
						Toast.makeText(context, context.getString(R.string.strmin)
										+ " " + minvalue,
								Toast.LENGTH_LONG).show();
						et.setText("");
						anset = "";
						et.clearFocus();
					}
					if (entered.compareTo(common) != 0) {
						if (entered.compareTo(maxdate) > 0) {
							Toast.makeText(context, context.getString(R.string.strmax)
											+ " " + maxvalue,
									Toast.LENGTH_LONG).show();
							et.setText("");
							anset = "";
						}
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
				break;
			case 4:
				break;
		}
	}

//	public static void delete
}