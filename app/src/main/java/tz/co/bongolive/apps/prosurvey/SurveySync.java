package tz.co.bongolive.apps.prosurvey;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import networking.prosurvey.Constants;
import networking.prosurvey.JsonObjectJsonArray;
import networking.prosurvey.Validating;
import survery.database.Questions;
import survery.database.Responsechoice;
import survery.database.Settings;
import survery.database.Survey;
import survery.database.User;

/**
 * Created by nasznjoka on 5/18/15.
 */
public class SurveySync extends AsyncTask<String, Integer, Integer> {
    private static final String TAG = SurveySync.class.getName();
    Context context;
    ProgressBar bar;
    TextView txt;

    public SurveySync(Context context, ProgressBar pbar,TextView textView){
        this.context = context;
        this.bar = pbar;
        this.txt = textView;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        if (this.bar != null) {
            bar.setProgress(values[0]);
            txt.setText("saving surveys");
            bar.setMax(values[1]);
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        bar.setIndeterminate(false);
        txt.setText("downloading survey data");

       /* pDialog.setMessage("Please wait ...");
        pDialog.setCancelable(true);
        pDialog.show();*/
    }


    @Override
    protected Integer doInBackground(String... params) {
        JSONObject json = null;
        try {
            json = json = Survey.getSurvey(context);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        int[] ret = {0};
        JSONArray userArray ;
        AppPreference appPreference = new AppPreference(context);
        Mail mail = new Mail();

        if(json != null)
            try{
                if(json.has(Survey.ARRAYNAME)) {
                    userArray = json.getJSONArray(Survey.ARRAYNAME);

                    if (!TextUtils.isEmpty(json.getString(Constants.SUCCESS))) {
                        String res = json.getString(Constants.SUCCESS);
                        if (Integer.parseInt(res) == 1) {
                            Log.v("NACHEKIDATA", " je data zinakuja?");
                            int success = 0;
                            for (int i = 0; i < userArray.length(); i++) {
                                JSONObject js = userArray.getJSONObject(i);
                                String sv = js.getString(Survey.SURVEYNO);
                                String ti = js.getString(Survey.TIMEINITIAL);
                                String tf = js.getString(Survey.TIMEFINAL);
                                String tt = js.getString(Survey.TITLE);
                                String cp = js.getString(Survey.CAP);
                                String qn = js.getString(Survey.QUESTIONS);
                                String st = js.getString(Survey.STATUS);
                                String ky = js.getString(Survey.KEYWORD);
                                String ds = js.getString(Survey.DESC);
                                String ac = js.getString(Survey.ACCOUNT);
                                String pm = js.getString(Survey.ASSIGNMENT);
                                String[] vals = new String[]{sv, ti, tf, tt, cp, qn, st, ky, ds, ac, pm};
                                if(Validating.areSet(vals)) {
                                    if (Survey.isSurveyPresent(context, vals[0]) < 1) {
                                        if (Survey.storeCredentials(context, vals) == 1) {
                                            success += 1;
                                            publishProgress(i,userArray.length());
                                        }
                                    } else {
                                        success += 1;
                                        publishProgress(i,userArray.length());
                                    }
                                } else {
                                    String subject = Constants.getIMEINO(context)+" null survey";
                                    String body = " survey no "+sv+"  has null value " +
                                            " today at "+Constants.getDate();
                                    String[] msg = {subject,body};
                                    try {
                                        mail.sendEmail(msg);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    publishProgress(i,userArray.length());
                                }
                            }
                            if (success == userArray.length()) {
//                                ack(context,0);
                                appPreference.saveSyncStatus(Constants.SURVEYSYNC);
                                ret[0] = 1;
                            }
                        }
                    }
                } else if (json.has(Survey.UPDATEARRAYNAME)) {

                    JSONArray jsonArray = null;
                    jsonArray = json.getJSONArray(Survey.UPDATEARRAYNAME);

                    for (int j = 0; j < jsonArray.length(); j++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(j);
                        if (Survey.isSurveyPresent(context, jsonObject.getString(Survey.SURVEYNO)) == 1) {
                            Log.v("survey is present", " up to me now to update it");
                            String sv = jsonObject.getString(Survey.SURVEYNO);
                            String tf = jsonObject.getString(Survey.TITLE);
                            String tt = jsonObject.getString(Survey.STATUS);
                            String pm = jsonObject.getString(Survey.ASSIGNMENT);
                            String[] vals = new String[]{sv, tf, tt, pm};

                            if (Validating.areSet(vals)){
                                if (Survey.updateSurvey(context, vals) == 1) {
//                            ack(context, 1);
                                    appPreference.saveSyncStatus(Constants.SURVEYSYNC);
                                    ret[0]= 1;
                                    publishProgress(j,jsonArray.length());
                                } else {
                                    Log.v("NACK", "zimeaillll");
                                }
                            } else {
                                //send mail here
                                String subject = Constants.getIMEINO(context)+" null survey";
                                String body = " survey no "+sv+"  has null value " +
                                        " today at "+Constants.getDate();
                                String[] msg = {subject,body};
                                try {
                                    mail.sendEmail(msg);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                publishProgress(j,jsonArray.length());
                            }
                        }  else {
                            String sv = jsonObject.getString(Survey.SURVEYNO);
                            String ti = jsonObject.getString(Survey.TIMEINITIAL);
                            String tf = jsonObject.getString(Survey.TIMEFINAL);
                            String tt = jsonObject.getString(Survey.TITLE);
                            String cp = jsonObject.getString(Survey.CAP);
                            String qn = jsonObject.getString(Survey.QUESTIONS);
                            String st = jsonObject.getString(Survey.STATUS);
                            String ky = jsonObject.getString(Survey.KEYWORD);
                            String ds = jsonObject.getString(Survey.DESC);
                            String ac = jsonObject.getString(Survey.ACCOUNT);
                            String pm = jsonObject.getString(Survey.ASSIGNMENT);
                            String[] vals = new String[]{sv, ti, tf, tt, cp, qn, st, ky, ds, ac, pm};
                            if (Validating.areSet(vals)) {
                                if (Survey.isSurveyPresent(context, vals[0]) < 1) {
                                    if (Survey.storeCredentials(context, vals) == 1) {
//                                ack(context,1);
                                        appPreference.saveSyncStatus(Constants.SURVEYSYNC);
                                        ret[0]= 1;
                                        publishProgress(j,jsonArray.length());
                                    }
                                } else {
//                            ack(context,1);
                                    appPreference.saveSyncStatus(Constants.SURVEYSYNC);
                                    ret[0]= 1;
                                    publishProgress(j,jsonArray.length());
                                }
                            } else {
                                //send mail here
                                String subject = Constants.getIMEINO(context)+" null survey";
                                String body = " survey no "+sv+"  has null value " +
                                        " today at "+Constants.getDate();
                                String[] msg = {subject,body};
                                try {
                                    mail.sendEmail(msg);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                publishProgress(j,jsonArray.length());
                            }
                        }
                    }
                }
            } catch (JSONException e){
                e.printStackTrace();
            }
        return ret[0] ;
    }

    protected void onPostExecute(final Integer ret) {
        super.onPostExecute(ret);
        AppPreference appPreference = new AppPreference(context);
        Log.v("returned", "returned is "+ret);

        if(appPreference.getSyS() == 1){
            if(ret == 1){
                bar.setProgress(100);
                new ProcessAck(context).execute(new String[]{"surveys"});
                bar.setIndeterminate(true);
            } else {
                bar.setProgress(100);
            }
                new QuestionsSync(context,bar,txt).execute();

        } else {

            bar.setProgress(100);
            new QuestionsSync(context,bar,txt).execute();
        }
      /*
        if(ap.getSyncStats() > 4*//*User.getUserCount(context) > 0 && Survey.getCount(context) > 0 && Settings.getSettingsCount(context) > 0 &&
                Questions.getCount(context) > 50 && Responsechoice.getCount(context) > 150*//*){

            Toast.makeText(context," survey sync is complete you can login now ",Toast.LENGTH_SHORT).show();
            bar.setProgress(100);
        }*/
    }
}
