package tz.co.bongolive.apps.prosurvey;


import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import networking.prosurvey.Constants;
import networking.prosurvey.Validating;
import survery.database.*;
import survery.database.Settings;

/**
 * Created by nasznjoka on 5/18/15.
 */
public class SettingsSync extends AsyncTask<String, Integer, Integer> {
    private static final String TAG = SettingsSync.class.getName();
    Context context;
    ProgressBar bar;
    TextView txt;

    public SettingsSync(Context context, ProgressBar pbar,TextView textView){
        this.context = context;
        this.bar = pbar;
        this.txt = textView;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        if (this.bar != null) {
            bar.setProgress(values[0]);
            txt.setText("saving settings");
            bar.setMax(values[1]);
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        bar.setIndeterminate(false);
        txt.setText("downloading settings information");

       /* pDialog.setMessage("Please wait ...");
        pDialog.setCancelable(true);
        pDialog.show();*/
    }


    @Override
    protected Integer doInBackground(String... params) {
        int[] ret = {0};
        AppPreference appPreference = new AppPreference(context);
        JSONObject json = null;
        try {
            json = survery.database.Settings.getSettingsJson(context);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Mail mail = new Mail();
        try {
            if(json.has(Constants.SUCCESS)) {
                String res = json.getString(Constants.SUCCESS);
                if (Integer.parseInt(res) == 1) {
                    if (json.has(Settings.ARRAYNAME)) {
                        JSONObject js = json.getJSONObject(Settings.ARRAYNAME);
                        String sv = js.getString(Settings.SYSTEMID);
                        String ti = js.getString(Settings.ACCOUNT);
                        String tf = js.getString(Settings.ACTIVESURVEY);
                        String tt = js.getString(Settings.SKIPVALUE);
                        String cp = js.getString(Settings.LANGUAGE);
                        String qn = js.getString(Settings.SMSREPORT);
                        String sr = js.getString(Settings.SERVER);
                        String[] vals = {sv, ti, tf, tt, cp, qn, sr};
                        if (Validating.areSet(vals)) {
                            if (Settings.isSettingsExist(context, vals[0]) < 1) {
                                if (Settings.insertSettings(context, vals) == 1) {
//                                    ack(context, 0);
                                    ret[0] = 1;
                                    publishProgress(80,100);
                                    appPreference.saveSyncStatus(Constants.SETTINGSSYNC);
                                }
                            } else {
//                                ack(context, 0);
                                ret[0] = 1;
                                publishProgress(80,100);
                                appPreference.saveSyncStatus(Constants.SETTINGSSYNC);
                            }
                        } else {
                            //send mail here
                            String subject = Constants.getIMEINO(context) + " null settings";
                            String body = " Settings no " + sv + "  has null value " +
                                    " today at " + Constants.getDate();
                            String[] msg = {subject, body};
                            try {
                                mail.sendEmail(msg);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            publishProgress(80,100);
                        }
                    } else if (json.has(Settings.UPDATEARRAYNAME)) {
                        JSONObject js = json.getJSONObject(Settings.UPDATEARRAYNAME);
                        if (Settings.isSettingsExist(context, js.getString(Settings.SYSTEMID)) == 1) {
                            String sv = js.getString(Settings.SYSTEMID);
                            String tf = js.getString(Settings.ACTIVESURVEY);
                            String cp = js.getString(Settings.LANGUAGE);
                            String tt = js.getString(Settings.SKIPVALUE);
                            String qn = js.getString(Settings.SMSREPORT);
                            String sr = js.getString(Settings.SERVER);
                            String[] vals = {sv, tf, cp, tt, qn, sr};
                            if (Validating.areSet(vals)) {
                                if (Settings.updateSettings(context, vals) == 1) {
//                                    ack(context, 1);
                                    ret[0] = 1;
                                    publishProgress(80,100);
                                    appPreference.saveSyncStatus(Constants.SETTINGSSYNC);
                                }
                            } else {
                                //send mail here
                                String subject = Constants.getIMEINO(context) + " null settings";
                                String body = " Settings no " + sv + "  has null value " +
                                        " today at " + Constants.getDate();
                                String[] msg = {subject, body};
                                try {
                                    mail.sendEmail(msg);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                publishProgress(80,100);
                            }
                        } else {
                            JSONObject jss = json.getJSONObject(Settings.UPDATEARRAYNAME);
                            String sv = jss.getString(Settings.SYSTEMID);
                            String ti = jss.getString(Settings.ACCOUNT);
                            String tf = jss.getString(Settings.ACTIVESURVEY);
                            String tt = jss.getString(Settings.SKIPVALUE);
                            String cp = jss.getString(Settings.LANGUAGE);
                            String qn = jss.getString(Settings.SMSREPORT);
                            String sr = jss.getString(Settings.SERVER);
                            String[] vals = {sv, ti, tf, tt, cp, qn, sr};
                            if (Validating.areSet(vals)) {
                                if (Settings.insertSettings(context, vals) == 1) {
//                                    ack(context, 1);
                                    ret[0] = 1;
                                    publishProgress(80,100);
                                    appPreference.saveSyncStatus(Constants.SETTINGSSYNC);
                                } else {
//                                    ack(context, 1);
                                    ret[0] = 1;
                                    publishProgress(80,100);
//                                    publishProgress(ui,jsonArray.length());
                                    appPreference.saveSyncStatus(Constants.SETTINGSSYNC);
                                }
                            } else {
                                //send mail here
                                String subject = Constants.getIMEINO(context) + " null settings";
                                String body = " Settings no " + sv + "  has null value " +
                                        " today at " + Constants.getDate();
                                String[] msg = {subject, body};
                                try {
                                    mail.sendEmail(msg);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                publishProgress(80,100);
                            }
                        }
                    }
                }
            }
        } catch (JSONException e)
        {
            e.printStackTrace();
        }
        return ret[0] ;
    }

    protected void onPostExecute(final Integer ret) {
        super.onPostExecute(ret);
        AppPreference appPreference = new AppPreference(context);
        Log.v("returned", "returned is " + ret);
        Log.v("syncst"," "+appPreference.getSySt());
        if(appPreference.getSySt() == 5){
            if(ret == 1){               bar.setProgress(100);
                txt.setText(context.getString(R.string.strsyncompt));
                new ProcessAck(context).execute(new String[]{"settings"});
                bar.setVisibility(View.GONE);
            }else {
                bar.setProgress(100);
                txt.setText(context.getString(R.string.strsyncompt));
                bar.setVisibility(View.GONE);
            }
        } else {
            if(ret == 1){
                new ProcessAck(context).execute(new String[]{"settings"});
            }
            txt.setText(context.getString(R.string.strsynincomplete));
            if(appPreference.getSyS() != 1)
                txt.append("  \nSurvey is not synced");
            if(appPreference.getSyQ() != 2)
                txt.append("  \nQuestions are not synced ");
            if(appPreference.getSyO() != 3)
                txt.append("  \nQuestion options are not synced ");
            if(appPreference.getSyU() != 4)
                txt.append("  \nUsers are not synced ");
            bar.setProgress(0);
            bar.setVisibility(View.GONE);
        }
      /*
        if(ap.getSyncStats() > 4*//*User.getUserCount(context) > 0 && Survey.getCount(context) > 0 && Settings.getSettingsCount(context) > 0 &&
                Questions.getCount(context) > 50 && Responsechoice.getCount(context) > 150*//*){

            Toast.makeText(context," survey sync is complete you can login now ",Toast.LENGTH_SHORT).show();
            bar.setProgress(100);
        }*/
    }
}
