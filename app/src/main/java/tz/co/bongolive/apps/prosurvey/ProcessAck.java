package tz.co.bongolive.apps.prosurvey;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import networking.prosurvey.Constants;
import networking.prosurvey.JsonObjectJsonArray;
import survery.database.Questions;
import survery.database.Responsechoice;
import survery.database.Settings;
import survery.database.Survey;
import survery.database.User;

/**
 * Created by nasznjoka on 5/18/15.
 */
public class ProcessAck extends AsyncTask<String, Integer, JSONObject> {
    private static final String TAG = ProcessAck.class.getName();

    Context context ;
    public ProcessAck(Context _con){
        context = _con;
    }
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }


    @Override
    protected JSONObject doInBackground(String... params) {
        JSONObject jobs = new JSONObject();

        try {
            jobs.put("tag", Constants.TAGACK);
            jobs.put(Constants.IMEI, Constants.getIMEINO(context));
            jobs.put("item",params[0]);
            JsonObjectJsonArray js = new JsonObjectJsonArray();
            String job =  js.getJsonArray(Settings.getServerName(context), JsonObjectJsonArray.POST, jobs);
            Log.v("server", Settings.getServerName(context) + " server");
            JSONObject json;
        try {
            json = new JSONObject(job);
            if(json.length() != 0)
            {
                return json ;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null ;
    }

    protected void onPostExecute(final JSONObject json) {
        super.onPostExecute(json);

        if(json != null){
            Log.v("ack","ack is a success " + json.toString());

        }
    }
}
