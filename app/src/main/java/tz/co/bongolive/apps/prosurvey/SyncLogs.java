package tz.co.bongolive.apps.prosurvey;

import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import networking.prosurvey.Constants;
import networking.prosurvey.Validating;
import survery.database.LogsTable;
import survery.database.Participant;
import survery.database.Questions;
import survery.database.Response;
import survery.database.Survey;
import survery.database.User;

public class SyncLogs extends Fragment {
	private static final String ARG_SECTION_NUMBER = "section_number";
	View _rootView;
	LogsAdapter adapter ;
	ListView listview;
	AppPreference appPreference;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		if(_rootView == null)
		{
			_rootView = inflater.inflate(R.layout.list_items, container, false);
		} else {
			((ViewGroup)_rootView.getParent()).removeView(_rootView) ;
		}
		return _rootView ;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		((Dashboard)getActivity()).onSectionAttached(getArguments().getInt(ARG_SECTION_NUMBER));
		listview = (ListView)getActivity().findViewById(R.id.list);


		adapter = new LogsAdapter(getActivity(), getListLogs());
		listview.setAdapter(adapter);

	}

	public ArrayList<LogsTable> getListLogs(){
		ContentResolver cr = getActivity().getContentResolver() ;
		Cursor c = cr.query(LogsTable.BASEURI, null, null, null, LogsTable.ID+ " DESC");
		ArrayList<LogsTable> list = new ArrayList<LogsTable>();
		try{
			if(c.getCount() > 0)
			{
				c.moveToFirst() ;
				do{
					int time = c.getColumnIndex(LogsTable.WHEN);
					int cause = c.getColumnIndex(LogsTable.SOURCE);
					int info = c.getColumnIndex(LogsTable.ISSUE);
					String _ti = c.getString(time);
					String _cau = c.getString(cause);
					String _inf = c.getString(info);
					list.add(new LogsTable(_ti,_cau,_inf));
				} while (c.moveToNext()) ;
			} else {
				String u = "no data" ;
				String d = "" ;
				list.add(new LogsTable(u,d,d));
			}
		} finally{
			if(c != null) {
				c.close();
			}
		}
		return list ;
	}
	public class LogsAdapter extends BaseAdapter {
		private LayoutInflater mInflater ;
		private List<LogsTable> mItems = new ArrayList<LogsTable>() ;
		Context _context ;
		int userid ;
		public LogsAdapter(Context context, List<LogsTable> items)
		{
			mInflater = LayoutInflater.from(context);
			mItems = items ;
			this._context = context;
		}
		/* (non-Javadoc)
         * @see android.widget.Adapter#getCount()
         */
		@Override
		public int getCount() {
			return mItems.size() ;
		}
		/* (non-Javadoc)
         * @see android.widget.Adapter#getItem(int)
         */
		@Override
		public Object getItem(int position) {
			return mItems.get(position);
		}
		/* (non-Javadoc)
         * @see android.widget.Adapter#getItemId(int)
         */
		@Override
		public long getItemId(int arg0) {
			return 0;
		}

		/* (non-Javadoc)
         * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
         */
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ItemViewHolder holder ;

			final AppPreference appPreference = new AppPreference(_context.getApplicationContext());
			if(convertView == null)
			{
				convertView = mInflater.inflate(R.layout.logsitems
						, null);
				holder = new ItemViewHolder() ;
				holder.txtqn = (TextView)convertView.findViewById(R.id.txtquestion);
				holder.txtres = (TextView)convertView.findViewById(R.id.txtresponse);
				holder.txtqntext = (TextView)convertView.findViewById(R.id.txtqntext);

				holder.txtqn.setText(LogsTable.getTime());
				holder.txtres.setText(LogsTable.getCause());
				holder.txtqntext.setText(LogsTable.getTime());

				convertView.setTag(holder);
			} else {
				holder = (ItemViewHolder)convertView.getTag();
			}
			return convertView ;
		}


		 class ItemViewHolder {
			TextView txtqn,txtqntext, txtres ;
		}

	}

}
