package tz.co.bongolive.apps.prosurvey;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import networking.prosurvey.Validating;
import survery.database.Questions;
import survery.database.Response;
import survery.database.Survey;
import survery.database.User;
 
public class SurveyAdapter extends BaseAdapter {
private LayoutInflater mInflater ;
private List<Survey> mItems = new ArrayList<Survey>() ;
Context _context ;
int userid ;
public SurveyAdapter(Context context, List<Survey> items, int user)
{
	mInflater = LayoutInflater.from(context);
	mItems = items ;
	this._context = context;
	this.userid = user ;
}  
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() { 
		return mItems.size() ;
	}
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public Object getItem(int position) { 
		return mItems.get(position);
	}
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int arg0) { 
		return 0;
	}
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) { 
		ItemViewHolder holder ;
		if(convertView == null)
		{
			convertView = mInflater.inflate(R.layout.survey_items, null); 
			holder = new ItemViewHolder() ;
			holder.txttitle = (TextView)convertView.findViewById(R.id.txttitle);
			holder.start = (Button)convertView.findViewById(R.id.btnstart); 
			holder.peruse = (Button)convertView.findViewById(R.id.btnperuse); 
			convertView.setTag(holder);
		} else {
			holder = (ItemViewHolder)convertView.getTag();
		}
		final Survey cl = mItems.get(position); 
		if(Validating.areSet(cl.getAcc())){ 
			holder.txttitle.setText(cl.getTt()) ;
		} else {

			convertView = mInflater.inflate(R.layout.general_layout, null); 
			holder = new ItemViewHolder();
			convertView.setTag(holder); 
		}
		final AppPreference appPreference = new AppPreference(_context);
		holder.start.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {

				String language = appPreference.getDefaultLanguage();
				int questionid = Questions.getQuestionId(_context, new int[]{1,
						Integer.parseInt(cl.getSno())},language);
				String questiontext = Questions.getQuestionTextLanguage(_context,
						new int[]{questionid,Integer.parseInt(cl.getSno())});
				int activestatus = Survey.getActiveSurvey(_context, cl.getAcc());
				int survno = Integer.parseInt(cl.getSno());

				 if(appPreference.getUserRole() == 1 && appPreference.getAdminStatus(_context) ==
						 1|| appPreference.getUserRole() == 0) {
					 if (Validating.areSet(questiontext)) {
						 GeneralViews.initialQuestion(_context, userid, cl.getAcc(), cl.getSno());
//						 Log.v("survey","survey no is "+cl.getSno());
					 }
					 else {
						 Toast.makeText(_context, _context.getString( R.string.strnoquestions),
								 Toast.LENGTH_LONG).show();

					 }
				 } else {
					 Toast.makeText(_context, _context.getString( R.string.strackquireuser),
							 Toast.LENGTH_LONG).show();
				 }
			}
		}) ;
		
        holder.peruse.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(appPreference.getUserRole() == 1 && appPreference.getAdminStatus(_context) ==
						1 || appPreference.getUserRole() == 0) {
					Intent intent = new Intent(_context, Peruse.class);
					intent.putExtra(Survey.ACCOUNT, cl.getAcc());
					intent.putExtra(Survey.SURVEYNO, cl.getSno());
					intent.putExtra(User.SYSTEMUSERID, String.valueOf(userid));
					int response = 0;
					response = Response.getNoResponse(_context, cl.getSno());
					if (response > 0) {
						_context.startActivity(intent);
					}
					else {
						Toast.makeText(_context, _context.getString(R.string.stremptyresponsesurvey)


								, Toast.LENGTH_LONG).show();
					}
				} else {
					Toast.makeText(_context, _context.getString( R.string.strackquireuser),
							Toast.LENGTH_LONG).show();
				}
			}
		}) ;
		return convertView ;
	} 
	
	
	static class ItemViewHolder {
		TextView txttitle ;
		Button start ; 
		Button peruse ; 
	}
  
}
