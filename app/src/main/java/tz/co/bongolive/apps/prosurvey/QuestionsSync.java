package tz.co.bongolive.apps.prosurvey;

import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import networking.prosurvey.Constants;
import networking.prosurvey.Validating;
import survery.database.LogsTable;
import survery.database.Questions;
import survery.database.Survey;

/**
 * Created by nasznjoka on 5/18/15.
 */
public class QuestionsSync extends AsyncTask<String, Integer, Integer> {
    private static final String TAG = QuestionsSync.class.getName();
    Context context;
    ProgressBar bar;

    TextView txt;

    public QuestionsSync(Context context, ProgressBar pbar,TextView textView){
        this.context = context;
        this.bar = pbar;
        this.txt = textView;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        if (this.bar != null) {
            bar.setProgress(values[0]);
            txt.setText("saving questions");
            bar.setMax(values[1]);
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        bar.setIndeterminate(false);
        txt.setText("downloading questions");

       /* pDialog.setMessage("Please wait ...");
        pDialog.setCancelable(true);
        pDialog.show();*/
    }


    @Override
    protected Integer doInBackground(String... params) {
        JSONArray userArray;
        int[] ret = {0};
        JSONObject json = null;
        try {
            json = Questions.getQuestions(context);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Mail mail = new Mail();
        AppPreference appPreference = new AppPreference(context);
        try {
            if (json.has(Questions.ARRAYNAME)) {
                userArray = json.getJSONArray(Questions.ARRAYNAME);

                if (!TextUtils.isEmpty(json.getString(Constants.SUCCESS))) {
                    String res = json.getString(Constants.SUCCESS);
                    if (Integer.parseInt(res) == 1) {
                        int success = 0;
                        for (int i = 0; i < userArray.length(); i++) {
                            JSONObject js = userArray.getJSONObject(i);
                            String si = js.getString(Questions.SYSTEMQUESTIONID);
                            String sv = js.getString(Questions.SURVEYNO);
                            String qt = js.getString(Questions.QUESTION);
                            String ct = js.getString(Questions.CATEGORY);
                            String rk = js.getString(Questions.RANK);
                            String ty = js.getString(Questions.TYPE);
                            String ln = js.getString(Questions.LANGUAGE);
                            String rq = "";
                            if(js.has(Questions.REQUIRED)) {
                                rq = js.getString(Questions.REQUIRED);
                            } else {
                                rq = "0";
                            }
//							 if(sv.equals("1"))
//							 Log.v("debug"," language "+ln+" rank "+rk+" survey "+sv+" question text "+qt+ " qid "+si);
                            String[] vals = new String[]{si, sv, qt, ct, rk, ty, ln,rq};
                            if(Validating.areSet(vals)) {
                                if (Questions.isQnPresent(context, vals[0]) < 1) {
                                    if (Questions.storeQuestion(context, vals) == 1) {
                                        success += 1;
                                        publishProgress(i,userArray.length());
                                    }
                                } else {
                                    success +=1;
                                    publishProgress(i,userArray.length());
                                }
                            } else {
                                //send mail here
                                String subject = Constants.getIMEINO(context)+" null question";
                                String body = " Question no "+si+" in survey no "+sv+" has null value " +
                                        " today at "+Constants.getDate();
                                String[] logsdata = {"null value",body};
                                LogsTable.insert(context, logsdata);
                                StringBuffer buffer = new StringBuffer();
                                for(int j = 0; j< vals.length; j++){
                                    buffer.append(vals[j]);
                                }
                                body = body.concat(" the contents of this question is "+buffer.toString());
                                String[] msg = {subject,body};
                                try {
                                    mail.sendEmail(msg);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                publishProgress(i,userArray.length());
                            }
                        }
                        if (success == userArray.length()) {
                             ret[0]= 1;                     
                            appPreference.saveSyncStatus(Constants.QUESTIONSYNC);
                        }
                    }
                }
            }

            else if (json.has(Questions.UPDATEARRAYNAME)) {
                JSONArray jsonArray = json.getJSONArray(Questions.UPDATEARRAYNAME);
                int succes = 0;
                int success_ = 0;
                for (int qi = 0; qi < jsonArray.length(); qi++) {
                    JSONObject js = jsonArray.getJSONObject(qi);
                    if (Questions.isQnPresent(context, js.getString(Questions.SYSTEMQUESTIONID)) > 0) {//the question exists
                        String si = js.getString(Questions.SYSTEMQUESTIONID);
                        String qt = js.getString(Questions.QUESTION);
                        String ty = js.getString(Questions.TYPE);
                        String ln = js.getString(Questions.LANGUAGE);
                        String rank = js.getString(Questions.RANK);
                        String survno = js.getString(Questions.SURVEYNO);
//					 String rq = js.getString(Questions.REQUIRED );
                        String rq = "";
                        if(js.has(Questions.REQUIRED)) {
                            rq = js.getString(Questions.REQUIRED);
                        } else {
                            rq = "0";
                        }
                        String[] vals = new String[]{si, qt, ty, ln, rank, survno,rq};
                        if(Validating.areSet(vals)) {
                            int k = Questions.updateQuestion(context, vals);
                            if (k == 1) {
                                succes += 1;
                                publishProgress(qi,jsonArray.length());
                            }
                        } else {
                            //send mail here
                            String subject = Constants.getIMEINO(context)+" null question";
                            String body = " Question no "+si+" in survey no "+survno+" has null value " +
                                    " today at "+Constants.getDate();
                            String[] logsdata = {"null value",body};
                            LogsTable.insert(context,logsdata);
                            body.concat(" the contents of this question is " + vals);
                            String[] msg = {subject,body};
                            try {
                                mail.sendEmail(msg);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            publishProgress(qi,jsonArray.length());
                        }
                    } else { // its an update but the question is new
                        String si = js.getString(Questions.SYSTEMQUESTIONID);
                        String sv = js.getString(Questions.SURVEYNO);
                        String qt = js.getString(Questions.QUESTION);
                        String ct = js.getString(Questions.CATEGORY);
                        String rk = js.getString(Questions.RANK);
                        String ty = js.getString(Questions.TYPE);
                        String ln = js.getString(Questions.LANGUAGE);
//					 String rq = js.getString(Questions.REQUIRED );
                        String rq = "";
                        if(js.has(Questions.REQUIRED)) {
                            rq = js.getString(Questions.REQUIRED);
                        } else {
                            rq = "0";
                        }
                        String[] vals = new String[]{si, sv, qt, ct, rk, ty, ln,rq};
                        if(Validating.areSet(vals)) {
                            if (Questions.isQnPresent(context, vals[0]) < 1) {
                                if (Questions.storeQuestion(context, vals) == 1) {
                                    success_ += 1;

                                    publishProgress(qi,jsonArray.length());
                                }
                            } else {
                                success_ += 1;
//                                ret[0]= 1;
                                publishProgress(qi,jsonArray.length());
                            }
                        } else {
                            //send mail here
                            String subject = Constants.getIMEINO(context)+" null question";
                            String body = " Question no "+si+" in survey no "+sv+" has null value " +
                                    " today at "+Constants.getDate();
                            body.concat(" the contents of this question is "+vals);
                            String[] msg = {subject,body};
                            String[] logsdata = {"null value",body};
                            LogsTable.insert(context,logsdata);
                            try {
                                mail.sendEmail(msg);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
//                            ret[0]= 1;
                            publishProgress(qi,jsonArray.length());
                        }
                    }

                }
                if(succes > 0 || success_ > 0){
//                    ack(context,1);
                    ret[0]= 1;
//                    publishProgress(qi,jsonArray.length());
                    appPreference.saveSyncStatus(Constants.QUESTIONSYNC);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return ret[0] ;
    }

    protected void onPostExecute(final Integer ret) {
        super.onPostExecute(ret);
        AppPreference appPreference = new AppPreference(context);
        Log.v("returned", "returned is "+ret);

        if(appPreference.getSyQ() == 2){
            if(ret == 1){
                bar.setProgress(100);
                new ProcessAck(context).execute(new String[]{"questions"});
                bar.setIndeterminate(true);
            }else {
                bar.setProgress(100);
            }

            new OptionsSync(context,bar,txt).execute();
        } else {
            bar.setProgress(100);
            new OptionsSync(context,bar,txt).execute();
        }
      /*
        if(ap.getSyncStats() > 4*//*User.getUserCount(context) > 0 && Survey.getCount(context) > 0 && Settings.getSettingsCount(context) > 0 &&
                Questions.getCount(context) > 50 && Responsechoice.getCount(context) > 150*//*){

            Toast.makeText(context," survey sync is complete you can login now ",Toast.LENGTH_SHORT).show();
            bar.setProgress(100);
        }*/
    }
}
