/**
 *
 */
package tz.co.bongolive.apps.prosurvey;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import networking.prosurvey.Validating;
import survery.database.Participant;
import survery.database.Questions;
import survery.database.Response;
import survery.database.Responsechoice;
import survery.database.Survey;
import survery.database.User;

/**
 *    @author nasznjoka
 *
 *
 *    Dec 2, 2014
 */
@SuppressLint("DefaultLocale") public class CloseEnded {
	public static void mcq(final Context context, final String[] info)
	{
		final Dialog closedorigin = new Dialog(context, R.style.CustomDialog);
		closedorigin.setCancelable(false);

		final int rank = Integer.parseInt(info[0]) ;
		int next = Integer.parseInt(info[1]) ;

		final int prev = Integer.parseInt(info[2]);
		Log.v("PREVIOUS QUESTION", info[2]);
		final int participant = Integer.parseInt(info[3]);
		final String flag= info[4];
		final int survey = Integer.parseInt(info[5]) ;

		final String interviewerno = info[6];
		//rank(current),nextqn(same as current),prev,participant,flag,survey,interviewno
		LayoutInflater infl = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = infl.inflate(R.layout.questions, null);
		AppPreference appPreference = new AppPreference(context.getApplicationContext());
		final String language = appPreference.getDefaultLanguage();
		Log.v("LANGUAGE","Language is "+language);
		final int[] checkupdate = {0};

		final int questionid = Questions.getQuestionId(context, new int[]{rank,survey},language);
		Log.v("QUESTION ID","question id  "+questionid +" lang "+language);
		final ArrayList<String> list = Responsechoice.getQuestionAnswer(context, questionid) ;
		final ArrayList<String> listchoice = Responsechoice.getQuestionAnswerId(context,
				questionid, survey) ;

		final int listsize = list.size();
		int responseid = 0;

		final int width = LayoutParams.MATCH_PARENT;
		int height = LayoutParams.MATCH_PARENT ;
		final int viewheight = LayoutParams.WRAP_CONTENT ;
		closedorigin.setContentView(view);

		final LinearLayout layout = (LinearLayout)closedorigin.findViewById(R.id.qnlayout);

		String account = String.valueOf(User.getUserAccount(context,
				Integer.parseInt(flag)));
		String titlee = Survey.getTitle(context, new String[]{account,String.valueOf(survey)});
		TextView txttitle = (TextView)closedorigin.findViewById(R.id.txttitle);
		txttitle.setText(titlee.toUpperCase());

		TextView title = (TextView)closedorigin.findViewById(R.id.txtqno);
		title.setText(context.getString(R.string.strquestionnotitle)+ " "+ rank) ;


		TextView dc = (TextView)closedorigin.findViewById(R.id.txtqn);
		dc.setText(Questions.getQuestionTextLanguage(context,  new int[]{questionid, survey}));

		final RadioGroup rg = new RadioGroup(context);
		final RadioButton[] cb = new RadioButton[listsize] ;

		for(int i = 0; i < listsize; i++) {
			String value = list.get(i);
			cb[i] = new RadioButton(context);
			cb[i].setText(value);
			cb[i].setId(i);
			rg.addView(cb[i]) ;
		}

		layout.addView(rg, width, viewheight);



		final Button bedit = (Button)closedorigin.findViewById(R.id.btnedit);
		final Button bst = (Button)closedorigin.findViewById(R.id.goforward);

		bst.setVisibility(View.VISIBLE);
		int answered = Response.isResponseStored(context, new String[]{info[3],info[0],info[5]});
		TextView txtinfo = new TextView(context);
		TextView txtanswer = new TextView(context);
		txtinfo.setText(context.getString(R.string.stransweredalready));
		if(answered == 1)
		{
			bedit.setEnabled(true);
			layout.addView(txtinfo, width, viewheight);
			for(int i = 0 ; i < listsize; i++)
			{
				cb[i].setEnabled(false);
			}
			String[] ans = Response.getAnswer(context, new String[]{info[3],info[0],info[5]});
			String an = ans[0];
			an = "<b>"+an+"</b>";
			an = an.toUpperCase();
			responseid = Integer.parseInt(ans[1]);

			txtanswer.setTextColor(Color.BLUE);
			txtanswer.setText("\n "+context.getString(R.string.stransweris)+" "+Html.fromHtml(an));
			layout.addView(txtanswer, width, viewheight);
			bst.setVisibility(View.GONE);
		} else {
			txtanswer.setVisibility(View.GONE);
			bedit.setEnabled(false);
			txtinfo.setVisibility(View.GONE);
		}
		bedit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

				for (int i = 0; i < listsize; i++) {
					cb[i].setEnabled(true);
				}
				bedit.setText(context.getString(R.string.strupdate));
				checkupdate[0] = 1;

				String ans = "";
				String ansid = "";
				for (int i = 0; i < listsize; i++) {
					if (cb[i].isChecked()) {
						ans = cb[i].getText().toString();
						ansid = listchoice.get(i);
//						cb[i].clearFocus();
					}
				}
					if (!ans.isEmpty() && !ansid.isEmpty()) {
						final String qn = String.valueOf(rank);
						String prv = String.valueOf(prev);
						String part = String.valueOf(participant);


						final String[] j = {part, qn, prv, String.valueOf(survey), "comments",ans,
								ansid};
						if (Validating.areSet(j)) {
							int storesponse = Response.updateResponse(context, j);
							if (storesponse != 0) {
								final int nextqn = Responsechoice.getNextQn(context, questionid,  j[5]);
								System.out.print("\n\n THIS IS THE NEXT QUESTION  (" + nextqn +
										")");

								if (nextqn != 0) {
									Log.w("tayana", " the next qn flag is not 0 it is " + nextqn +
											"\n");
									int quid = Questions.getQuestionId(context,new int[]{nextqn,survey},
											language);//rank,survey
									Log.v("quid","quid is "+quid);
									String nextqntxt = Questions.getQuestionTextLanguage(context,
											new int[]{quid, survey});
									if (Validating.areSet(nextqntxt)) {
										int jumpedrank = Response.getJumpedRank(context,
												new int[]{questionid,participant,survey});
										Log.v("jumpedrank"," it is "+jumpedrank + " nextqn is "+
												nextqn);
										if(jumpedrank != 0){
											 // this question had jumped before
											if (jumpedrank > nextqn){
												Log.v("storeresponse","response id is " +
														""+storesponse);
												//data[] = id,jumpflag,jumprank
												int jumpflag = Response.updateJumpFlag(context,
														new int[]{storesponse,1,nextqn});
												if(jumpflag == 1) {
													String[] info = {j[1], String.valueOf(nextqn), qn, j[0], flag, String.valueOf(survey), interviewerno};

													//info[0] rank, nextqn, prevqn,survey,
													// participant,
													GeneralViews.getNextQuestion(context, info);
													info = null;
													list.clear();

													closedorigin.dismiss();
												}
											} else if (jumpedrank < nextqn){
												//last jump smaller than the current one
												//delete all response btn lastjump and nextqn
												for(int k= jumpedrank; k < nextqn ; k++){
													int deletequid = Questions.getQuestionId
															(context,new int[]{k,survey},
																	language);
													Log.v("deletequid","deletequid is "+deletequid);
													int delres = Response.getResponseId
															(context,new int[]{deletequid,
																	participant,survey});
													Log.v("delresid","it is "+delres);
													int del = Response.deleteResponse(context, delres);
													Log.v("deleteresponse","this is deleted " +
															" del flag is "+del +" qn no is "+k);
												}
												int jumpflag = Response.updateJumpFlag(context,
														new int[]{storesponse,1,nextqn});
												if(jumpflag == 1) {
													String[] info = {j[1], String.valueOf(nextqn), qn, j[0], flag, String.valueOf(survey), interviewerno};


													//info[0] rank, nextqn, prevqn,survey,
													// participant,
													GeneralViews.getNextQuestion(context, info);
													info = null;
													list.clear();

													closedorigin.dismiss();
												}
											} else {
												//the jump flag equals to the nextqn so do nothing
												String[] info = {j[1], String.valueOf(nextqn), qn, j[0], flag, String.valueOf(survey), interviewerno};

												//info[0] rank, nextqn, prevqn,survey,participant,
												GeneralViews.getNextQuestion(context, info);
												info = null;
												list.clear();
												closedorigin.dismiss();
											}
										} else if (jumpedrank == 0) {
											Log.v("jumprank","jump rank is "+jumpedrank);
											//this question did not jump before but now jumps
											// so delete all responses btn this rank and the
											// jumped rank
											for(int k= rank+1; k < nextqn ; k++){
												int deletequid = Questions.getQuestionId
														(context,new int[]{k,survey}, language);
												Log.v("deletequid","deletequid is "+deletequid);
												int delres = Response.getResponseId
														(context,new int[]{deletequid,
																participant,survey});
												Log.v("delresid","it is "+delres);

												int del = Response.deleteResponse(context, delres);
												Log.v("deleteresponse","this is deleted " +
														" del flag is "+del +" qn no is "+k);
											}
											int jumpflag = Response.updateJumpFlag(context,
													new int[]{storesponse,1,nextqn});
											if(jumpflag == 1) {
												String[] info = {j[1], String.valueOf(nextqn), qn, j[0], flag, String.valueOf(survey), interviewerno};


												//info[0] rank, nextqn, prevqn,survey,
												// participant,
												GeneralViews.getNextQuestion(context, info);
												info = null;
												list.clear();

												closedorigin.dismiss();
											}
										}
									}
									else {
										String _surv = String.valueOf(survey);
										String _part = String.valueOf(participant);
										int end = Participant.insetEndtime(context, new String[]{_surv, interviewerno, _part});
										if (end == 1) {
											Toast.makeText(context, context.getString(R.string.strsurveycomplete), Toast.LENGTH_LONG).show();
											if (closedorigin.isShowing()) {
												closedorigin.dismiss();
											}
										}
									}
								}
								else {
									//1 if now there's jump then the the next responses before
									// the jump rank should be deleted
									//2 if now there's no jump then the proceed normally
									Log.w("tayana", " the next qn flag is 0 where we follow a norma flow " +
											"\n");
									int rnk = rank + 1;

									int quid = Questions.getQuestionId(context,new int[]{rnk,survey},
											language);//rank,survey
									String nextqntxt = "";
									if(quid > 0) {
										nextqntxt = Questions.getQuestionTextLanguage(context,
												new int[]{quid, survey});
									}
									if (Validating.areSet(nextqntxt)) {
										// check to see if the last response jumped
										int jumpedrank = Response.getJumpedRank(context,
												new int[]{questionid,participant,survey});
										if(jumpedrank != 0){
											Log.v("jumpstatus","this question had jump");
											int jumpflag = Response.updateJumpFlag(context,
													new int[]{storesponse,0,0});
											if(jumpflag == 1) {
												String[] info = {j[1], String.valueOf(rnk), qn,
														j[0], flag, String.valueOf(survey),
														interviewerno};
												GeneralViews.getNextQuestion(context, info);
												info = null;
												list.clear();
												closedorigin.dismiss();
											}

											} else if (jumpedrank == 0){
											Log.v("jumpstatus","this question had no jump");
											//this question did not jump before and does not jump
											// now
											// so just proceed normally
											String nextq = String.valueOf(rnk);
											Log.w("tayana", " increment rank by 1 to be" + rnk + "\n");
											String[] info = {j[1], nextq, qn, j[0], flag, String.valueOf(survey), interviewerno};
											//info[0] rank, nextqn, prevqn,survey,participant,
											GeneralViews.getNextQuestion(context, info);
											info = null;
											list.clear();
											closedorigin.dismiss();
										}
									}
									else {
										String _surv = String.valueOf(survey);
										String _part = String.valueOf(participant);
										int end = Participant.insetEndtime(context, new String[]{_surv, interviewerno, _part});
										if (end == 1) {
											Toast.makeText(context, context.getString(R.string.strsurveycomplete), Toast.LENGTH_LONG).show();
											if (closedorigin.isShowing()) {
												closedorigin.dismiss();
											}
										}
									}
								}
							}
						}
					}
					else {
						Toast.makeText(context, context.getString(R.string.strfillanswer),
								Toast.LENGTH_LONG).show();

					}

//				}
			}
		}) ;



// submt button begins here
		bst.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				String ans = "";
				String ansid = "";
				for (int i = 0; i < listsize; i++) {
					if (cb[i].isChecked()) {
						ans = cb[i].getText().toString();
						ansid = listchoice.get(i);
						cb[i].setChecked(true);
					}
				}
					final String qn = String.valueOf(rank);
					String prv = String.valueOf(prev);
					String part = String.valueOf(participant);

					final String[] j = {part, qn, prv, String.valueOf(survey), "comments",ans,
							ansid};
					if (Validating.areSet(j)) {
						int storesponse = Response.storeResponse(context, j);
						if (storesponse != 0) {
							final int nextqn = Responsechoice.getNextQn(context, questionid,  j[5]);
//							int responseid = Response.getResponseId(context, questionid);

							Log.v("GETNEXT", " QN NO " + rank + " ANSWER " + ans + " responseid " +
									""+storesponse);
							if (nextqn != 0) {
								Log.w("tayana", " the next qn flag is not 0 it is " + nextqn  );
								int quid = Questions.getQuestionId(context,new int[]{nextqn,survey},
										language);
								String nextqntxt = Questions.getQuestionTextLanguage(context,
										new int[]{quid, survey});
								Log.v("storeresponse","store response is "+ storesponse);
								if (Validating.areSet(nextqntxt)) {
									int jumpflag = Response.updateJumpFlag(context,
											new int[]{storesponse,1,nextqn});
									if(jumpflag == 1) {
										String[] info = {j[1], String.valueOf(nextqn), qn, j[0],
												flag, String.valueOf(survey), interviewerno};
										GeneralViews.getNextQuestion(context, info);
										info = null;
										list.clear();

										closedorigin.dismiss();
									}
								}
								else {
									String _surv = String.valueOf(survey);
									String _part = String.valueOf(participant);
									int end = Participant.insetEndtime(context, new String[]{_surv, interviewerno, _part});
									if (end == 1) {
										Toast.makeText(context, context.getString(R.string.strsurveycomplete), Toast.LENGTH_LONG).show();
										if (closedorigin.isShowing()) {
											closedorigin.dismiss();
										}
									}
								}
							}
							else {
								Log.w("tayana", " the next qn flag is 0 where we follow a norma flow " +
										"\n");
								int rnk = rank + 1;
								int quid = Questions.getQuestionId(context,new int[]{rnk,survey},
										language);//rank,survey
								String nextqntxt = "";
								if(quid > 0) {
									nextqntxt = Questions.getQuestionTextLanguage(context,
											new int[]{quid, survey});
								}
								if (Validating.areSet(nextqntxt)) {
									String nextq = String.valueOf(rnk);
									Log.w("tayana", " increment rank by 1 to be" + rnk + "\n");
									String[] info = {j[1], nextq, qn, j[0], flag, String.valueOf(survey), interviewerno};
									//info[0] rank, nextqn, prevqn,survey,participant,
									GeneralViews.getNextQuestion(context, info);
									info = null;
									list.clear();
									closedorigin.dismiss();
								}
								else {
									String _surv = String.valueOf(survey);
									String _part = String.valueOf(participant);
									int end = Participant.insetEndtime(context, new String[]{_surv, interviewerno, _part});
									if (end == 1) {
										Toast.makeText(context, context.getString(R.string.strsurveycomplete), Toast.LENGTH_LONG).show();
										if (closedorigin.isShowing()) {
											closedorigin.dismiss();
										}
									}
								}
							}
						}

					}
				}

//			}
		}) ;

		ImageView bspt = (ImageView)closedorigin.findViewById(R.id.goback);
		bspt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				int pre = Response.getPreviousQn(context, info);
				String prevofpre = String.valueOf(pre);
				Log.v("PREVIOUS", "rank qn is "+ info[2]);
				Log.v("PREVIOUS", "previous qn is "+ prevofpre);
				Log.v("PREVIOUS", "next qn is "+ info[2]);
				Log.v("PREVIOUS", "previous qn before "+ info[2] +" qn is "+ prevofpre);
				String[] data = {info[2],info[2],prevofpre,info[3],info[4],info[5],info[6]} ;
				GeneralViews.getPrevQuestion(context, data, Integer.parseInt(info[2]));
				Log.v("testing"," current rank assumed is "+info[2] + " rank is "+rank);
				closedorigin.dismiss();
			}
		}) ;

		ImageView skip = (ImageView)closedorigin.findViewById(R.id.btnskip);
		final String[] answeravailable = Response.getAnswer(context, new String[]{info[3],info[0],
				info[5]});
		if(answeravailable[0].isEmpty()){
			skip.setVisibility(View.GONE);
		}
			skip.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					if (checkupdate[0] == 1) {

						AlertDialogManager alertDialogManager = new AlertDialogManager();
						alertDialogManager.showAlertDialog(context, "ERROR",
								context.getString(R.string.strnoskip), false);
					}
					else {
						int rnk;
						int rank = Integer.parseInt(info[0]);
						rnk = rank + 1;
						String pr = info[2];
						String p = info[3];
						String flag = info[4];
						String s = info[5];
						String inter = info[6];
						int quid = Questions.getQuestionId(context, new int[]{rnk, survey},
								language);//rank,survey
						String nextqntxt = "";
						if (quid > 0) {
							nextqntxt = Questions.getQuestionTextLanguage(context,
									new int[]{quid, survey});
						}
						if (Validating.areSet(nextqntxt)) {
							int responseid = Integer.parseInt(answeravailable[1]);
							Log.w("responseid", " response id is "+responseid);
							int newrank = Response.getJumpedRank(context,
									new int[]{questionid,participant,survey});
							if(newrank == 0){
								Log.v("nojump","this question has no jump skip normally");
								String[] info = {String.valueOf(rnk), String.valueOf(rnk), pr, p, flag, s, inter};


								GeneralViews.getNextQuestion(context, info);
								info = null;
								list.clear();
								closedorigin.dismiss();
							} else if (newrank > 0){
								Log.v("availjump","this question has jump skip to the jumped rank");
								String[] info = {String.valueOf(newrank), String.valueOf(newrank), pr, p,
										flag, s, inter};


								GeneralViews.getNextQuestion(context, info);
								info = null;
								list.clear();
								closedorigin.dismiss();
							}
						}
						else {
							String _surv = String.valueOf(survey);
							String _part = String.valueOf(participant);
							int end = Participant.insetEndtime(context, new String[]{_surv, interviewerno, _part});
							if (end == 1) {
								Toast.makeText(context, context.getString(R.string.strsurveycomplete), Toast.LENGTH_LONG).show();
								if (closedorigin.isShowing()) {
									closedorigin.dismiss();
								}
							}
						}
					}
				}
			});

		Button masterexit = (Button)closedorigin.findViewById(R.id.btnexit);
		masterexit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String _surv = String.valueOf(survey);
				String _part = String.valueOf(participant);
				if (rank > 1) {

					int end = Participant.insetEndtime(context, new String[]{_surv, interviewerno, _part});

					if (end == 1) {

						Toast.makeText(context, context.getString(R.string.strsurveyexited),
								Toast.LENGTH_LONG).show();
//						if (closedorigin.isShowing()) {
							closedorigin.dismiss();
						Intent intent = new Intent(context, Dashboard.class);
						intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
						context.startActivity(intent);

//						}
					}
				}
			}
		});

		closedorigin.show();
	}
}