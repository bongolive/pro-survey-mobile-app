package tz.co.bongolive.apps.prosurvey;

import android.accounts.Account;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import survery.database.ContentProviderSurvey;
import survery.database.User;

public class Dashboard extends ActionBarActivity implements
		NavigationDrawerFragment.NavigationDrawerCallbacks {

	private NavigationDrawerFragment mNavigationDrawerFragment;

	private CharSequence mTitle;
	private static final String ARG_SECTION_NUMBER = "section_number";
	Account dmAccount ;
	static int role ;
	SharedPreferences sp ;
	AppPreference appPreference;
	int status ;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dashboard);

		appPreference = new AppPreference(getApplicationContext());
		role = appPreference.getUserRole();
		mNavigationDrawerFragment = (NavigationDrawerFragment)
				getSupportFragmentManager()
						.findFragmentById(R.id.navigation_drawer);

		mTitle = getTitle();
		mNavigationDrawerFragment.setUp(R.id.navigation_drawer,
				(DrawerLayout) findViewById(R.id.drawer_layout));
//		Responsechoice.updateVariousData(this, 44);
	}
	@Override
	public void onNavigationDrawerItemSelected(int position) {
		int newposition = position+1 ;
		FragmentManager fragmentManager = getSupportFragmentManager();
		Bundle args = new Bundle() ;
		if(role == 0) {
			switch(newposition)
			{
				case 1:
						Surveys bb = new Surveys();
						args.putInt(ARG_SECTION_NUMBER, newposition);
						bb.setArguments(args) ;
						fragmentManager.beginTransaction().
								replace(R.id.container,bb).
								commit();
					break;
				case 2:
					Surveys oo = new Surveys();
					args.putInt(ARG_SECTION_NUMBER, newposition);
					oo.setArguments(args) ;
					fragmentManager.beginTransaction().
							replace(R.id.container,oo).
							addToBackStack(ARG_SECTION_NUMBER).
							commit();
					break;
				case 3:
					Intent intent = new Intent(this, Settings.class);
					startActivity(intent);
					break;
				case 4:
					System.exit(0);
					finish();
					break;
			}
		} else if (role == 1) {

			int isadmin = appPreference.getAdminStatus(this);
			switch(newposition)
			{
				case 1:
					if(isadmin == 0) {
						SurveysAdmin sa = new SurveysAdmin();
						args.putInt(ARG_SECTION_NUMBER, newposition);
						args.putInt(User.SYSTEMUSERID, status);
						sa.setArguments(args);
						fragmentManager.beginTransaction().
								replace(R.id.container, sa).
								addToBackStack(ARG_SECTION_NUMBER).
								commit();
					} else if(isadmin == 1)
					{
						Surveys oo = new Surveys();
						args.putInt(ARG_SECTION_NUMBER, newposition);
						oo.setArguments(args) ;
						fragmentManager.beginTransaction().
								replace(R.id.container,oo).
								addToBackStack(ARG_SECTION_NUMBER).
								commit();
					}
					break;
				case 2:

					if(isadmin == 0) {
						SurveysAdmin oo = new SurveysAdmin();
						args.putInt(ARG_SECTION_NUMBER, newposition);
						args.putInt(User.SYSTEMUSERID, status);
						oo.setArguments(args);
						fragmentManager.beginTransaction().
								replace(R.id.container, oo).
								addToBackStack(ARG_SECTION_NUMBER).
								commit();
					} else if(isadmin == 1)
					{
						Surveys oo = new Surveys();
						args.putInt(ARG_SECTION_NUMBER, newposition);
						oo.setArguments(args) ;
						fragmentManager.beginTransaction().
								replace(R.id.container,oo).
								addToBackStack(ARG_SECTION_NUMBER).
								commit();
					}
					break;
				case 3:
					Users cc = new Users();
					args.putInt(ARG_SECTION_NUMBER, newposition);
					cc.setArguments(args);
					fragmentManager.beginTransaction().
							replace(R.id.container,cc).
							addToBackStack(ARG_SECTION_NUMBER).
							commit();
					break;
				case 6:
					System.exit(0);
					finish();
					break;
				case 5:
					Intent intent = new Intent(this, PrefActivity.class);
					startActivity(intent);
					break;
				case 4:
					SyncLogs sy = new SyncLogs();
					args.putInt(ARG_SECTION_NUMBER, newposition);
					sy.setArguments(args);
					fragmentManager.beginTransaction().
							replace(R.id.container,sy).
							addToBackStack(ARG_SECTION_NUMBER).
							commit();
					break;
			}
		}
	}

	public void onSectionAttached(int number) {
		if(role == 0) {
			switch (number) {
				case 1:
					mTitle = getString(R.string.title_section1);
					break;
				case 2:
					mTitle = getString(R.string.title_section2);
					break;
				case 3:
					mTitle = getString(R.string.title_section3);
					break;
				case 4:
					mTitle = getString(R.string.title_section4);
					break;
			}

		} else if (role == 1){
			switch (number) {
				case 1:
					mTitle = getString(R.string.title_section1);
					break;
				case 2:
					mTitle = getString(R.string.title_section2);
					break;
				case 3:
					mTitle = getString(R.string.title_section3);
					break;
				case 4:
					mTitle = getString(R.string.title_section6);
					break;
				case 5:
					mTitle = getString(R.string.title_section4);
					break;
				case 6:
					mTitle = getString(R.string.title_section5);
					break;
			}
		}
	}

	public void restoreActionBar() {
		ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle(mTitle);
	}

	public void restoreOnBackStack()
	{
		getSupportFragmentManager().addOnBackStackChangedListener(
				new FragmentManager.OnBackStackChangedListener() {
					@Override
					public void onBackStackChanged() {
						restoreActionBar();
					}
				}) ;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (!mNavigationDrawerFragment.isDrawerOpen()) {
			// Only show items in the action bar relevant to this screen
			// if the drawer is not showing. Otherwise, let the drawer
			// decide what to show in the action bar.
			getMenuInflater().inflate(R.menu.dashboard, menu);
			restoreActionBar();
			restoreOnBackStack();
			return true;
		}
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch(item.getItemId()){
			case R.id.action_syncnow:
				StartScreen startScreen = new StartScreen();
				ondemandsync(dmAccount);
				break;
		}
		return super.onOptionsItemSelected(item);
	}

	public void ondemandsync(Account mAccount)
	{

		Bundle settingsBundle = new Bundle() ;
		settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
		settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
		ContentResolver.requestSync(mAccount, ContentProviderSurvey.AUTHORITY, settingsBundle);
	}


}
